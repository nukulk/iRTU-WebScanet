#include "AIOService.hpp"

int main(int argc, char* argv[])
{
    AIOService aio;

    if(!aio.Initialize())
    {
        return -1;
    }

    aio.Start();

    return 0;
}
