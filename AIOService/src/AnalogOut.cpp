//
// Created by mukeshp on 5/19/2020.
//
#include "AnalogOut.hpp"
#include "I2CInterface.hpp"

#define DAC_REG_ADDR_DEVICE_ID  (0x01)
#define DAC_REG_ADDR_SYNC       (0x02)
#define DAC_REG_ADDR_CONFIG     (0x03)
#define DAC_REG_ADDR_GAIN       (0x04)
#define DAC_REG_ADDR_TRIGGER    (0x05)
#define DAC_REG_ADDR_STATUS     (0x07)
#define DAC_REG_ADDR_DATA       (0x08)

static int AnalogOutReadRegister(char iSlaveAddr, char iRegisterAddr, char *pBuf, unsigned int iBufLen);
static int AnalogOutWriteRegister(char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen);

int DacInit(void)
{
    DacReadDeviceIdentification(AO1_I2C_SLAVE_ADDR);

    DacReadDeviceIdentification(AO2_I2C_SLAVE_ADDR);

    DacWriteTriggerRegister(AO1_I2C_SLAVE_ADDR, 0x000A);
    DacWriteTriggerRegister(AO2_I2C_SLAVE_ADDR, 0x000A);
    sleep(5);

    DacGetGainAndDivisor(AO1_I2C_SLAVE_ADDR, 0,0);
    DacGetGainAndDivisor(AO2_I2C_SLAVE_ADDR, 0,0);

    /* 0 to 1.25 V output : Taken from TI support */
    //DacSetGainAndDivisor(AO1_I2C_SLAVE_ADDR, 0, 1);
    //DacSetGainAndDivisor(AO2_I2C_SLAVE_ADDR, 0, 1);

    /* 0 to 2.5 V output : Taken from TI support */
    DacSetGainAndDivisor(AO1_I2C_SLAVE_ADDR, 1, 1);
    DacSetGainAndDivisor(AO2_I2C_SLAVE_ADDR, 1, 1);

    DacGetGainAndDivisor(AO1_I2C_SLAVE_ADDR, 0,0);
    DacGetGainAndDivisor(AO2_I2C_SLAVE_ADDR, 0,0);

    return 0;
}

int Dac_write(char iDevice, unsigned short int iData)
{
    /** 10V - 0xFFF
     *  0V  - 0x000
     *  5V  - 0x7FF
     * */

    unsigned short int tDataRead1 = 0;
    unsigned short int tDataRead2 = 0;

    DacWriteData(iDevice, iData);
    //DacWriteData(AO2_I2C_SLAVE_ADDR, iData);

    DacReadData(iDevice, &tDataRead1);
    //DacReadData(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    printf("tDataRead:%x\n", tDataRead1);
    //printf("tDataRead2:%x\n", tDataRead2);

    //DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    //DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    //printf("tDataRead1-Status:%x\n", tDataRead1);
    //printf("tDataRead2-Status:%x\n", tDataRead2);

#if 0
    DacWriteData(AO1_I2C_SLAVE_ADDR, 0x7FF);
        DacWriteData(AO2_I2C_SLAVE_ADDR, 0x7FF);

        DacReadData(AO1_I2C_SLAVE_ADDR, &tDataRead1);
        DacReadData(AO2_I2C_SLAVE_ADDR, &tDataRead2);
        printf("tDataRead1:%x\n", tDataRead1);
        printf("tDataRead2:%x\n", tDataRead2);

        DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
        DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
        printf("tDataRead1-Status:%x\n", tDataRead1);
        printf("tDataRead2-Status:%x\n", tDataRead2);
        sleep(10);
#endif
#if 0
    DacWriteData(AO1_I2C_SLAVE_ADDR, 0x000);
    DacWriteData(AO2_I2C_SLAVE_ADDR, 0xFFF);

    DacReadData(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    DacReadData(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    printf("tDataRead1:%x\n", tDataRead1);
    printf("tDataRead2:%x\n", tDataRead2);

    DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    printf("tDataRead1-Status:%x\n", tDataRead1);
    printf("tDataRead2-Status:%x\n", tDataRead2);

    DacReadStatusRegister(AO1_I2C_SLAVE_ADDR, &tDataRead1);
    DacReadStatusRegister(AO2_I2C_SLAVE_ADDR, &tDataRead2);
    printf("tDataRead1-Status:%x\n", tDataRead1);
    printf("tDataRead2-Status:%x\n", tDataRead2);

    sleep(10);
#endif
    return 0;
}

/*
 *
 */
int DacReadDeviceIdentification(char iSlaveAddr)
{
    char tDeviceIdBuf[2] = {0,};
    //unsigned short int tDeviceId = 0;

    if(0 != AnalogOutReadRegister(iSlaveAddr, DAC_REG_ADDR_DEVICE_ID, (char*)&tDeviceIdBuf, 2))
    {
        printf("AnalogOut Device Id read failed\n");
        return -1;
    }
    printf("tDeviceId[0]:%x, tDeviceId[1]:%x\n", tDeviceIdBuf[0], tDeviceIdBuf[1]);

    return 0;
}

/* It set DAC mode of operations: synchronpus or asynchronous
 * Synchronous Mode: When set to 1, the DAC output is set to update in response to an LDAC trigger (synchronous mode).
 * Asynchronous Mode: When cleared to 0 ,the DAC output is set to update immediately (asynchronous mode), default.
 * */
int DacSetOperationMode(char iSlaveAddr, unsigned short int iMode)
{
    //unsigned short int tMode = 0x01; //0x01 /* Synchronous*/ , 0x00 /* Asynchronous mode*/;
    if(0 != AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_SYNC, (char*)&iMode, 2))
    {
        printf("Error to set operational mode\n");
        return -1;
    }

    return 0;
}

/*
 *
 */
int DacSoftReset(char iSlaveAddr)
{
    unsigned short int tSoftResetCmd = 0x000A;

    if(0!= AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_TRIGGER, (char*)&tSoftResetCmd, 2))
    {
        printf("Dac soft reset failed\n");
        return -1;
    }

    return 0;
}

/*
 *
 */
int DacLoadSynchronously(char iSlaveAddr)
{
    unsigned short int tSyncLoadCmd = 0x0010;

    if(0!= AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_TRIGGER, (char*)&tSyncLoadCmd, 2))
    {
        printf("Dac synchronously load failed\n");
        return -1;
    }

    return 0;
}

/**
 * Select Reference voltage.
 * 1 : External reference voltage, 0: Internal reference voltage.
 * @return
 */
int DcaSelectReferenceVoltage(char iSlaveAddr, char iRef)
{
    unsigned short int tRefVolData = 0;

    if(1 == iRef) /* External reference voltage */
    {
        tRefVolData = 0x0100;
    }
    else if(0 == iRef) /* Internal reference voltage */
    {
        tRefVolData = 0;
    }
    else
    {
        printf("Invalid reference voltage selection\n");
        return -1;
    }

    if(0!= AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_CONFIG, (char*)&tRefVolData, 2))
    {
        printf("Dac reference voltage selection failed\n");
        return -1;
    }

    return 0;
}

int DacSetGainAndDivisor(char iSlaveAddr, unsigned char iBufGain, unsigned char iRefDiv)
{
    unsigned short int tMulAndDiv = 0;

    if(1 == iBufGain) /* When set to 1, the buffer amplifier for corresponding DAC has a gain of 2.*/
    {
        tMulAndDiv |= 0x0001;
    }
    else if(0 == iBufGain) /* When clear to 0, the buffer amplifier for corresponding DAC has a gain of 1.*/
    {
        tMulAndDiv &= (~0x0001);
    }
    else
    {
        printf("Invalid multiplier value at %s\n", __func__ );
    }

    if(1 == iRefDiv) /* When set to 1, the reference voltage division by 2 */
    {
        tMulAndDiv |= 0x0100;
    }
    else if(0 == iRefDiv) /* When clear to 0, the reference voltage division by 1 */
    {
        tMulAndDiv &= (~0x0100);
    }
    else
    {
        printf("Invalid Divisor value at %s\n", __func__ );
    }

    char tBuffer[8] = {0,};

    tBuffer[0] = (char)(tMulAndDiv >> 8) & 0x00FF;
    tBuffer[1] = tMulAndDiv & 0x00FF;

    if(0 != AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_GAIN, tBuffer, 2))
    {
        printf("Dac set multiplier and divisor failed\n");
        return -1;
    }

    return 0;
}

int DacGetGainAndDivisor(char iSlaveAddr, unsigned char iBufGain, unsigned char iRefDiv)
{
    char tBuffer[8] = {0,};

    if(0 != AnalogOutReadRegister(iSlaveAddr, DAC_REG_ADDR_GAIN, (char*)tBuffer, 2))
    {
        printf("Dac Get multiplier and divisor failed\n");
        return -1;
    }

    printf("Gain Register -> tBuffer[1]:%x, tBuffer[0]:%x\n", tBuffer[1], tBuffer[0]);
    return 0;
}

/*
 *
 */
int DacWriteData(char iSlaveAddr, unsigned short int iData)
{
    char tBuffer[8] = {0,};

    //iData = (iData << 8) & 0xFFF0;
    //iData &= 0x0FFF;

    tBuffer[0] = (char)((iData & 0x0FF0) >> 4);
    tBuffer[1] = (char)((iData & 0x000F) << 4);

    printf("Data Write tBuffer[0]:%x, tBuffer[1]:%x\n", tBuffer[0], tBuffer[1]);
    //if(0!= AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_DATA, (char*)&iData, 2))
    if(0!= AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_DATA, (char*)tBuffer, 2))
    {
        printf("Dac Data write failed\n");
        return -1;
    }

    return 0;
}

int DacReadData(char iSlaveAddr, unsigned short int *pData)
{
    char tBuffer[8] = {0,};

    if(0!= AnalogOutReadRegister(iSlaveAddr, DAC_REG_ADDR_DATA, (char*)tBuffer, 2))
    {
        printf("Dac Data read failed\n");
        return -1;
    }

    printf("\n--------------------------------------\n");
    printf("tBuffer[0]:%x,tBuffer[1]:%x\n", tBuffer[0], tBuffer[1]);
    printf("\n--------------------------------------\n");
    (*pData) = 0;
    (*pData) = (tBuffer[0] << 8);
    (*pData) = (*pData) | tBuffer[1];

    return 0;
}

int DacReadStatusRegister(char iSlaveAddr, unsigned short int *pData)
{
    char tBuffer[8] = {0,};

    if(0!= AnalogOutReadRegister(iSlaveAddr, DAC_REG_ADDR_STATUS, (char*)tBuffer, 2))
    {
        printf("Dac Status register read failed\n");
        return -1;
    }

    (*pData) = 0;
    (*pData) = (tBuffer[0] << 8);
    (*pData) = (*pData) | tBuffer[1];

    return 0;
}

int DacWriteTriggerRegister(char iSlaveAddr, unsigned short int iData)
{
    char tBuffer[8] = {0,};

    iData &= 0x0FFF;

    tBuffer[0] = (char)((iData & 0xFF00)>>8);
    tBuffer[1] = (char)(iData & 0x00FF);

    if(0!= AnalogOutWriteRegister(iSlaveAddr, DAC_REG_ADDR_TRIGGER, (char*)tBuffer, 2))
    {
        printf("Dac Trigger register write failed\n");
        return -1;
    }

    return 0;
}


static int AnalogOutWriteRegister(char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen)
{
    char tDeviceName[64] = {0,};
    int tFd = -1;
    int tRetVal = -1;
    char tBuffer[16] = {0,};

    tBuffer[0] = iRegisterAddr;
    tBuffer[1] = pDataBuf[0];
    tBuffer[2] = pDataBuf[1];
    tBuffer[3] = '\0';

#if 0
    /** todo: Need to identify adapter number with sting operation from /sys/class/i2c-dev/ */
    if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-0");
    }
    else if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-1");
    }
#endif

    strcpy(tDeviceName, "/dev/i2c-1");

    tFd = I2C_Open(tDeviceName);
    if(tFd < 0)
    {
        printf("Error at I2c_Device open (%s) at %s\n", tDeviceName, __func__ );
        return -1;
    }

    do
    {
        if (ioctl(tFd, I2C_SLAVE, iSlaveAddr>>1) < 0) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            printf("Error to set slave address at %s\n", __func__);
            //exit(1);
            break;
        }

#if 0
        if (1 != I2C_Write(tFd, tBuffer, 1)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            printf("Error to write register address at %s\n", __func__);
            //exit(1);
            break;
        }
#endif
        if (3 != I2C_Write(tFd, tBuffer, 3)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            printf("Error to write register address at %s\n", __func__);
            //exit(1);
            break;
        }

        /*Success return value */
        tRetVal = 0;

    }while (0);

    I2C_Close(tFd);

    return tRetVal;
}

static int AnalogOutReadRegister(char iSlaveAddr, char iRegisterAddr, char *pDataBuf, unsigned int iDataBufLen)
{
    extern int adaptorNo;
    char tDeviceName[64] = {0,};
    int tFd = -1;
    int tRetVal = -1;

    /** todo: Need to identify adapter number with sting operation from /sys/class/i2c-dev/ */
#if 0
    if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-0");
    }
    else if(AO1_I2C_SLAVE_ADDR == iSlaveAddr)
    {
        strcpy(tDeviceName, "/dev/i2c-1");
    }
#endif

    //sprintf(tDeviceName,"/dev/i2c-%d", adaptorNo);
    strcpy(tDeviceName, "/dev/i2c-1");

    tFd = I2C_Open(tDeviceName);
    if(tFd < 0)
    {
        printf("Error at I2c_Device open(%s) at %s\n", tDeviceName, __func__ );
        return -1;
    }

    do
    {
        if (ioctl(tFd, I2C_SLAVE, iSlaveAddr>>1) < 0) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            printf("Error to set slave address at %s\n", __func__);
            //exit(1);
            break;
        }

        if (1 != I2C_Write(tFd, &iRegisterAddr, 1)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            printf("Error to write register address at %s\n", __func__);
            //exit(1);
            break;
        }

        if (iDataBufLen != I2C_Read(tFd, pDataBuf, iDataBufLen)) {
            /* ERROR HANDLING; you can check errno to see what went wrong */
            printf("Error to write register address at %s\n", __func__);
            //exit(1);
            break;
        }

        /*Success return value */
        tRetVal = 0;

    }while (0);

    I2C_Close(tFd);

    return tRetVal;
}

