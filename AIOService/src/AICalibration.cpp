//
// Created by mukeshp on 31-Jul-20.
//
#include "AICalibration.hpp"
#include "AIOConfig.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <termios.h>

extern  AiCnfg_t global_analog_in_configuration;

AiCalibration_t gAiCalibration[AI_CHANNEL_MAX];

int kbhit();


int Adc_calibration(void)
{
    unsigned short int tData = 0;
    float tEnggData = 0;

    for(int tChannelNum = CHANNEL_NUM_1 ; tChannelNum <= CHANNEL_NUM_4 ; ++tChannelNum)
    {
        AiChannelCnfg_t *pAichannelCnfg = &global_analog_in_configuration.mAiChannelCnfg[tChannelNum];

        if(AI_CHANNEL_ENABLE == pAichannelCnfg->mIsChannelEnable)
        {
            char option;
            printf("Want to perform calibration for channel:%02d(Y/N)\n", tChannelNum+1);
            scanf(" %c", &option);

            if( ('N' == option) || ('n' == option))
            {
                continue;
            }

            printf("Please give system low %s at channel:%02d then enter\n", GET_AI_CHANNEL_TYPE_STR(pAichannelCnfg->mChannelType), tChannelNum+1);
            while (!kbhit());
            while (!kbhit())
            {
                tData = Adc_ReadData((ChannelNum_e)tChannelNum);
                Adc_ConvertBin2Engg((ChannelNum_e)tChannelNum, tData, &tEnggData);
                //printf("AI%02d : %3.2f\n", tChannelNum+1, tEnggData);
                printf("AI%02d : %3.2f(%d)\n", tChannelNum+1, tEnggData, tData);
                sleep(1);
            }

            Adc_systemLowCalibration((ChannelNum_e)tChannelNum, tData);

            printf("\n\nPlease give system high %s at channel:%02d then enter\n", GET_AI_CHANNEL_TYPE_STR(pAichannelCnfg->mChannelType), tChannelNum+1);
            while (!kbhit());
            while (!kbhit())
            {
                tData = Adc_ReadData((ChannelNum_e)tChannelNum);
                Adc_ConvertBin2Engg((ChannelNum_e)tChannelNum, tData, &tEnggData);
                printf("AI%02d : %3.2f\n", tChannelNum+1, tEnggData);
                sleep(1);
            }
            Adc_systemHighCalibration((ChannelNum_e)tChannelNum, tData);

            printf("\n\nPlease give system mid %s at channel:%02d then enter\n", GET_AI_CHANNEL_TYPE_STR(pAichannelCnfg->mChannelType), tChannelNum+1);
            while (!kbhit());
            while (!kbhit())
            {
                tData = Adc_ReadData((ChannelNum_e)tChannelNum);
                Adc_ConvertBin2Engg((ChannelNum_e)tChannelNum, tData, &tEnggData);
                printf("AI%02d : %3.2f\n", tChannelNum+1, tEnggData);
                sleep(1);
            }
            Adc_systemMidCalibration((ChannelNum_e)tChannelNum, tData);
            printf("\n\n");

            printf("Want to Verify AI Channel:%02d calibration (Y/N)\n", tChannelNum+1);
            scanf(" %c", &option);

            if( ('N' == option) || ('n' == option))
            {
                continue;
            }
            else if( ('Y' == option) || ('y' == option))
            {
                printf("Please provide analog input to verify then enter\n\n");
                while (!kbhit());
                printf("Press enter key after verification\n\n");

                while (!kbhit())
                {
                    tData = Adc_ReadData((ChannelNum_e)tChannelNum);
                    Adc_ConvertBin2Engg((ChannelNum_e)tChannelNum, tData, &tEnggData);
                    printf("AI%02d : %3.2f\n", tChannelNum+1, tEnggData);
                    sleep(1);
                }
            }
        }
    }

    return 0;
}

int Adc_systemLowCalibration(ChannelNum_e iChannelNum, unsigned short int iData)
{
    char tag[256] = {0, };
    char tValue[256] = {0, };

    if((iChannelNum < CHANNEL_NUM_1) || (iChannelNum > CHANNEL_NUM_4))
    {
        printf("Invalid channel number (%d) at %s\n", iChannelNum, __func__ );
        return -1;
    }

    gAiCalibration[iChannelNum].mAiLowCal = iData;

    sprintf(tag, "aiChannel-%d:SysLowCal" , iChannelNum+1);
    sprintf(tValue, "%d", gAiCalibration[iChannelNum].mAiLowCal);

    Aio_ConfigurationWrite(tag, tValue);
    return 0;
}

int Adc_systemMidCalibration(ChannelNum_e iChannelNum, unsigned short int iData)
{
    char tag[256] = {0, };
    char tValue[256] = {0, };

    if((iChannelNum < CHANNEL_NUM_1) || (iChannelNum > CHANNEL_NUM_4))
    {
        printf("Invalid channel number (%d) at %s\n", iChannelNum, __func__ );
        return -1;
    }

    gAiCalibration[iChannelNum].mAiMidCal = iData;

    sprintf(tag, "aiChannel-%d:SysMidcal" , iChannelNum+1);
    sprintf(tValue, "%d", gAiCalibration[iChannelNum].mAiMidCal);

    Aio_ConfigurationWrite(tag, tValue);

    return 0;
}

int Adc_systemHighCalibration(ChannelNum_e iChannelNum, unsigned short int iData)
{
    char tag[256] = {0, };
    char tValue[256] = {0, };

    if((iChannelNum < CHANNEL_NUM_1) || (iChannelNum > CHANNEL_NUM_4))
    {
        printf("Invalid channel number (%d) at %s\n", iChannelNum, __func__ );
        return -1;
    }

    gAiCalibration[iChannelNum].mAiHighCal = iData;

    sprintf(tag, "aiChannel-%d:SysHighCal" , iChannelNum+1);
    sprintf(tValue, "%d", gAiCalibration[iChannelNum].mAiHighCal);

    Aio_ConfigurationWrite(tag, tValue);

    return 0;
}

void Adc_ConvertBin2Engg(ChannelNum_e iChannelNum, unsigned short int tAnalogDataBin, float *pAnalogDataEngg)
{
    switch(global_analog_in_configuration.mAiChannelCnfg[iChannelNum].mChannelType)
    {
        case AI_CHANNEL_TYPE_4_20mA:
        {
            if(tAnalogDataBin >= gAiCalibration[iChannelNum].mAiMidCal)
            {
                (*pAnalogDataEngg) = 10 + ((float)((tAnalogDataBin - gAiCalibration[iChannelNum].mAiMidCal) * ((20-0)/2) ) /
                                          (gAiCalibration[iChannelNum].mAiHighCal - gAiCalibration[iChannelNum].mAiMidCal));
            }
            else if(tAnalogDataBin < gAiCalibration[iChannelNum].mAiMidCal)
            {
                (*pAnalogDataEngg) = 0 + ((float)((tAnalogDataBin - gAiCalibration[iChannelNum].mAiLowCal) * ((20-0)/2) ) /
                                          (gAiCalibration[iChannelNum].mAiMidCal - gAiCalibration[iChannelNum].mAiLowCal));
            }
            break;
        }
        case AI_CHANNEL_TYPE_BIPOLAR_10V:
        {
            if(tAnalogDataBin > gAiCalibration[iChannelNum].mAiMidCal)
            {
                (*pAnalogDataEngg) = ((float)((tAnalogDataBin - gAiCalibration[iChannelNum].mAiMidCal) * (10-0))) /
                                     (gAiCalibration[iChannelNum].mAiHighCal - gAiCalibration[iChannelNum].mAiMidCal);
            }
            else if(tAnalogDataBin < gAiCalibration[iChannelNum].mAiMidCal)
            {
                (*pAnalogDataEngg) = 0 - (((float)((gAiCalibration[iChannelNum].mAiMidCal - tAnalogDataBin) * (10-0))) /
                                          (gAiCalibration[iChannelNum].mAiMidCal - gAiCalibration[iChannelNum].mAiLowCal));
            }
            else
            {
                (*pAnalogDataEngg) = 0;
            }
            break;
        }
        default:
        {
            printf("Invalid channel:%d type:%c at %s\n", iChannelNum, global_analog_in_configuration.mAiChannelCnfg[iChannelNum].mChannelType, __func__ );
        }
    }
}

struct termios orig_termios;

void reset_terminal_mode()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
    struct termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);
    cfmakeraw(&new_termios);
    tcsetattr(0, TCSANOW, &new_termios);
}

int kbhit()
{
    int tRetVal ;
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    char buf[256] = {0, };

    FD_ZERO(&fds);
    FD_SET(0, &fds);

    tRetVal = select(0+1, &fds, NULL, NULL, &tv);
    if (FD_ISSET(0, &fds))
    {
        FD_CLR(0, &fds);
        read(0, &buf, sizeof(buf));
        lseek(0, 0, SEEK_SET);
    }

    return tRetVal;
}

int getch()
{
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}


