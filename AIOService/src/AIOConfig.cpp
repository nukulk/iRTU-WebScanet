//
// Created by mukeshp on 31-Jul-20.
//
#include "AIOConfig.hpp"
#include "AICalibration.hpp"
#include "AOCalibration.hpp"
#include "iniparser.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

extern AiCalibration_t gAiCalibration[];
extern AoCalibration_t gAoCalibration[];
AiCnfg_t    global_analog_in_configuration;
AoCnfg_t    global_analog_out_configuration;

int Aio_ConfigurationRead(void) {
    dictionary *tpIni = NULL;
    char *tpIniName = NULL;
    const char *tpName = NULL;
    char *tmpPtr = NULL;
    int tRetCode = -1;
    char tCnfgFileName[512] = {0,};
    char tag[256] = {0,};

    sprintf(tCnfgFileName, "%s/%s", AIO_CNFG_FILE_PATH, AIO_CNFGL_FILE);

    tpIniName = tCnfgFileName;

    errno = 0;
    tpIni = iniparser_load(tpIniName);
    printf("%s\n", strerror(errno));
    if (tpIni == NULL) {
        printf("Error in opening file %s\n", tpIniName);
        return tRetCode;
    }

    do {
        tpName = iniparser_getstring((const dictionary *) tpIni, "aiCnfg:NumOfChannels", (const char *) tmpPtr);
        if (tpName == NULL) {
            printf("iniparser_getstring Failed For getting aiCnfg:NumOfChannels\n");
            //tRetCode = -1;
            //break;
        } else {
            global_analog_in_configuration.mNumOfCnannel = atoi(tpName);
        }

        for (int tChannelNum = CHANNEL_NUM_1; tChannelNum <= CHANNEL_NUM_4; ++tChannelNum) {
            sprintf(tag, "aiChannel-%d:Enable", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                global_analog_in_configuration.mAiChannelCnfg[tChannelNum].mIsChannelEnable = atoi(tpName);
            }

            sprintf(tag, "aiChannel-%d:ChannelType", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                global_analog_in_configuration.mAiChannelCnfg[tChannelNum].mChannelType = tpName[0];
            }

            sprintf(tag, "aiChannel-%d:SysLowCal", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                gAiCalibration[tChannelNum].mAiLowCal = atoi(tpName);
            }

            sprintf(tag, "aiChannel-%d:SysMidcal", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                gAiCalibration[tChannelNum].mAiMidCal = atoi(tpName);
            }

            sprintf(tag, "aiChannel-%d:SysHighCal", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                gAiCalibration[tChannelNum].mAiHighCal = atoi(tpName);
            }
        }

        tpName = iniparser_getstring((const dictionary *) tpIni, "aoCnfg:NumOfChannels", (const char *) tmpPtr);
        if (tpName == NULL) {
            printf("iniparser_getstring Failed For getting aoCnfg:NumOfChannels\n");
            //tRetCode = -1;
            //break;
        } else {
            global_analog_out_configuration.mNumOfChannel = atoi(tpName);
        }

        for (int tChannelNum = AO_CHANNEL_NUM_1; tChannelNum <= AO_CHANNEL_NUM_2; ++tChannelNum)
        {
            sprintf(tag, "aoChannel-%d:Enable", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                global_analog_out_configuration.mAoChannelCnfg[tChannelNum].mIsChannelEnable = atoi(tpName);
            }

            sprintf(tag, "aoChannel-%d:SysLowCal", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                gAoCalibration[tChannelNum].mAoLowCal = atoi(tpName);
            }

            sprintf(tag, "aoChannel-%d:SysMidcal", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                gAoCalibration[tChannelNum].mAoMidCal = atoi(tpName);
            }

            sprintf(tag, "aoChannel-%d:SysHighCal", tChannelNum + 1);
            tpName = iniparser_getstring((const dictionary *) tpIni, tag, (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting %s\n", tag);
                //tRetCode = -1;
                //break;
            } else {
                gAoCalibration[tChannelNum].mAoHighCal = atoi(tpName);
            }
        }

        tRetCode = 0;
    } while (0);

    iniparser_freedict(tpIni);

    printf("Num of AI channel : %d\n", global_analog_in_configuration.mNumOfCnannel);

    for (int tChannelNum = CHANNEL_NUM_1; tChannelNum <= CHANNEL_NUM_4; ++tChannelNum)
    {
        printf("AI-%02d channel : %s\n", tChannelNum+1, global_analog_in_configuration.mAiChannelCnfg[tChannelNum].mIsChannelEnable ? "ENABLE" : "DISABLE");
        printf("AI-%02d channel : %c\n", tChannelNum+1, global_analog_in_configuration.mAiChannelCnfg[tChannelNum].mChannelType);
        printf("AI-%02d channel(SYS_LOW_CAL)  : %d\n", tChannelNum+1, gAiCalibration[tChannelNum].mAiLowCal);
        printf("AI-%02d channel(SYS_MID_CAL)  : %d\n", tChannelNum+1, gAiCalibration[tChannelNum].mAiMidCal);
        printf("AI-%02d channel(SYS_HIGH_CAL) : %d\n", tChannelNum+1, gAiCalibration[tChannelNum].mAiHighCal);
    }

    printf("Num of AO channel : %d\n", global_analog_out_configuration.mNumOfChannel);

    for (int tChannelNum = AO_CHANNEL_NUM_1; tChannelNum <= AO_CHANNEL_NUM_2; ++tChannelNum)
    {
        printf("AO-%02d channel : %s\n", tChannelNum+1, global_analog_out_configuration.mAoChannelCnfg[tChannelNum].mIsChannelEnable ? "ENABLE" : "DISABLE");
        printf("AO-%02d channel(SYS_LOW_CAL)  : %d\n", tChannelNum+1, gAoCalibration[tChannelNum].mAoLowCal);
        printf("AO-%02d channel(SYS_MID_CAL)  : %d\n", tChannelNum+1, gAoCalibration[tChannelNum].mAoMidCal);
        printf("AO-%02d channel(SYS_HIGH_CAL) : %d\n", tChannelNum+1, gAoCalibration[tChannelNum].mAoHighCal);
    }

    return tRetCode;
}

int WriteToIniFile(dictionary *ini , char *config_file_name)
{
    FILE *Ini_Fp = NULL;

    if((Ini_Fp = fopen(config_file_name, "w+")) == NULL )
    {
        printf("Ini file not present\n");
        return -1;
    }

    iniparser_dump_ini(ini, Ini_Fp);

    if(Ini_Fp != NULL)
    {
        fclose(Ini_Fp);
    }

    return 0;
}

int Aio_ConfigurationWrite(const char *pTag, const char *pValue)
{
    dictionary  *ini  = NULL;
    char    *ini_name = NULL ;
    char    config_file_name[100] = {0 ,} ;
    int  	ret = -1;
    int 	rc = -1;
    char 	tag_dc[100] 	= {0,};
    char 	value_dc[100] 	= {0,};

    if((NULL == pTag) || (NULL == pValue))
    {
        printf("Error ptag:%p or pValue:%p at %s\n", __func__ );
        return -1;
    }

    sprintf(config_file_name,"%s/%s",AIO_CNFG_FILE_PATH, AIO_CNFGL_FILE);
    ini_name = config_file_name ;

    ini = iniparser_load(ini_name);
    if(ini == NULL)
    {
        printf("Error in opening file %s", ini_name);
        return -1;
    }

    printf("tag:%s  value:%s\n", pTag, pValue);

    do {
        ret = iniparser_set(ini, pTag, pValue);
        if (ret != 0)
        {
            printf("Error iniparser_set  to write dc value");
            rc = -1;
            break;
        }

        ret = WriteToIniFile(ini, config_file_name);
        if (ret != 0)
        {
            printf("Error at to write dc value in ini file");
            rc = -1;
            break;
        }

        rc = 0;
    }while (0);

    iniparser_freedict(ini);
    return rc;
}


