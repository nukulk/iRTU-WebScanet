//
// Created by mukeshp on 10-Aug-20.
//
#include "AOCalibration.hpp"
#include "AIOConfig.hpp"
#include "AnalogOut.hpp"

#include <stdio.h>
#include <unistd.h>

AoCalibration_t gAoCalibration[AO_CHANNEL_MAX];
extern AoCnfg_t global_analog_out_configuration;

int Dac_calibration(void)
{
    unsigned short int tData = 0;

    for(int tChannelNum = AO_CHANNEL_NUM_1 ; tChannelNum <= AO_CHANNEL_NUM_2 ; ++tChannelNum)
    {
        AoChannelCnfg_t *pAochannelCnfg = &global_analog_out_configuration.mAoChannelCnfg[tChannelNum];

        if(AO_CHANNEL_ENABLE == pAochannelCnfg->mIsChannelEnable)
        {
            char option;
            printf("Want to perform calibration for DAC channel:%02d(Y/N)\n", tChannelNum+1);
            scanf(" %c", &option);

            if( ('N' == option) || ('n' == option))
            {
                continue;
            }

            do
            {
                printf("Please give system low count for channel:%02d then enter\n", tChannelNum+1);
                scanf("%d", &tData);

                Dac_write(GET_AO_CHANNEL_ADDR(tChannelNum), tData);

                printf("Want to perform system low calibration again(Y/N)\n");
                scanf(" %c", &option);

                if( ('N' == option) || ('n' == option))
                {
                    Dac_systemLowCalibration(tChannelNum, tData);
                    break;
                }
            }while (1);

            do
            {
                printf("Please give system high count for channel:%02d then enter\n", tChannelNum+1);
                scanf("%d", &tData);

                Dac_write(GET_AO_CHANNEL_ADDR(tChannelNum), tData);

                printf("Want to perform system high calibration again(Y/N)\n");
                scanf(" %c", &option);

                if( ('N' == option) || ('n' == option))
                {
                    Dac_systemHighCalibration(tChannelNum, tData);
                    break;
                }
            }while (1);

            do
            {
                printf("Please give system Mid count for channel:%02d then enter\n", tChannelNum+1);
                scanf("%d", &tData);

                Dac_write(GET_AO_CHANNEL_ADDR(tChannelNum), tData);

                printf("Want to perform system mid calibration again(Y/N)\n");
                scanf(" %c", &option);

                if( ('N' == option) || ('n' == option))
                {
                    Dac_systemMidCalibration(tChannelNum, tData);
                    break;
                }
            }while (1);

            printf("AO Calibration Completed for channel:%02d\n\n", tChannelNum+1);

            do
            {
                int tVoltInmV = 0;

                printf("Want to Verify AO Channel:%02d calibration (Y/N)\n", tChannelNum+1);
                scanf(" %c", &option);

                if( ('N' == option) || ('n' == option))
                {
                    break;
                }
                else if( ('Y' == option) || ('y' == option))
                {
                    printf("Enter Voltage(in mV) to verify: \n");
                    scanf("%d", &tVoltInmV);

                    Dac_write(GET_AO_CHANNEL_ADDR(tChannelNum), GetDacCount(tChannelNum, tVoltInmV));
                    printf("\nPlease Verify output voltage\n\n");
                    sleep(2);
                }
            }while (1);

            printf("Want to perform calibration for channel: %02d again(Y/N)\n", tChannelNum+1);
            scanf(" %c", &option);

            if((option == 'Y') || (option == 'y'))
            {
                tChannelNum -= 1;
            }
        }
        printf("AO channel:%02d Calibration Completed!!!\n", tChannelNum+1);
    }

    printf("AO Calibration Completed!!!\n");
    return 0;
}

int Dac_systemLowCalibration(int iChannelNum, unsigned short int iData)
{
    char tag[256] = {0, };
    char tValue[256] = {0, };

    if((iChannelNum != 0) && (iChannelNum != 1))
    {
        printf("Error at Invalid channel num (%d) at %s\n", iChannelNum, __func__ );
        return -1;
    }

    gAoCalibration[iChannelNum].mAoLowCal = iData;

    sprintf(tag, "aoChannel-%d:SysLowCal" , iChannelNum+1);
    sprintf(tValue, "%d", gAoCalibration[iChannelNum].mAoLowCal);

    Aio_ConfigurationWrite(tag, tValue);

    return 0;
}

int Dac_systemMidCalibration(int iChannelNum, unsigned short int iData)
{
    char tag[256] = {0, };
    char tValue[256] = {0, };

    if((iChannelNum != 0) && (iChannelNum != 1))
    {
        printf("Error at Invalid channel num (%d) at %s\n", iChannelNum, __func__ );
        return -1;
    }

    gAoCalibration[iChannelNum].mAoMidCal = iData;

    sprintf(tag, "aoChannel-%d:SysMidcal" , iChannelNum+1);
    sprintf(tValue, "%d", gAoCalibration[iChannelNum].mAoMidCal);

    Aio_ConfigurationWrite(tag, tValue);

    return 0;
}

int Dac_systemHighCalibration(int iChannelNum, unsigned short int iData)
{
    char tag[256] = {0, };
    char tValue[256] = {0, };

    if((iChannelNum != 0) && (iChannelNum != 1))
    {
        printf("Error at Invalid channel num (%d) at %s\n", iChannelNum, __func__ );
        return -1;
    }

    gAoCalibration[iChannelNum].mAoHighCal = iData;

    sprintf(tag, "aoChannel-%d:SysHighCal" , iChannelNum+1);
    sprintf(tValue, "%d", gAoCalibration[iChannelNum].mAoHighCal);

    Aio_ConfigurationWrite(tag, tValue);

    return 0;
}

int GetDacCount(int iChannelNum, int iVolInmV)
{
    int tCnt = 0;

    if((iVolInmV < 0) || (iVolInmV > 10000))
    {
        printf("Error iVolInmV(%s) is invalid at %s\n", iVolInmV, __func__ );
        return -1;
    }

    if(iVolInmV < 5000)
    {
        tCnt = gAoCalibration[iChannelNum].mAoLowCal + \
                (((float)(gAoCalibration[iChannelNum].mAoMidCal - gAoCalibration[iChannelNum].mAoLowCal) / 5000) * iVolInmV);
    }
    else if(iVolInmV > 5000)
    {
        //tCnt = gAoCalibration[iChannelNum].mAoMidCal + \
                (((float)(gAoCalibration[iChannelNum].mAoHighCal - gAoCalibration[iChannelNum].mAoMidCal) / 5000) * (iVolInmV/2));
        tCnt = gAoCalibration[iChannelNum].mAoLowCal + \
                (((float)(gAoCalibration[iChannelNum].mAoHighCal - gAoCalibration[iChannelNum].mAoMidCal) / 5000) * iVolInmV);
    }
    else
    {
        tCnt = gAoCalibration[iChannelNum].mAoMidCal;
    }

    printf("DAC: InputVoltage:%d mV -> Cnt:%d\n", iVolInmV, tCnt);

    return (tCnt);
}
