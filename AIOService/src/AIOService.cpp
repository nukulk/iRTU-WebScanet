#include "AIOService.hpp"
#include "AnalogIn.hpp"
#include "AnalogOut.hpp"
#include "AIOConfig.hpp"
#include "AICalibration.hpp"
#include "AOCalibration.hpp"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <MessageBus/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>

#include <string>
#include <thread>
#include <chrono>

static void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void OnSignalReceived(SignalType type);

static AIOService *appptr = nullptr;

AIOService::AIOService()
{
    message_bus = nullptr;
    logger = nullptr;
    config = nullptr;
    appptr = this;
}

AIOService::~AIOService()
{

}

bool AIOService::Initialize()
{
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        return false;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == nullptr)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    std::string destinations;
    configuration_get_value_as_string(config, "default", "destination", destinations);
    str_ex_split_with_char(destinations, destination_list, ',');

    configuration_release(config);

    return true;
}

bool AIOService::Destroy()
{
    return true;
}

bool AIOService::Start()
{
    if(!message_bus_initialize(&message_bus, OnNetworkEvent))
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    int ctr = 0;

    bool continue_loop = true;

    while(continue_loop)
    {
        sleep(5);

        std::string payload;

        for(int tChNum = CHANNEL_NUM_1 ; tChNum < global_analog_in_configuration.mNumOfCnannel ; ++tChNum )
        {
            float tEnggData = 0;
            unsigned short int tBinData = 0;

            tBinData = Adc_ReadData((ChannelNum_e)tChNum);
            Adc_ConvertBin2Engg((ChannelNum_e)tChNum, tBinData, &tEnggData);

            printf("CH%02d : 0x%04X, %3.2f\n", tChNum+1, tBinData, tEnggData);

            sleep(1);
        }

        for(int tChNum = 0 ; tChNum < global_analog_out_configuration.mNumOfChannel ; ++tChNum)
        {
            //Dac_write(GET_AO_CHANNEL_ADDR(tChNum), 0xFFF);
            Dac_write(GET_AO_CHANNEL_ADDR(tChNum), GetDacCount(tChNum, 10000));

            sleep(5);

            //Dac_write(GET_AO_CHANNEL_ADDR(tChNum), 0x7FF);
            Dac_write(GET_AO_CHANNEL_ADDR(tChNum), GetDacCount(tChNum, 5000));

            sleep(5);

            //Dac_write(GET_AO_CHANNEL_ADDR(tChNum), 0x000);
            Dac_write(GET_AO_CHANNEL_ADDR(tChNum), GetDacCount(tChNum, 0));
        }

        for(auto destination : destination_list)
        {
            if(!message_bus_send(message_bus, destination.c_str(), Data, UserData, Text, payload.c_str(), payload.length()))
            {
                continue_loop = false;
                break;
            }
        }

        ctr++;
    }

    return true;
}

bool AIOService::Restart()
{
    return false;
}

bool AIOService::Stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    destination_list.clear();
    logger_release(logger);

    return false;
}

void AIOService::CalibrateADC()
{

}

void AIOService::CalibrateDAC()
{

}

void* AIOService::GetLogger()
{
    return logger;
}

void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    if(ptype == Data || mtype == LoopBack)
    {
        WriteInformation(appptr->GetLogger(), "Loopback successfull");
    }

    //We assume Text and Userdata for data type and message type
    if(ptype == Request)
    {
        char error_message[64];

        std::vector<std::string> command_lines;

        str_ex_split_with_str(messagebuffer, command_lines, "\r\n");

        std::string command_name = command_lines[0];
        std::string command_string = command_lines[1];

        //Ignore the command name, assume that we need to write out

        std::vector<std::string> command_values;

        //Make a list of CHANNEL:ENABLE:TYPE:LOW:MID:HIGH combinations
        str_ex_split_with_char(command_string, command_values, ',');

        for(auto kv : command_values)
        {
            std::vector<std::string> calibration_args;
            str_ex_split_with_char(kv, calibration_args, ':');

            if(calibration_args.size() != 6)
            {
                continue;
            }

            // String is composed as PIN:SIGNAL
            int channel = atoi(calibration_args[0].c_str());
            int enabled = atoi(calibration_args[1].c_str());
            int aiotype = atoi(calibration_args[2].c_str());
            int low = atoi(calibration_args[3].c_str());
            int mid = atoi(calibration_args[4].c_str());
            int high = atoi(calibration_args[5].c_str());

            memset(error_message, 0, sizeof (error_message));

            if(command_name == "CALIBRATE_DAC")
            {
                Dac_calibration();
            }
            else
            {
                if(command_name == "CALIBRATE_ADC")
                {
                    Adc_calibration();
                }
            }
        }
    }

}

void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(appptr->GetLogger(), "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(appptr->GetLogger(), "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(appptr->GetLogger(), "SHUTDOWN SIGNAL", LOG_CRITICAL);
            appptr->Stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(appptr->GetLogger(), "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(appptr->GetLogger(), "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(appptr->GetLogger(), "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(appptr->GetLogger(), "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(appptr->GetLogger(), "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
