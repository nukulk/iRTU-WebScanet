//
// Created by mukeshp on 5/19/2020.
//
#include "I2CInterface.hpp"

int I2C_Open(char *pDeviceName)
{
    int tFd = -1 ;
    int adapter_nr = 2; /* probably dynamically determined */
    char tFileName[64] = {0,};

    //sprintf(tFileName, "/dev/i2c-%d", adapter_nr);
    //tFd = open (tFileName, O_RDWR);
    tFd = open (pDeviceName, O_RDWR);
    if (tFd < 0)
    {
        /* ERROR HANDLING; you can check errno to see what went wrong */
        printf("Error to open %s device at I2C_Open\n", pDeviceName);
        //exit(1);
        return -1;
    }

    return tFd;
}

int I2C_Write(int iFd, char *pBuf, unsigned int iBufLen)
{
    int tWrittenBytes = -1;

    tWrittenBytes = write(iFd, pBuf, iBufLen);
    if(tWrittenBytes < 0)
    {
        /* ERROR HANDLING: i2c transaction failed */
        printf("Error at I2C_Write(%s)\n", strerror(errno));
    }
    else
    {
        //printf("Success at I2C_Write\n");
    }

    return tWrittenBytes;
}

int I2C_Read(int iFd, char *pBuf, unsigned int iBufLen)
{
    int tReadBytes = -1;

    /* Using I2C Read */
    tReadBytes = read(iFd, pBuf, iBufLen);
    if(tReadBytes < 0)
    {
        /* ERROR HANDLING: i2c transaction failed */
        printf("Error at I2C_Read\n");
    }
    else
    {
        /* buf[0] contains the read byte */
        //printf("I2C Read success \n");
    }

    return tReadBytes;
}

void I2C_Close(int iFd)
{
    close(iFd);
    //printf("I2C Close executed\n");
}
