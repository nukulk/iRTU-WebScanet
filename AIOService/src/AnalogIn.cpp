//
// Created by mukeshp on 5/21/2020.
//

#include "AnalogIn.hpp"
#include "SPIInterface.hpp"
#include "AIOConfig.hpp"

extern AiCnfg_t    global_analog_in_configuration;

int Adc_init(void)
{
    unsigned long int tDeviceId;
    unsigned long int tDataOutGetMode;
    unsigned long int tSdiMode;
    unsigned long int tSdoMode;
    unsigned long int tGetRangeSelect;
    unsigned long int tGetAlarmHighThresold;
    unsigned long int tGetAlarmLowThresold;
    unsigned long int tGetPowerCntlReg;

    /* ADC : Channel selection pin configure */
    Adc_IOPinConfigure();

    /* ADC : Interrupt pin configure */
    Adc_InterruptPinConfigure();

    /* ADC : Get Device Id */
    AdcGetDeviceIdentification(&tDeviceId);

    /* ADC : Set output conversion data mode  */
    //AdcSetDataOutMode(DEVICE_ADDR_INCL | SELECTED_RANGE_INCL | PARITY_EN);
    AdcSetDataOutMode(0);
    AdcGetDataOutMode(&tDataOutGetMode);

    /* ADC : Set SDI mode */
    AdcSetSdiMode(tSdiMode);
    AdcGetSdiMode(&tSdiMode);

    /* ADC : Set SDO mode */
    AdcSetSdoMode(tSdiMode);
    AdcGetSdoMode(&tSdoMode);

    /* ADC : Set reference voltage, and input voltage range selection */
    AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, VOLTAGE_RANGE_BIPOLAR_2_5V);
    //AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, VOLTAGE_RANGE_UNIPOLAR_1_25V);
    AdcGetRangeReferenceVoltage(&tGetRangeSelect);

    /* Misc */
    AdcGetAlarmHighThresoldRegister(&tGetAlarmHighThresold);

    AdcGetAlarmLowThresoldRegister(&tGetAlarmLowThresold);

    AdcGetResetPowerCntrlReg(&tGetPowerCntlReg);

    return 0;
}

void* AnalogIn_Thread(void *arg)
{
    unsigned long int tDeviceId = 0;
    int tFd = -1;
    unsigned char tConvertedData[4] = {0, };
    unsigned long int tPowerCntlReg;
    unsigned long int tSdiMode = 0xFFFFFFFF;
    unsigned long int tRangeSelect;
    unsigned long int tDataOutGetMode = 0xFFFFFFFF;
    unsigned long int tDataOutSetMode = 0;
    unsigned long int tSdoMode;

    pthread_detach(pthread_self());

    Adc_IOPinConfigure();

    AdcGetDeviceIdentification(&tDeviceId);

    AdcGetDataOutMode(&tDataOutGetMode);

    AdcGetSdiMode(&tSdiMode);

    AdcGetSdoMode(&tSdoMode);

    AdcGetResetPowerCntrlReg(&tPowerCntlReg);

    AdcGetRangeReferenceVoltage(&tRangeSelect);

    unsigned char temp = 0x0B;

    AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, temp);

    AdcGetRangeReferenceVoltage(&tRangeSelect);

    unsigned long int tAlarmHighThresold = 0;
    unsigned long int tAlarmLowThresold = 0;

    AdcGetAlarmHighThresoldRegister(&tAlarmHighThresold);

    AdcGetAlarmLowThresoldRegister(&tAlarmLowThresold);


    //AdcRangeReferenceVoltageSelection(INT_REF_VOLTAGE, 0x00);
    //AdcSetSdoMode(0x00);
    AdcSetDataOutMode(DEVICE_ADDR_INCL | SELECTED_RANGE_INCL | PARITY_EN);
    //AdcSetDataOutMode(0);

    AdcGetDataOutMode(&tDataOutGetMode);


    //AdcChannelSelection(4);

    while (1)
    {
        //Start_Conversion();

        //delay
        //sleep(1); //Minimum we can keep 15ms.
        usleep(5);
        //Stop_Conversion();
        //Read Data

        tFd = SpiOpen(ANALOG_IN_DEVICE);
        if(tFd < 0)
        {
            printf("Error to open %s\n", ANALOG_IN_DEVICE);
        }

        SpiDataTransfer(tFd, (unsigned char*)tConvertedData, 4);

        SpiClose(tFd);



        unsigned long int tData = (tConvertedData[3] << 24) | (tConvertedData[2] << 16) |
                                    (tConvertedData[1] << 8) | tConvertedData[0];
        printf("Converted Data:0x%04X\n", tData);

        break;
    }

    pthread_exit(NULL);
}

/**
 *
 * */
int AdcGetDeviceIdentification(unsigned long *pDeviceId)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    if(NULL == pDeviceId)
    {
        printf("pDeviceId is null at %s\n", __func__ );
        return -1;
    }

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DEVICE_ID_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4,(unsigned char *) &tRdData[0], 2) < 0) {
            printf("Error to SpiRead at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DEVICE_ID_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4,(unsigned char *) &tRdData[2], 2) < 0) {
            printf("Error to SpiRead at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(tFd);

    memcpy(pDeviceId, &tRdData, 4);

    printf("Device Id: 0x%08X\n", (*pDeviceId));

    return (tRetVal);
}

/** AdcRangeReferenceVoltageSelection
 * iRefSel : 0 - Internal reference voltage, 1 - External reference voltage.
 * iRangeVoltage :  0000b (0x00) = ±3 × VREF
 *                  0001b (0x01) = ±2.5 × VREF
 *                  0010b (0x02) = ±1.5 × VREF
 *                  0011b (0x03) = ±1.25 × VREF
 *                  0100b (0x04) = ±0.625 × VREF
 *                  1000b (0x08) = 3 × VREF
 *                  1001b (0x09) = 2.5 × VREF
 *                  1010b (0x0A) = 1.5 × VREF
 *                  1011b (0x0B) = 1.25 × VREF
 * */
int AdcSetRangeReferenceVoltage(unsigned char iRefSel, unsigned char iRangeVoltage)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned short int tData = 0;
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tData = (iRefSel << 6) | iRangeVoltage;

        tWrData[0] = ADS8689_WRITE_FULL << 1;
        tWrData[1] = ADS8689_RANGE_SEL_REG;
        tWrData[2] = (unsigned char) (tData >> 8);
        tWrData[3] = (unsigned char) tData;

        if (SpiWrite(tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            printf("Error to SpiWrite-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }
    }while (0);

    SpiClose(tFd);

    return (tRetVal);
}

int AdcGetRangeReferenceVoltage(unsigned long *pRangeSel)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE,  __func__ );
        return -1;
    }

    do {
        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_RANGE_SEL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0) {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_RANGE_SEL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0) {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(tFd);

    memcpy(pRangeSel, &tRdData, 4);

    printf("pRangeSel: 0x%08X\n", (*pRangeSel));

    return (tRetVal);
}

int AdcGetAlarmHighThresoldRegister(unsigned long *pAlarmHighThresold)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_H_TH_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_H_TH_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(tFd);

    memcpy(pAlarmHighThresold, &tRdData, 4);

    printf("pAlarmHighThresold: 0x%08X\n", (*pAlarmHighThresold));

    return (tRetVal);
}

int AdcGetAlarmLowThresoldRegister(unsigned long *pAlarmLowThresold)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0, };

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do {

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_L_TH_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0) {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = (ADS8689_READ_HWORD << 1);
        tWrData[1] = ADS8689_ALARM_L_TH_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0) {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(tFd);

    memcpy(pAlarmLowThresold, &tRdData, 4);

    printf("pAlarmLowThresold: 0x%08X\n", (*pAlarmLowThresold));

    return (tRetVal);
}

int AdcGetResetPowerCntrlReg(unsigned long *pPowerCntrlreg)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    if(NULL == pPowerCntrlreg)
    {
        printf("pPowerCntrlreg is null at %s\n", __func__ );
        return -1;
    }

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_RST_PWRCTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, (unsigned char *) &tRdData[0], 2) < 0)
        {
            printf("Error to SpiRead at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[2]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_RST_PWRCTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, (unsigned char *) &tRdData[2], 2) < 0)
        {
            printf("Error to SpiRead at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(tFd);

    memcpy(pPowerCntrlreg, &tRdData, 4);

    printf("RST Power CNTL REG: 0x%08X\n", (*pPowerCntrlreg));

    return (tRetVal);
}

/* iSdiMode :   0 - Standard SPI with CPOL = 0 and CPHASE = 0
 *              1 - Standard SPI with CPOL = 0 and CPHASE = 1
 *              2 - Standard SPI with CPOL = 1 and CPHASE = 0
 *              3 - Standard SPI with CPOL = 1 and CPHASE = 1
 * */
int AdcSetSdiMode(unsigned char iSdiMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned long int tData = 0;
    unsigned char tWrData[4] = {0, };

    tData = iSdiMode;

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_WRITE_FULL << 1;
        tWrData[1] = ADS8689_SDI_CTL_REG;
        tWrData[2] = (unsigned char )(tData >> 8);
        tWrData[3] = (unsigned char )tData;

        if (SpiWrite(tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            printf("Error to SpiWrite-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

    }while (0);

    SpiClose(tFd);

    return (tRetVal);
}

int AdcGetSdiMode(unsigned long *pSdiMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDI_CTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDI_CTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);

    }while (0);

    SpiClose(tFd);

    memcpy(pSdiMode, &tRdData, 4);

    printf("SDI MODE: %X\n", (*pSdiMode));

    return 0;
}

int AdcGetSdoMode(unsigned long *pSdoMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s)  at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDO_CTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_SDO_CTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3])

    }while (0);

    SpiClose(tFd);

    memcpy(pSdoMode, &tRdData, 4);

    printf("SDO MODE: %X\n", (*pSdoMode));

    return (tRetVal);
}

int AdcSetSdoMode(unsigned char iSdoMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned long int tData = 0;
    unsigned char tWrData[4] = {0, };

    tData = iSdoMode;

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen(%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_WRITE_FULL << 1 ;
        tWrData[1] = ADS8689_SDO_CTL_REG;
        tWrData[2] = (unsigned char )(tData >> 8);
        tWrData[3] = (unsigned char )tData;

        if (SpiWrite(tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            printf("Error to SpiWrite-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

    }while (0);

    SpiClose(tFd);

    return (tRetVal);
}

int AdcSetDataOutMode(unsigned int iDataOutMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned long int tData = iDataOutMode;
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen (%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_WRITE_FULL << 1;
        tWrData[1] = ADS8689_DATAOUT_CTL_REG;
        tWrData[2] = (unsigned char )(tData >> 8);
        tWrData[3] = (unsigned char )tData;

        if (SpiWrite(tFd, (unsigned char *) tWrData, 4, false) < 0)
        {
            printf("Error to SpiWrite-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

    }while(0);

    SpiClose(tFd);

    return (tRetVal);
}

int AdcGetDataOutMode(unsigned long int *pDataOutMode)
{
    int tRetVal = 0;
    int tFd = -1;
    unsigned char tRdData[4] = {0,};
    unsigned char tWrData[4] = {0,};

    tFd = SpiOpen(ANALOG_IN_DEVICE);
    if(tFd < 0)
    {
        printf("Error to SpiOpen (%s) at %s\n", ANALOG_IN_DEVICE, __func__ );
        return -1;
    }

    do
    {
        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DATAOUT_CTL_REG;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[0], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[0], tRdData[1]);

        tWrData[0] = ADS8689_READ_HWORD << 1;
        tWrData[1] = ADS8689_DATAOUT_CTL_REG + 2;
        tWrData[2] = 0;
        tWrData[3] = 0;

        if (SpiRead(tFd, (unsigned char *) tWrData, 4, &tRdData[2], 2) < 0)
        {
            printf("Error to SpiRead-Reg Address at %s\n", __func__);
            tRetVal = -1;
            break;
        }

        SWAP_BYTES(tRdData[2], tRdData[3]);
    }
    while (0);

    SpiClose(tFd);

    memcpy(pDataOutMode, &tRdData, 4);

    printf("DATA_OUT MODE: 0x%08X\n", (*pDataOutMode));

    return (tRetVal);
}

int AdcChannelSelection(ChannelNum_e iChNum)
{
    #if 0
    if((iChNum >= CHANNEL_NUM_1) && (iChNum <= CHANNEL_NUM_4))
    {
        /* Channel Range selection : Current channel : 0 to 5.12 V, Voltage channel: -10V to +10V */
        if(gAiCnfg.mAiChannelCnfg[iChNum].mChannelType == AI_CHANNEL_TYPE_BIPOLAR_10V)
        {
            AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, VOLTAGE_RANGE_BIPOLAR_2_5V);
        }
        else
        {
            AdcSetRangeReferenceVoltage(INT_REF_VOLTAGE, VOLTAGE_RANGE_UNIPOLAR_1_25V);
        }
    }
    #endif

    switch (iChNum)
    {
        case CHANNEL_NUM_1:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_NUM_2:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_NUM_3:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_NUM_4:
        {
            /* EN_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio57/value");
            /* A0_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio191/value");
            /* A1_MUX = 1; */
            system("echo \"1\" > /sys/class/gpio/gpio52/value");
            break;
        }
        case CHANNEL_DISABLE_ALL:
        {
            /* EN_MUX = 0; */
            system("echo \"0\" > /sys/class/gpio/gpio57/value");
        }
        default:
        {
            printf("Invalid analog channel number(%d)\n", iChNum);
        }
    }

    usleep(50000); /* 50ms Delay after channel selection */
    return 0;
}

#if 0
void* AdcRvsInterrupt_Thread(void *arg)
{
    char gpio_irq[64];
    int ret, irqfd = 0, i = 0;
    fd_set fds;
    FD_ZERO(&fds);
    int buf;

    pthread_detach(pthread_self());


    snprintf(gpio_irq, sizeof(gpio_irq), "/sys/class/gpio/gpio%d/value", 49);
    irqfd = open(gpio_irq, O_RDONLY, S_IREAD);

    if(irqfd == -1) {
        printf("Could not open IRQ %s\n", 49);
        printf("Make sure the GPIO is already exported", 49);
        return NULL;
    }

    // Read first since there is always an initial status
    ret = read(irqfd, &buf, sizeof(buf));

    while(1) {
        FD_SET(irqfd, &fds);
        // See if the IRQ has any data available to read
        ret = select(irqfd + 1, NULL, NULL, &fds, NULL);

        if(FD_ISSET(irqfd, &fds))
        {
            FD_CLR(irqfd, &fds);  //Remove the filedes from set
            //printf("IRQ detected %d\n", i);
            fflush(stdout);
            i++;

            /* The return value includes the actual GPIO register value */
            read(irqfd, &buf, sizeof(buf));
            lseek(irqfd, 0, SEEK_SET);
        }

        //Sleep, or do any other processing here
        //usleep(100000);
    }

    pthread_exit(NULL);
}
#endif

void Adc_IOPinConfigure(void)
{
    int tFdEnMux = -1;
    int tFdA0Mux = -1;
    int tFdA1Mux = -1;

    tFdEnMux = open("/sys/class/gpio/gpio57/direction", O_RDWR);
    if (tFdEnMux < 0)
    {
        //if not exported already than export it
        system("echo \"57\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdEnMux);
    }
    system("echo \"out\" > /sys/class/gpio/gpio57/direction");

    tFdA0Mux = open("/sys/class/gpio/gpio191/direction", O_RDWR);
    if (tFdA0Mux < 0)
    {
        //if not exported already than export it
        system("echo \"191\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdA0Mux);
    }
    system("echo \"out\" > /sys/class/gpio/gpio191/direction");

    tFdA1Mux = open("/sys/class/gpio/gpio52/direction", O_RDWR);
    if (tFdA1Mux < 0)
    {
        //if not exported already than export it
        system("echo \"52\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdA1Mux);
    }
    system("echo \"out\" > /sys/class/gpio/gpio52/direction");

    return;
}

void Adc_InterruptPinConfigure(void)
{
    int tFdAdcRvs = -1;
    char tgpioirq[256] = {0,};
    char tCommand[256] = {0,};

    sprintf(tgpioirq, "/sys/class/gpio/%s/direction", GPIO_ADC_RVS_STR);

    tFdAdcRvs = open(tgpioirq, O_RDWR);
    if (tFdAdcRvs < 0)
    {
        //if not exported already than export it
        system("echo \"49\" > /sys/class/gpio/export");
    }
    else
    {
        close(tFdAdcRvs);
    }

    sprintf(tCommand, "echo \"in\" > /sys/class/gpio/%s/direction", GPIO_ADC_RVS_STR);
    system(tCommand);

    sprintf(tCommand,"echo \"rising\" > /sys/class/gpio/%s/edge", GPIO_ADC_RVS_STR);
    system(tCommand);

    return;
}

unsigned short int Adc_ReadData(ChannelNum_e iChannelNum)
{
    char gpio_irq[64] = {0, };
    int irqfd = 0;
    fd_set fds;
    int buf;
    unsigned char tDummyWrData[4] = {0, };
    unsigned long int tAverageData = 0;
    unsigned short int tConvData[ADC_AVERAGE_CNT] = {0, };
    unsigned long int tCumulativeData = 0;

    /** Channel Selection */
    AdcChannelSelection(iChannelNum);

    snprintf(gpio_irq, sizeof(gpio_irq), "/sys/class/gpio/%s/value", GPIO_ADC_RVS_STR);
    irqfd = open(gpio_irq, O_RDONLY, S_IREAD);
    if(irqfd == -1)
    {
        printf("Could not open ADC_IRQ (%s), Make sure the GPIO is already exported\n", GPIO_ADC_RVS_STR);
        return -1;
    }

    FD_ZERO(&fds);

    for(int tcnt = 0 ; tcnt < ADC_AVERAGE_CNT ; ++tcnt)
    {
        FD_SET(irqfd, &fds);

        /* See if the IRQ has any data available to read */
        select(irqfd + 1, NULL, NULL, &fds, NULL);

        if (FD_ISSET(irqfd, &fds))
        {
            /* Remove the file des from set */
            FD_CLR(irqfd, &fds);

            /* The return value includes the actual GPIO register value */
            read(irqfd, &buf, sizeof(buf));
            lseek(irqfd, 0, SEEK_SET);

            unsigned char tConvertedData[4] = {0,};
            int tFd = SpiOpen(ANALOG_IN_DEVICE);
            if (tFd < 0)
            {
                printf("Error to open %s\n", ANALOG_IN_DEVICE);
            }

            SpiRead(tFd, (unsigned char *) tDummyWrData, 4, tConvertedData, 4);
            SpiClose(tFd);

            //tConvData[tcnt] = (tConvertedData[0] << 24) | (tConvertedData[1] << 16) || (tConvertedData[2] << 8) || tConvertedData[3];
            tConvData[tcnt] = (tConvertedData[0] << 8) | (tConvertedData[1]) ;
            //printf("tConvData[%d]:0x%04X\n", tcnt+1, tConvData[tcnt]);
        }
    }

    tCumulativeData = 0;
    for(int i = 0 ; i < ADC_AVERAGE_CNT ; ++i)
    {
        tCumulativeData += tConvData[i] ;
        //printf("tCumulativeData : 0x%X\n", tCumulativeData);
    }

    //printf("tCumulativeData : 0x%X\n", tCumulativeData);
    tAverageData = tCumulativeData / ADC_AVERAGE_CNT;
    //printf("tCumulativeData : 0x%X\n", tCumulativeData);
    //printf("tAverageData : 0x%04X\n", tAverageData);

    return (tAverageData);
}

