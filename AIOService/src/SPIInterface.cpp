//
// Created by mukeshp on 5/20/2020.
//

#include "SPIInterface.hpp"
#include "AnalogIn.hpp"

int SpiOpen(const char *pDeviceName)
{
    int tFd = -1;
    int tRetVal = -1;
    unsigned int tSetMode = 0;
    unsigned int tGetMode = 0;
    unsigned char tSetBits = 0;
    unsigned char tGetBits = 0;
    unsigned int tSetSpeed = 0;
    unsigned int tGetSpeed = 0;

    tSetMode = SPI_MODE_0;
    tSetBits = 8;
    tSetSpeed = 100000; /* 100 KHZ SPI clock speed */

    tFd = open(pDeviceName, O_RDWR);
    if (tFd < 0)
    {
        printf("Cant open Device-%s at %s\n", pDeviceName, __func__ );
        return tFd;
    }
    //printf("%s device open success\n", ANALOG_IN_DEVICE);

    /**
     * SPI Mode set
     * */
    tRetVal = ioctl(tFd, SPI_IOC_WR_MODE, &tSetMode);
    if (tRetVal == -1)
    {
        printf("Can't set a spi mode at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    tRetVal = ioctl(tFd, SPI_IOC_RD_MODE, &tGetMode);
    if (tRetVal == -1)
    {
        printf("Can't get a spi mode at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    /**
     *  bits per word
     * */
    tRetVal = ioctl(tFd, SPI_IOC_WR_BITS_PER_WORD, &tSetBits);
    if (tRetVal == -1)
    {
        printf("Can't set a bits per word at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    tRetVal = ioctl(tFd, SPI_IOC_RD_BITS_PER_WORD, &tGetBits);
    if (tRetVal == -1)
    {
        printf("Can't get a bits per word at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    /*
	 * max speed hz
	 */
    tRetVal = ioctl(tFd, SPI_IOC_WR_MAX_SPEED_HZ, &tSetSpeed);
    if (tRetVal == -1)
    {
        printf("Can't set a max speed hz at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    tRetVal = ioctl(tFd, SPI_IOC_RD_MAX_SPEED_HZ, &tGetSpeed);
    if (tRetVal == -1)
    {
        printf("Can't get a max speed hz at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    //printf("spi mode: 0x%x\n", tGetMode);
    //printf("bits per word: %d\n", tGetBits);
    //printf("max speed: %d Hz (%d KHz)\n", tGetSpeed, tGetSpeed/1000);

    return tFd;
}

int SpiRead(int iFd, unsigned char *pTx, unsigned long iTxDataLen, unsigned char *pRx, unsigned long iRxDataLen)
{
    int tRetVal = -1;
    struct spi_ioc_transfer tr[2] = {0,};
    char tDummyRxBuf[4] = {0,};

    tr[0].tx_buf = (unsigned long)pTx;
    tr[0].rx_buf = (unsigned long)tDummyRxBuf;
    tr[0].len = iTxDataLen;
    tr[0].cs_change = true;
    tr[0].delay_usecs = 50000;

    tr[1].tx_buf = (unsigned long)pTx;
    tr[1].rx_buf = (unsigned long)pRx;
    tr[1].len = iRxDataLen;
    tr[1].cs_change = false;
    tr[1].delay_usecs = 50000;

    tRetVal = ioctl(iFd, SPI_IOC_MESSAGE(2), tr);
    if (tRetVal < 1)
    {
        printf("Can't Read spi message at %s\n", __func__ );
        return -1;
    }

    return 0;
}

/* iCsAfterWr : 1 => After write and before next read/wrire device deselected.
 * iCsAfterWr : 0 => Device will be continuous selected.
 * */
int SpiWrite(int iFd, unsigned char *pTx, unsigned long iDataLen, char iCsAfterWr)
{
    int tRetVal = -1;
    unsigned char tDummyRd[4] = {0,};

    struct spi_ioc_transfer tr[1] = {0, };

    tr[0].tx_buf = (unsigned long)pTx;
    tr[0].rx_buf = (unsigned long)tDummyRd;
    tr[0].len = iDataLen;
    tr[0].cs_change = iCsAfterWr;
    tr[0].delay_usecs = 50000;

    tRetVal = ioctl(iFd, SPI_IOC_MESSAGE(1), &tr[0]);
    if (tRetVal < 1)
    {
        printf("SpiWrite:%s\n", strerror(errno));
        printf("Can't Write spi message at %s\n", __func__ );
        return -1;
    }

    //printf("SpiWrite:%s\n", strerror(errno));

    return 0;
}


int SpiOpen_Hart(const char *pDeviceName)
{
    int tFd = -1;
    int tRetVal = -1;
    unsigned int tSetMode = 0;
    unsigned int tGetMode = 0;
    unsigned char tSetBits = 0;
    unsigned char tGetBits = 0;
    unsigned int tSetSpeed = 0;
    unsigned int tGetSpeed = 0;

    tSetMode = SPI_MODE_1;
    tSetBits = 8;
    tSetSpeed = 100000;//500000;

    tFd = open(pDeviceName, O_RDWR);
    if (tFd < 0)
    {
        printf("Cant open Device-%s at %s\n", pDeviceName, __func__ );
        return tFd;
    }
    //printf("%s device open success\n", ANALOG_IN_DEVICE);

    /**
     * SPI Mode set
     * */
    tRetVal = ioctl(tFd, SPI_IOC_WR_MODE, &tSetMode);
    if (tRetVal == -1)
    {
        printf("Can't set a spi mode at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    tRetVal = ioctl(tFd, SPI_IOC_RD_MODE, &tGetMode);
    if (tRetVal == -1)
    {
        printf("Can't get a spi mode at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    /**
     *  bits per word
     * */
    tRetVal = ioctl(tFd, SPI_IOC_WR_BITS_PER_WORD, &tSetBits);
    if (tRetVal == -1)
    {
        printf("Can't set a bits per word at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    tRetVal = ioctl(tFd, SPI_IOC_RD_BITS_PER_WORD, &tGetBits);
    if (tRetVal == -1)
    {
        printf("Can't get a bits per word at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    /*
	 * max speed hz
	 */
    tRetVal = ioctl(tFd, SPI_IOC_WR_MAX_SPEED_HZ, &tSetSpeed);
    if (tRetVal == -1)
    {
        printf("Can't set a max speed hz at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

    tRetVal = ioctl(tFd, SPI_IOC_RD_MAX_SPEED_HZ, &tGetSpeed);
    if (tRetVal == -1)
    {
        printf("Can't get a max speed hz at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }

#if 0
    /** Read MSB first */
    tRetVal = ioctl(tFd, SPI_IOC_RD_LSB_FIRST, (unsigned long int *)1);
    if (tRetVal == -1)
    {
        printf("Can't set a read msb first at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }
#endif
#if 0
    /** Write MSB first */
    tRetVal = ioctl(tFd, SPI_IOC_WR_LSB_FIRST, (unsigned long int *)0);
    if (tRetVal == -1)
    {
        printf("Can't set a write msb first at %s\n", __func__ );
        SpiClose(tFd);
        return -1;
    }
#endif
    printf("spi mode: 0x%x\n", tGetMode);
    printf("bits per word: %d\n", tGetBits);
    printf("max speed: %d Hz (%d KHz)\n", tGetSpeed, tGetSpeed/1000);

    return tFd;
}

int SpiWrite_Hart(int iFd, unsigned char *pTx, unsigned long iDataLen, char iCsAfterWr)
{
    int tRetVal = -1;
    unsigned char tDummyRd[4] = {0,};

    struct spi_ioc_transfer tr[1] = {0, };

    tr[0].tx_buf = (unsigned long)pTx;
    tr[0].rx_buf = (unsigned long)tDummyRd;
    //tr[0].len = iDataLen;
    tr[0].len = 3;
    //tr[0].cs_change = iCsAfterWr;
    tr[0].cs_change = true;
    tr[0].delay_usecs = 50000;

    tRetVal = ioctl(iFd, SPI_IOC_MESSAGE(1), &tr[0]);
    if (tRetVal < 1)
    {
        printf("SpiWrite:%s\n", strerror(errno));
        printf("Can't Write spi message at %s\n", __func__ );
        return -1;
    }

    printf("tDummyRd[0]:%x\n", tDummyRd[0]);
    printf("tDummyRd[1]:%x\n", tDummyRd[1]);
    printf("tDummyRd[2]:%x\n", tDummyRd[2]);

    printf("SpiWrite:%s\n", strerror(errno));

    return 0;
}

int SpiRead_Hart(int iFd, unsigned char *pTx, unsigned char *pRx, unsigned long iDataLen)
{
    int tRetVal = -1;
    struct spi_ioc_transfer tr[2] = {0,};
    char tDummyTxBuf[4] = {0,};
    char tDummyRxBuf[4] = {0,};

    tr[0].tx_buf = (unsigned long)pTx;
    tr[0].rx_buf = (unsigned long)tDummyRxBuf;
    tr[0].len = 3;
    tr[0].cs_change = true;
    tr[0].delay_usecs = 50000;

    //tr[1].tx_buf = (unsigned long)tDummyTxBuf;
    tr[1].tx_buf = (unsigned long)pTx;
    tr[1].rx_buf = (unsigned long)pRx;
    tr[1].len = iDataLen;
    tr[1].cs_change = true;
    tr[1].delay_usecs = 50000;

    tRetVal = ioctl(iFd, SPI_IOC_MESSAGE(2), tr);
    if (tRetVal < 1)
    {
        printf("Can't Read spi message at %s\n", __func__ );
        return -1;
    }

    printf("tDummyRxBuf[0]:%x\n", tDummyRxBuf[0]);
    printf("tDummyRxBuf[1]:%x\n", tDummyRxBuf[1]);
    printf("tDummyRxBuf[2]:%x\n", tDummyRxBuf[2]);

    sleep(1);
    return 0;
}

int SpiDataTransfer(int iFd, unsigned char *pRx, unsigned long iDataLen)
{
    int tRetVal = -1;
    char tDummyTxBuf[4] = {0,};

    struct spi_ioc_transfer tr[1] = {0,};

    tr[0].tx_buf = (unsigned long)tDummyTxBuf;
    tr[0].rx_buf = (unsigned long)pRx;
    tr[0].len = iDataLen;
    //tr[0].cs_change = true;
    tr[0].cs_change = false;
    tr[0].delay_usecs = 50000;
    tr[0].speed_hz = 100000;
    tr[0].bits_per_word = 8;

    tRetVal = ioctl(iFd, SPI_IOC_MESSAGE(1), tr);
    if (tRetVal < 1)
    {
        printf("Can't send spi message at %s\n", __func__ );
        return -1;
    }

    return 0;
}

int SpiDataTransfer_hart(int iFd, unsigned char *pTx, unsigned char *pRx, unsigned long iDataLen)
{
    int tRetVal = -1;
    char tDummyTxBuf[4] = {0,};

    struct spi_ioc_transfer tr = {
            .tx_buf = (unsigned long)pTx,
            .rx_buf = (unsigned long)pRx,
            .len = iDataLen,
            .cs_change = 1,
            //.delay_usecs = 50000,
            //.speed_hz = 500000,
            //.bits_per_word = 8,
    };

    tRetVal = ioctl(iFd, SPI_IOC_MESSAGE(1), &tr);
    if (tRetVal < 1)
    {
        printf("Can't send spi message at %s\n", __func__ );
        printf("%s\n", strerror(errno));
        return -1;
    }
    //printf("%s\n", strerror(errno));
    return 0;
}


void SpiClose(int iFd)
{
    close(iFd);
    //printf("%s is executed\n", __func__ );
}

