//
// Created by mukeshp on 5/19/2020.
//

#ifndef DEVELOPEMENT_I2C_INTERFACE_H
#define DEVELOPEMENT_I2C_INTERFACE_H

#include <linux/i2c-dev.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

int I2C_Open(char *pDeviceName);
int I2C_Write(int iFd, char *pBuf, unsigned int iBufLen);
int I2C_Read(int iFd, char *pBuf, unsigned int iBufLen);
void I2C_Close(int iFd);

#endif //DEVELOPEMENT_I2C_INTERFACE_H
