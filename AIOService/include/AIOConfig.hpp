//
// Created by mukeshp on 31-Jul-20.
//

#ifndef AIOINTERFACE_AIOCNFG_H
#define AIOINTERFACE_AIOCNFG_H

#include "AnalogIn.hpp"
#include "AnalogOut.hpp"

#define AI_APP_MODE_RUN     (0)
#define AI_APP_MODE_ADC_CAL     (1)
#define AI_APP_MODE_DAC_CAL     (2)

#define GET_API_APP_MODE_STR(_arg_)     (((_arg_) == (AI_APP_MODE_RUN)) ? "RUN_MODE" : "CAL_MODE")

#define AI_CHANNEL_ENABLE          (1)
#define AI_CHANNEL_DISABLE         (0)

#define AI_CHANNEL_TYPE_BIPOLAR_10V     'V'
#define AI_CHANNEL_TYPE_4_20mA          'I'

#define AI_CHANNEL_MAX (4)

#define GET_AI_CHANNEL_TYPE_STR(_arg_)  (((_arg_) == AI_CHANNEL_TYPE_BIPOLAR_10V) ? "Voltage" : "Current")

#define AIO_CNFG_FILE_PATH    "."
#define AIO_CNFGL_FILE         "aioCnfg.ini"

typedef struct AiChannelCnfg
{
    char mIsChannelEnable;
    char mChannelType;

    AiChannelCnfg()
    {
        this->mIsChannelEnable = AI_CHANNEL_ENABLE;
        this->mChannelType = AI_CHANNEL_TYPE_4_20mA;
    }
}AiChannelCnfg_t;

typedef struct AiCnfg
{
    int mNumOfCnannel;
    AiChannelCnfg_t mAiChannelCnfg[AI_CHANNEL_MAX];

    AiCnfg()
    {
        this->mNumOfCnannel = AI_CHANNEL_MAX;
    }
}AiCnfg_t;

typedef struct AoChannelCnfg
{
    char mIsChannelEnable;

    AoChannelCnfg()
    {
        this->mIsChannelEnable = AO_CHANNEL_ENABLE;
    }
}AoChannelCnfg_t;

typedef struct AoCnfg
{
    int mNumOfChannel;
    AoChannelCnfg_t mAoChannelCnfg[AO_CHANNEL_MAX];

    AoCnfg()
    {
        this->mNumOfChannel = AO_CHANNEL_MAX;
    }
}AoCnfg_t;

int Aio_ConfigurationRead(void);
int Aio_ConfigurationWrite(const char *pTag, const char *pValue);

extern AiCnfg_t    global_analog_in_configuration;
extern AoCnfg_t    global_analog_out_configuration;

#endif //AIOINTERFACE_AIOCNFG_H
