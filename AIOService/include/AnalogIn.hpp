//
// Created by mukeshp on 5/21/2020.
//

#ifndef DEVELOPEMENT_ANALOGIN_H
#define DEVELOPEMENT_ANALOGIN_H

#include <stdlib.h>
#include <pthread.h>

#define ADC_AVERAGE_CNT     (10)

#define SWAP_BYTES(byte1, byte2) { unsigned char temp = byte1; byte1 = byte2 ; byte2 = temp;}

/** On write, MSB first should transfer.
 *  On Read, It sends LSB first.
 *  SPI driver by deafult transfer MSB first to slave device.
 * */
#define ANALOG_IN_DEVICE       "/dev/spidev1.1"

/* GPIO Pins */

#define GPIO_ADC_RVS    49
#define GPIO_ADC_RVS_STR    "gpio49"

#define GPIO_EN_MUX     "gpio57"
#define GPIO_A0_MUX     "gpio191"
#define GPIO_A1_MUX     "gpio52"

/* no need to control, because it is connected with board reset pin.
 * OFF_BD_RESET# is an output from the macrocontroller that automatically sends a reset signal when the unit powers up or reboots.
 * It can be connected to any IC on the base board that requires a reset.
 * */
//#define GPIO_ADC_RESET  "gpio53"

/*
 * ADC_CS
ADC_SDI
ADC_SDO
ADC_SCLK */

/*
 * 1) Half word means 16-bit regarding a 32-bit internal register, below is an command example to use READ_HWORD command
 * to read from ALARM_H_TH_REG Register.:
 * Data frame to read the 16-bit (0~15 bit) of 32-bit register:      <11001_xx> <0_0010_0100><00000000><00000000>
 * Data frame to read the 16-bit (16~31 bit) of 32-bit register:    <11001_xx> <0_0010_0110><00000000><00000000>
 * 2) Yes, the SCLK clocks in first frame F are used to send the command to the ADC device,
 * and the clocks in second frame F+1 are used to shift out 16-bit content of the register from
 * the device to the microcontroller.
 * Notice that the second frame is required because the command gets executed at the rising edge of CONVST/&CS signal.
 * 3) Yes, see the command example in 1).
 * */

/* Data out control register options */

#define DEVICE_ADDR_INCL        (1<<14)
#define SELECTED_RANGE_INCL     (1<<8)
#define PARITY_EN               (1<<3)

// Register Locations and Names
#define ADS8689_DEVICE_ID_REG   0x00
#define ADS8689_RST_PWRCTL_REG  0x04
#define ADS8689_SDI_CTL_REG     0x08
#define ADS8689_SDO_CTL_REG     0x0C
#define ADS8689_DATAOUT_CTL_REG 0x10
#define ADS8689_RANGE_SEL_REG   0x14
#define ADS8689_ALARM_REG       0x20
#define ADS8689_ALARM_H_TH_REG  0x24
#define ADS8689_ALARM_L_TH_REG  0x28

// SPI commands
#define ADS8689_NOP         0b0000000
#define ADS8689_CLEAR_HWORD 0b1100000
#define ADS8689_READ_HWORD  0b1100100
#define ADS8689_READ        0b0100100
#define ADS8689_WRITE_FULL  0b1101000
#define ADS8689_WRITE_MS    0b1101001
#define ADS8689_WRITE_LS    0b1101010
#define ADS8689_SET_HWORD   0b1101100

#define INT_REF_VOLTAGE     (0)
#define EXT_REF_VOLTAGE     (1)


#define VOLTAGE_RANGE_BIPOLAR_3V        (0x00)
#define VOLTAGE_RANGE_BIPOLAR_2_5V      (0x01)
#define VOLTAGE_RANGE_BIPOLAR_1_5V      (0x02)
#define VOLTAGE_RANGE_BIPOLAR_1_25V     (0x03)
#define VOLTAGE_RANGE_BIPOLAR_0_625V    (0x04)
#define VOLTAGE_RANGE_UNIPOLAR_3V       (0x08)
#define VOLTAGE_RANGE_UNIPOLAR_2_5V     (0x09)
#define VOLTAGE_RANGE_UNIPOLAR_1_5V     (0x0A)
#define VOLTAGE_RANGE_UNIPOLAR_1_25V    (0x0B)

typedef enum
{
    CHANNEL_NUM_1 = 0,
    CHANNEL_NUM_2,
    CHANNEL_NUM_3,
    CHANNEL_NUM_4,
    CHANNEL_DISABLE_ALL,
    CHANNEL_NUM_MAX,
}ChannelNum_e;

void* AnalogIn_Thread(void *arg);

int AdcChannelSelection(ChannelNum_e iChNum);

int Adc_init(void);

/* Adc IO pin configure*/
void Adc_IOPinConfigure(void);

/* Adc Interrupt configure */
void Adc_InterruptPinConfigure(void);

/* Adc Get device Id */
int AdcGetDeviceIdentification(unsigned long *pDeviceId);

int AdcSetRangeReferenceVoltage(unsigned char iRefSel, unsigned char iRangeVoltage);
int AdcGetRangeReferenceVoltage(unsigned long *pRangeSel);

int AdcSetSdiMode(unsigned char iSdiMode);
int AdcGetSdiMode(unsigned long *pSdiMode);

int AdcSetSdoMode(unsigned char iSdoMode);
int AdcGetSdoMode(unsigned long *pSdoMode);

int AdcSetDataOutMode(unsigned int iDataOutMode);
int AdcGetDataOutMode(unsigned long int *pDataOutMode);

int AdcGetAlarmHighThresoldRegister(unsigned long *pAlarmHighThresold);

int AdcGetAlarmLowThresoldRegister(unsigned long *pAlarmLowThresold);

int AdcGetResetPowerCntrlReg(unsigned long *pPowerCntrlreg);

unsigned short int Adc_ReadData(ChannelNum_e iChannelNum);

#endif //DEVELOPEMENT_ANALOGIN_H
