//
// Created by mukeshp on 5/19/2020.
//

#ifndef DEVELOPEMENT_ANALOGOUT_H
#define DEVELOPEMENT_ANALOGOUT_H

//

#define AO_CHANNEL_MAX  (2)

/* @Pending: Device Name search is pending, consolidated function creation pending, testing pending. */
#define AO1_I2C_SLAVE_ADDR (0x90)	/* The AO1 - I2C slave address */
#define AO2_I2C_SLAVE_ADDR (0x92)	/* The AO2 - I2C slave address */

#define GET_AO_CHANNEL_ADDR(_arg_)  (((_arg_) == 0) ? (AO1_I2C_SLAVE_ADDR) : (AO2_I2C_SLAVE_ADDR))

#define AO_CHANNEL_NUM_1        (0)
#define AO_CHANNEL_NUM_2        (1)

#define AO_CHANNEL_ENABLE          (1)
#define AO_CHANNEL_DISABLE         (0)

int DacInit(void);
int Dac_write(char iDevice, unsigned short int iData);

int DacReadDeviceIdentification(char iSlaveAddr);
int DacSetOperationMode(char iSlaveAddr, unsigned short int iMode);
int DacSoftReset(char iSlaveAddr);
int DacLoadSynchronously(char iSlaveAddr);
int DcaSelectReferenceVoltage(char iSlaveAddr, char iRef);
int DacSetGainAndDivisor(char iSlaveAddr, unsigned char iMul, unsigned char iDiv);
int DacWriteData(char iSlaveAddr, unsigned short int iData);
int DacReadData(char iSlaveAddr, unsigned short int *pData);
int DacReadStatusRegister(char iSlaveAddr, unsigned short int *pData);
int DacGetGainAndDivisor(char iSlaveAddr, unsigned char iBufGain, unsigned char iRefDiv);
int DacWriteTriggerRegister(char iSlaveAddr, unsigned short int iData);

#endif //DEVELOPEMENT_ANALOGOUT_H
