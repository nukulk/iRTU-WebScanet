#ifndef ANALOG_IO_SERVICE
#define ANALOG_IO_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>

class AIOService
{
public:
    AIOService();
    virtual ~AIOService();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    void CalibrateADC();
    void CalibrateDAC();
    void* GetLogger();
private:
    void* config;
    void* message_bus;
    void* logger;
    std::vector<std::string> destination_list;
};

#endif
