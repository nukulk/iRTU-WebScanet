#ifndef PSQL_CONNECTION_H
#define PSQL_CONNECTION_H

#include <string.h>
#include <postgresql/libpq-fe.h>
#include <Variant.hpp>
#include <DateTime.hpp>

extern "C"
{

const int DB_SUCCESS = 0;
const int ERR_NULL_CONST_VIOLATION = 901;
const int ERR_UNIQ_CONST_VIOLATION = 902;
const int ERR_COLUMN_SIZE_VIOLATION = 903;
const int ERR_MISSING_COLUMN = 904;
const int ERR_INVALID_INT = 905;
const int ERR_INAVLID_SQL_STRING = 906;
const int ERR_NO_CONNECTION = 907;
const int ERR_UNKNOWN_DB_ERROR = 999;

const unsigned int numcolumns = 32;

const unsigned int charsize = sizeof(char);
const unsigned int strsize = 256 * sizeof(char);
const unsigned int datesize = 7 * sizeof(unsigned char);
const unsigned int intsize = sizeof(long);
const unsigned int uintsize = sizeof(unsigned long);
const unsigned int realsize = sizeof(double);

class BindStruct
{
public:
    BindStruct();
    void setColumnName(const char* ptr);
    void setColumnType(const VariantType vtype);
    void setColumnPrecision(const unsigned int prec);

    const char* getColumnName();
    VariantType getColumnType();
    unsigned short getColumnPrecision();
private:
    char columnname[33];
    VariantType columntype;
    unsigned short precision;
};

class Column
{
public:
    Column();
    ~Column();

    const char* getName();
    long getLength();
    long getPosition();
    VariantType getType();
    unsigned long getNativeType();
    bool getByteUse();
    int getScale();
    int getPrecision();

    void setName(std::string val);
    void setLength(long val);
    void setPosition(long val);
    void setType(VariantType val);
    void setNativeType(unsigned long val);
    void setByteUse(bool buse);
    void setScale(int inscl);
    void setPrecision(int prec);
    void* data();

private:
    VariantType _ColumnType;
    unsigned long _NativeType;
    long    _ColumnLength;
    char _ColumnName[64];
    long    _ColumnPosition;
    bool    _ByteUsed;
    int     _Scale;
    int     _Precision;
    unsigned char _Data[256];
};

class ConnectionHandle
{
public:
    ConnectionHandle();
    virtual ~ConnectionHandle();

    void acquire();
    void release();

    const char *conninfo;
    PGconn     *conn;
    PGresult   *res;
};

class StatementHandle
{
public:
    StatementHandle();
    virtual ~StatementHandle();
    PGresult* rowset;
    Column columnList[32];
    Variant dataRow[32];
    long columnCount;
    long rowFetched;
    int rowcount;
};

class Database
{
public:
    Database ();
    ~Database ();

    bool initialize();
    bool deinitialize();
    bool createConnection(const std::string &dbname, const std::string &uname, const std::string &passwd, const std::string &host, const int &port, std::string &errmsg, int &errorCode);
    bool reConnect();
    bool closeConnection();
    bool executeDML(const char *sqlstr, std::string &errmsg, int &errorCode);
    bool executeSQL(const char *sqlstr, std::string &errmsg, int &errorCode);
    Variant* getRow(bool &fetchpending, std::string &errmsg, int &errorCode);
	void enableAutoCommit();
    bool startTransaction();
    bool commit();
    bool rollback();
    void getError(std::string &str, int &errorCode);
    ConnectionHandle* getConnectionHandle();
    StatementHandle* getStatementHandle();
    bool isOpen();
    void rowLimitClause(std::string &str);
    void releaseReadBuffers();
    bool prepareBind(const char* sqlstring, std::string &errmsg, int &errorCode);
    bool bindColumn(BindStruct &bstruct);
    bool copyValue(Variant &val, int column, int row);
	void setBulkArraySize(long sz);
    bool executeBulkDML(std::string &errmsg, int &errorCode);
protected:
    int errorDBToAppType(int errorcode);
    std::string getNativeTimeStamp(DateTime &timestamp);
    bool getColumns(std::string &errmsg, int &errorCode);
    VariantType translateToLocalType(unsigned int dbtype);
    bool setupDateFormat(std::string errmsg, int &errorCode);
private:
    std::string getDatabaseTimeStamp(const std::string &timestamp, const std::string &format);
    void translatePGTStoCTS(const char* ptr, tm &tsstruct);
    ConnectionHandle currentConnection;
    StatementHandle currentStatement;
    bool commit_on;
    long _arraysize;
    std::string _dbname;
    std::string _uname;
    std::string _passwd;
    std::string _host;
    int _port;
    std::string _portstr;
};

}

#endif

