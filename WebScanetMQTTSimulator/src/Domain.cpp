#include "Domain.hpp"
#include "RTUCore/StringEx.h"

Domain::Domain()
{

}

Domain::~Domain()
{

}

bool Domain::Initialize(const std::string db_name, const std::string db_server, const std::string db_port, const std::string db_user, const std::string db_pass)
{
    dbms_server = db_server;
    dbms_port = db_port;
    dbms_name = db_name;
    dbms_user = db_user;
    dbms_pass = db_pass;

    std::string connection_string;

    connection_string += "dbname = ";
    connection_string += dbms_name;
    connection_string += " user = ";
    connection_string += dbms_user;
    connection_string += " password = ";
    connection_string += dbms_pass;
    connection_string += " hostaddr = ";
    connection_string += dbms_server;
    connection_string += " port = ";
    connection_string += dbms_port;

    if(!db.initialize())
    {
        return false;
    }

    return true;
}

bool Domain::Connect()
{
    int error_code;
    std::string error_str;

    if(!db.createConnection(dbms_name, dbms_user, dbms_pass, dbms_server, atoi(dbms_port.c_str()), error_str, error_code))
    {
        return false;
    }

    return true;
}

bool Domain::ProcessPayload(const std::string &payload_str, const std::string &device_id, const std::string &latitude, const std::string &longitude, const std::string &timestamp)
{
    int error_code;
    std::string error_str;

    std::string column_sql;
    std::string value_sql;

    if(payload_str.find("led_count") != std::string::npos)
    {
        column_sql = "insert into telemetry_lighting_feeder_panel (";
    }
    else
    {
        if(payload_str.find("pump_efficiency") != std::string::npos)
        {
            column_sql = "insert into telemetry_water_pump (";
        }
        else
        {
            return false;
        }
    }

    std::string temp_str;

    column_sql += "device_id,latitude,longitude,timestamp,";
    value_sql += " values(";
    value_sql += "'";

    temp_str = device_id;
    str_ex_alltrim(temp_str);
    value_sql += temp_str;
    value_sql += "',";
    value_sql += "'";

    temp_str = latitude;
    str_ex_alltrim(temp_str);
    value_sql += temp_str;
    value_sql += "',";
    value_sql += "'";

    temp_str = longitude;
    str_ex_alltrim(temp_str);
    value_sql += temp_str;
    value_sql += "',";
    value_sql += "'";

    temp_str = timestamp;
    str_ex_alltrim(temp_str);
    temp_str.insert(4, 1,'-');
    temp_str.insert(7, 1,'-');
    temp_str.insert(10, 1,' ');
    temp_str.insert(13, 1,':');
    temp_str.insert(16, 1,':');
    value_sql += temp_str;;
    value_sql += "',";

    std::vector<std::string> data_fields;

    str_ex_split_with_char(payload_str, data_fields, ',');

    for(auto key_value: data_fields)
    {
        std::string key;
        std::string value;

        str_ex_keyvalue_with_char(key_value, key, value, ':');
        str_ex_alltrim(value);
        column_sql += key + ",";
        value_sql += "'";
        value_sql += value;
        value_sql += "',";
    }

    column_sql = column_sql.erase(column_sql.length()-1, 1);
    value_sql = value_sql.erase(value_sql.length()-1, 1);

    column_sql += ") ";
    value_sql += ")";

    std::string sql_buffer = column_sql + value_sql;

    return db.executeDML(sql_buffer.c_str(), error_str, error_code);
}

bool Domain::GetDeviceList(std::vector<std::string> &devlist)
{
    int error_code;
    std::string error_str;

    if(!db.executeSQL("select device_id from device_clients", error_str, error_code))
    {
        return false;
    }

    int rows = db.getStatementHandle()->rowcount;

    for(int index = 0; index < rows; index++)
    {
        bool pending = true;
        Variant* row = db.getRow(pending, error_str, error_code);
        devlist.push_back((char*)&row->rawBuffer[0]);
    }

    return true;
}
