#include <vector>
#include <string>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <mosquitto.h>

#include "Domain.hpp"
#include <RTUCore/Configuration.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>

#pragma pack(1)
typedef struct MQTTParams
{
    bool run;
    int mqtt_port;
    char mqtt_host[33];
    char client_id[33];
    struct mosquitto *mosq;
    pthread_t receiver_thread;
    int keep_alive;
}MQTTParams;
#pragma pop

static void OnSignalReceived(SignalType s);
static void SendSimulatedCommands();

static MQTTParams* appptr = nullptr;
static Domain domain;
static void* logger = nullptr;
static std::string simulator;

int main(int, char **)
{
    void* config = nullptr;
    int rc = 0;
    MQTTParams params;

    printf("00\n");
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        return -1;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    std::string mqtt_server;
    std::string mqtt_port;
    std::string dbms_server;
    std::string dbms_port;
    std::string dbms_name;
    std::string dbms_user;
    std::string dbms_pass;

    config = configuration_allocate_default();

    if(config == nullptr)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    configuration_get_value_as_string(config, "default", "mqtt_server", mqtt_server);
    configuration_get_value_as_string(config, "default", "mqtt_port", mqtt_port);
    configuration_get_value_as_string(config, "default", "dbms_server", dbms_server);
    configuration_get_value_as_string(config, "default", "dbms_port", dbms_port);
    configuration_get_value_as_string(config, "default", "dbms_name", dbms_name);
    configuration_get_value_as_string(config, "default", "dbms_user", dbms_user);
    configuration_get_value_as_string(config, "default", "dbms_pass", dbms_pass);
    configuration_get_value_as_string(config, "default", "simulator", simulator);

    configuration_release(config);

    if(!domain.Initialize(dbms_name, dbms_server, dbms_port, dbms_user, dbms_pass))
    {
        WriteLog(logger, "Could not initialize database", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    if(!domain.Connect())
    {
        WriteLog(logger, "Could not connect to the database", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    appptr = &params;

    params.run = true;
    params.mqtt_port = atoi(mqtt_port.c_str());
    params.keep_alive = 60;
    strcpy(params.mqtt_host, mqtt_server.c_str());
    memset(params.client_id, 0, 24);
    strcpy(params.client_id, "webscanet");

    rc = mosquitto_lib_init();
    if(rc != MOSQ_ERR_SUCCESS)
    {
        printf("Could not initialize libmosquitto - %d\n", rc);
        return -1;
    }

    params.mosq = mosquitto_new(params.client_id, true, nullptr);

    if(params.mosq)
    {
        rc = mosquitto_connect(params.mosq, params.mqtt_host, params.mqtt_port, params.keep_alive);

        if(rc != MOSQ_ERR_SUCCESS)
        {
            printf("Could not connect - %d\n", rc);
            return -1;
        }

        while(params.run)
        {
            rc = mosquitto_loop(params.mosq, -1, 1);

            if(params.run && rc)
            {
                printf("Connection error ... reconnecting in 10 seconds\n");
                sleep(10);

                rc = mosquitto_reconnect(params.mosq);

                if(rc != MOSQ_ERR_SUCCESS)
                {
                    printf("Could not reconnect - %d\n", rc);
                }
            }

            sleep(30);
            SendSimulatedCommands();
        }

        mosquitto_destroy(params.mosq);
    }

    mosquitto_lib_cleanup();

    return rc;
}

void OnSignalReceived(SignalType s)
{
    appptr->run = false;
}

void SendSimulatedCommands()
{
    std::vector<std::string> devlist;
    domain.GetDeviceList(devlist);

    for(std::string sender_mqtt_client : devlist)
    {
        std::string topic;
        int dummy_mid = 1000;
        std::string timestamp;
        std::string buffer;

        str_ex_get_default_timestamp(timestamp);
        std::string simulation_payload_header = "ascii,mqtt,device_id,WebScanet,recipient,text,1.0.0,1000,timestamp";
        str_ex_replace_with_str(simulation_payload_header, "timestamp", timestamp);
        str_ex_replace_with_str(simulation_payload_header, "device_id", sender_mqtt_client);
        str_ex_replace_with_str(simulation_payload_header, "recipient", "service_peripheral_simulator");

        topic += sender_mqtt_client + "/request/";

        // DIO Write out
        buffer.clear();
        buffer += simulation_payload_header;
        buffer += "\r\nDIO_WRITE_OUT\r\n1:1,2:1,3:0,4:0";
        mosquitto_publish(appptr->mosq, &dummy_mid, topic.c_str(), buffer.length(), buffer.c_str(), 0, 0);

        // ADC Calibration
        buffer.clear();
        buffer += simulation_payload_header;
        buffer += "\r\nAIO_AD_CALIBRATION\r\nI:1:1:1:0:32767:65535,I:2:1:1:0:32767:65535,O:1:1:1:0:32767:65535,O:2:1:1:0:32767:65535";
        mosquitto_publish(appptr->mosq, &dummy_mid, topic.c_str(), buffer.length(), buffer.c_str(), 0, 0);

        // DAC Calibration
        buffer.clear();
        buffer += simulation_payload_header;
        buffer += "\r\nAIO_DA_CALIBRATION\r\nI:1:1:1:0:32767:65535,I:2:1:1:0:32767:65535,O:1:1:1:0:32767:65535,O:2:1:1:0:32767:65535";
        mosquitto_publish(appptr->mosq, &dummy_mid, topic.c_str(), buffer.length(), buffer.c_str(), 0, 0);
    }
}

