#include "DataEntity.hpp"

DataEntity::DataEntity(Database* db, std::string tabname)
{
    _Database = db;
    _TableName = tabname;
}

DataEntity::~DataEntity()
{
}

void DataEntity::clear()
{
    _RecordList.clear();
}

std::vector<Variant *>* DataEntity::getAllRecords()
{

    std::vector<Variant *>* ret;

//    _RecordList.clear();

//    std::string querystring = "select * from " + _TableName;

//    QSqlQuery query = _Database->exec(querystring);

//    while(query.next())
//    {
//        _RecordList.append(query.record());
//    }

    return ret;
}

std::vector<Variant*>* DataEntity::getSelectedRecords(std::string keyname, Variant value)
{
    std::vector<Variant *>* ret;

//    _RecordList.clear();

//    QString valsub = "";
//    substitueValue(value, valsub);

//    QString querystring = "select * from " + _TableName + " where " + keyname + " = " + valsub;

//    QSqlQuery query = _Database->exec(querystring);

//    while(query.next())
//    {
//        _RecordList.append(query.record());
//    }

    return ret;
}

bool DataEntity::remove(std::string keyname, Variant value)
{
//    QSqlQuery qryresult;
//    QString err;
//    QString valsub = "";

//    substitueValue(value, valsub);

//    QString querystring = "delete from " + _TableName + " where " + keyname + " = " + valsub;

//    try
//    {
//        _Database->transaction();

//        qryresult = _Database->exec(querystring);

//        _Database->commit();
//    }
//    catch(QException e)
//    {
//       _Database->rollback();
//       err = _Database->lastError().text();
//       return false;
//    }

    return true;
}

Variant DataEntity::maximumValue(std::string keyname)
{
    Variant retval;

//    QString sqlstring = "select distinct _key_ from _table_ order by _key_  desc limit 1";
//    sqlstring = sqlstring.replace("_table_", _TableName);
//    sqlstring = sqlstring.replace("_key_", keyname);

//    QSqlQuery query = _Database->exec(sqlstring);

//    if(query.next())
//    {
//        retval = query.record().value(0);
//    }

    return retval;
}

Variant DataEntity::maximumValue(std::string keyname, std::string filter, Variant filterval)
{
    Variant retval;

//    QString valsub = "";
//    substitueValue(filterval, valsub);

//    QString sqlstring = "select _key_ from _table_ where _filter_ = _filterval_ order by _key_ desc limit 1";
//    sqlstring = sqlstring.replace("_table_", _TableName);
//    sqlstring = sqlstring.replace("_key_", keyname);
//    sqlstring = sqlstring.replace("_filter_", filter);
//    sqlstring = sqlstring.replace("_filterval_", valsub);

//    QSqlQuery query = _Database->exec(sqlstring);

//    if(query.next())
//    {
//        retval = query.record().value(0);
//    }

    return retval;
}

void DataEntity::substitueValue(Variant &var, std::string &str)
{
//    switch(var.type())
//    {
//        case QVariant::Char:
//        case QVariant::String:
//        {
//            str = "'" + var.toString() + "'";
//        }
//        break;

//        case QVariant::Double:
//        case QVariant::Int:
//        case QVariant::LongLong:
//        case QVariant::UInt:
//        case QVariant::ULongLong:
//        {
//            str = var.toString();
//        }
//        break;

//        default:
//        {
//        }
//        break;
//    }
}

