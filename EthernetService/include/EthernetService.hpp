#ifndef ETHERNET_SERVICE
#define ETHERNET_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>

class EthernetService
{
public:
    EthernetService();
    virtual ~EthernetService();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    void* GetLogger();
private:
    void* config;
    void* message_bus;
    void* logger;
    std::vector<std::string> destination_list;
};

#endif
