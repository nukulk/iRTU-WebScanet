#include "TransportService.hpp"
#include "ProtocolHTTP.hpp"
#include "ProtocolMQTT.hpp"

#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>
#include <ifaddrs.h>
#include <netpacket/packet.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <MessageBus/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>

static void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void OnSignalReceived(SignalType type);

int generate_device_id(const char* network_interface, char* device_id);

static TransportService *appptr = nullptr;
static IServiceCallback *callback = nullptr;

TransportService::TransportService()
{
    inet_protocol = nullptr;
    message_bus = nullptr;
    logger = nullptr;
    appptr = this;
    callback = this;
    memset(device_id, 0, sizeof (device_id));
}

TransportService::~TransportService()
{

}

bool TransportService::Initialize()
{
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        return false;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();


    config = configuration_allocate_default();

    if(config == nullptr)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    configuration_get_value_as_string(config, "default", "version", version);
    configuration_get_value_as_string(config, "default", "protocol", protocol);
    configuration_get_value_as_string(config, "default", "server", server);
    configuration_get_value_as_string(config, "default", "uri", uri);
    port = configuration_get_value_as_integer(config, "default", "port");
    enabled = configuration_get_value_as_boolean(config, "default", "enabled");
    latitude = configuration_get_value_as_real(config, "default", "latitude");
    longitude = configuration_get_value_as_real(config, "default", "longitude");
    configuration_get_value_as_string(config, "default", "header", header_template);

    std::string network_interface;
    configuration_get_value_as_string(config, "default", "device_id_src", network_interface);

    generate_device_id(network_interface.c_str(), device_id);

    WriteInformation(logger, std::string("Device ID is ") + device_id);

    configuration_release(config);

    WriteInformation(logger, "Read all configuration keys");

    return true;
}

bool TransportService::Start()
{
    if(enabled)
    {
        WriteInformation(logger, "Transport Service is enabled");

        if(protocol == "http")
        {
            inet_protocol = new ProtocolHTTP();
        }
        else
        {
            if(protocol == "mqtt")
            {
                inet_protocol = new ProtocolMQTT();
            }
            else
            {
                return false;
            }
        }

        if(!inet_protocol->Initialize(logger, server, port, device_id, this))
        {
            return false;
        }

        WriteInformation(logger, "Initialized protocol adapter");

        if(!inet_protocol->Connect())
        {
            return false;
        }
    }

    if(!message_bus_initialize(&message_bus, OnNetworkEvent))
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    WriteInformation(logger, "Initialized message bus client");

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    WriteInformation(logger, "Connected to message bus");

    //Test payload
    message_bus_send_loopback(message_bus);

    while(true)
    {
        sleep(10);
    }

    return true;
}

bool TransportService::ReStart()
{
    return false;
}

bool TransportService::Stop()
{
    inet_protocol->Close();
    WriteInformation(logger, "Closed protocol adapter");
    inet_protocol->Release();
    WriteInformation(logger, "Released protocol adapter");

    message_bus_close(message_bus);
    WriteInformation(logger, "Closed connection to message bus");
    message_bus_release(message_bus);
    WriteInformation(logger, "Released message bus");

    logger_release(logger);

    return false;
}

void *TransportService::GetLogger()
{
    return logger;
}

bool TransportService::OnDownLink(const std::string &message, long len, char mtypechar)
{
    std::vector<std::string> header_fields;
    std::string header;
    std::string destination_payload;

    str_ex_keyvalue_with_str(message, header, destination_payload, "\r\n");
    str_ex_split_with_char(header, header_fields, ',');

    if(header_fields.size() < 9)
    {
        WriteLog(logger, "Rejecting downlink command due field count mismatch, number of header fields must be 9", LOG_ERROR);
        return false;
    }

    PayloadType ptype = Request;
    MessageType mtype = (MessageType)mtypechar;
    DataType dtype = Raw;

    std::string recipient = header_fields[4];
    std::string datatype = header_fields[5];

    str_ex_alltrim(recipient);

    if(datatype == "text")
    {
        dtype = Text;
    }
    else
    {
        if(datatype == "image")
        {
            dtype = Image;
        }
        else
        {
            if(datatype == "audio")
            {
                dtype = Audio;
            }
            else
            {
                if(datatype == "video")
                {
                    dtype = Video;
                }
                else
                {
                    dtype = Raw;
                }
            }
        }
    }

    return message_bus_send(message_bus, recipient.c_str(), ptype, mtype, dtype, destination_payload.c_str(), destination_payload.length());
}

void TransportService::OnNodeOnline(const std::string &nodename)
{
    WriteInformation(logger, nodename + " is online");
}

void TransportService::OnNodeOffline(const std::string &nodename)
{
   WriteInformation(logger, nodename + " is offline");
}

void TransportService::OnData(const std::string &nodename, const char *messagebuffer, char dtype)
{
    if(inet_protocol)
    {
        Upload(nodename, messagebuffer, dtype, "data");
    }
}

void TransportService::OnEvent(const std::string &nodename, const char *messagebuffer, char dtype)
{
    if(inet_protocol)
    {
        Upload(nodename, messagebuffer, dtype, "event");
    }
}

void TransportService::OnRequest(const std::string &nodename, const char *messagebuffer, char dtype)
{
    if(inet_protocol)
    {
        Upload(nodename, messagebuffer, dtype, "request");
    }
}

void TransportService::OnResponse(const std::string &nodename, const char *messagebuffer, char dtype)
{
    if(inet_protocol)
    {
        Upload(nodename, messagebuffer, dtype, "response");
    }
}

void TransportService::Upload(const std::string &nodename, const char *messagebuffer, char dtype, const char *mtype)
{
    std::string ts;
    std::string buffer;
    buffer = header_template;

    str_ex_get_default_timestamp(ts);

    str_ex_replace_with_str(buffer, "message_type", mtype);
    str_ex_replace_with_real(buffer, "longitude", longitude);
    str_ex_replace_with_real(buffer, "latitude", latitude);
    str_ex_replace_with_str(buffer, "version", version);
    str_ex_replace_with_str(buffer, "transport_type", protocol);
    str_ex_replace_with_str(buffer, "device_id", device_id);
    str_ex_replace_with_str(buffer, "sender", nodename);
    str_ex_replace_with_str(buffer, "timestamp", ts);
    str_ex_replace_with_str(buffer, "peripheral_id", "000");

    //Ignore any raw data types

    if(dtype == Image || dtype == Audio || dtype == Video)
    {
        str_ex_replace_with_str(buffer, "encoding", "base64");
    }

    if(dtype == Text)
    {
        str_ex_replace_with_str(buffer, "encoding", "ascii");
    }

    switch(dtype)
    {
        case Text:
        {
            str_ex_replace_with_str(buffer, "data_type", "text");
            break;
        }
        case Image:
        {
            str_ex_replace_with_str(buffer, "data_type", "image");
            break;
        }
        case Audio:
        {
            str_ex_replace_with_str(buffer, "data_type", "audio");
            break;
        }
        case Video:
        {
            str_ex_replace_with_str(buffer, "data_type", "videos");
            break;
        }
        case Raw:
        default:
        {
            break;
        }
    }

    buffer += "\r\n";
    buffer += messagebuffer;

    if(strcmp(mtype, "event") == 0)
    {
        inet_protocol->SendEvent(buffer.c_str(), uri);
    }

    if(strcmp(mtype, "data") == 0)
    {
        inet_protocol->SendData(buffer.c_str(), uri);
    }

    if(strcmp(mtype, "request") == 0)
    {
        inet_protocol->SendRequest(buffer.c_str(), uri);
    }

    if(strcmp(mtype, "response") == 0)
    {
        inet_protocol->SendResponse(buffer.c_str(), uri);
    }
}

void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{   
    if(ptype == Data)
    {
        if(mtype != LoopBack)
        {
            if(callback)
            {
                callback->OnData(node_name, messagebuffer, dtype);
            }
        }
        else
        {
            return;
        }
    }

    if(ptype == Response)
    {
        if(callback)
        {
            callback->OnResponse(node_name, messagebuffer, dtype);
        }
    }

    if(ptype == Request)
    {
        if(callback)
        {
            callback->OnRequest(node_name, messagebuffer, dtype);
        }
    }

    if(ptype == Event)
    {
        if(mtype == NodeOnline)
        {
            if(callback)
            {
                callback->OnNodeOnline(messagebuffer);
            }
        }
        else
        {
            if(mtype == NodeOffline)
            {
                if(callback)
                {
                    callback->OnNodeOffline(messagebuffer);
                }
            }
            else
            {
                if(callback)
                {
                    callback->OnEvent(node_name, messagebuffer, dtype);
                }
            }
        }
    }
}

void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(appptr->GetLogger(), "SUSPEND SIGNAL", LOG_WARNING);
            break;
        }
        case Resume:
        {
            WriteLog(appptr->GetLogger(), "RESUME SIGNAL", LOG_WARNING);
            break;
        }
        case Shutdown:
        {
            WriteLog(appptr->GetLogger(), "SHUTDOWN SIGNAL", LOG_CRITICAL);
            appptr->Stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(appptr->GetLogger(), "ALARM SIGNAL", LOG_WARNING);
            break;
        }
        case Reset:
        {
            WriteLog(appptr->GetLogger(), "RESET SIGNAL", LOG_WARNING);
            break;
        }
        case ChildExit:
        {
            WriteLog(appptr->GetLogger(), "CHILD PROCESS EXIT SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined1:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 1 SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined2:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 2 SIGNAL", LOG_WARNING);
            break;
        }
        case WindowResized:
        {
            WriteLog(appptr->GetLogger(), "WINDOW SIZED CHANGED SIGNAL", LOG_WARNING);
            break;
        }
        default:
        {
            WriteLog(appptr->GetLogger(), "UNKNOWN SIGNAL", LOG_WARNING);
            break;
        }
    }
}

int generate_device_id(const char *network_interface, char *device_id)
{
    struct ifaddrs *ifaddr=nullptr;
    struct ifaddrs *ifa = nullptr;
    int i = 0;

    if (getifaddrs(&ifaddr) == -1)
    {
         perror("getifaddrs");
    }
    else
    {
         for ( ifa = ifaddr; ifa != nullptr; ifa = ifa->ifa_next)
         {
             if ( (ifa->ifa_addr) && (ifa->ifa_addr->sa_family == AF_PACKET) )
             {
                  struct sockaddr_ll *s = (struct sockaddr_ll*)ifa->ifa_addr;

                  if(strcmp(network_interface, ifa->ifa_name) == 0)
                  {
                      for (i=0; i <s->sll_halen; i++)
                      {
                          char temp[3] = {0};
                          sprintf(temp, "%02x", (s->sll_addr[i]));
                          strcat(device_id, temp);
                      }

                      for(int idx = 0; device_id[idx] != 0; idx++)
                      {
                            if(isalpha(device_id[idx]))
                            {
                                device_id[idx] = toupper(device_id[idx]);
                            }
                      }
                  }
             }
         }
         freeifaddrs(ifaddr);
    }
    return 0;
}

