#include "ProtocolHTTP.hpp"
#include <curl/curl.h>

CURL *curl;
CURLcode res;

ProtocolHTTP::ProtocolHTTP()
{
     curl_global_init(CURL_GLOBAL_ALL);
}

ProtocolHTTP::~ProtocolHTTP()
{
    curl_global_cleanup();
}

bool ProtocolHTTP::Initialize(void *loggerid, const std::string &str_server, int num_port, const std::string device_id, IDownLink *dlink)
{
    port = num_port;
    server = str_server;
    deviceid = device_id;
    downlink = dlink;
    logger_id = loggerid;

    return true;
}

bool ProtocolHTTP::Connect()
{
    return true;
}

bool ProtocolHTTP::Close()
{
    return true;
}

bool ProtocolHTTP::Release()
{
    return true;
}

bool ProtocolHTTP::SendData(const std::string &str, const std::string &uri)
{
    try
    {
        bool res = true;

        char url_buffer[1024] = {0};
        sprintf(url_buffer, "http://%s:%d/%s/data/", server.c_str(), port, uri.c_str());

        curl = curl_easy_init();

        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, url_buffer);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str.c_str());

            res = curl_easy_perform(curl);

            if(res != CURLE_OK)
            {
                res = false;
            }

        }

        curl_easy_cleanup(curl);
    }
    catch(...)
    {
        if(curl)
        {
            curl_easy_cleanup(curl);
        }
    }

    return true;
}

bool ProtocolHTTP::SendResponse(const std::string &str, const std::string &uri)
{
    try
    {
        bool res = true;

        char url_buffer[1024] = {0};
        sprintf(url_buffer, "http://%s:%d/%s/response/", server.c_str(), port, uri.c_str());

        curl = curl_easy_init();

        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, url_buffer);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str.c_str());

            res = curl_easy_perform(curl);

            if(res != CURLE_OK)
            {
                res = false;
            }

        }

        curl_easy_cleanup(curl);
    }
    catch(...)
    {
        if(curl)
        {
            curl_easy_cleanup(curl);
        }
    }

    return true;
}

bool ProtocolHTTP::SendRequest(const std::string &str, const std::string &uri)
{
    try
    {
        bool res = true;

        char url_buffer[1024] = {0};
        sprintf(url_buffer, "http://%s:%d/%s/request/", server.c_str(), port, uri.c_str());

        curl = curl_easy_init();

        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, url_buffer);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str.c_str());

            res = curl_easy_perform(curl);

            if(res != CURLE_OK)
            {
                res = false;
            }

        }

        curl_easy_cleanup(curl);
    }
    catch(...)
    {
        if(curl)
        {
            curl_easy_cleanup(curl);
        }
    }

    return true;
}

bool ProtocolHTTP::SendEvent(const std::string &str, const std::string &uri)
{
    try
    {
        bool res = true;

        char url_buffer[1024] = {0};
        sprintf(url_buffer, "http://%s:%d/%s/event/", server.c_str(), port, uri.c_str());

        curl = curl_easy_init();

        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, url_buffer);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, str.c_str());

            res = curl_easy_perform(curl);

            if(res != CURLE_OK)
            {
                res = false;
            }

        }

        curl_easy_cleanup(curl);
    }
    catch(...)
    {
        if(curl)
        {
            curl_easy_cleanup(curl);
        }
    }

    return true;
}

