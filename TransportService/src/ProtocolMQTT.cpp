#include "ProtocolMQTT.hpp"
#include <stdio.h>
#include <mosquitto.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <RTUCore/Logger.h>

#include <thread>

#pragma pack(1)
typedef struct MQTTParams
{
    bool run;
    int mqtt_port;
    char mqtt_host[33];
    char client_id[33];
    char device_id[33];
    struct mosquitto *mosq;
    int keep_alive;
    void* logger_id;
    IDownLink* downlink;
}MQTTParams;

static void OnConnect(struct mosquitto *mosq, void *obj, int result);
static void OnMessage(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);
static void* ReceiverThread();

static MQTTParams* appptr = nullptr;

ProtocolMQTT::ProtocolMQTT()
{
}

ProtocolMQTT::~ProtocolMQTT()
{
}

bool ProtocolMQTT::Initialize(void *loggerid, const std::string &str_server, int num_port, const std::string device_id, IDownLink *dlink)
{
    appptr = new MQTTParams();

    int rc = 0;
    port = num_port;
    server = str_server;
    deviceid = device_id;
    downlink = dlink;
    logger_id = loggerid;

    bool clean_session = true;

    appptr->run = true;
    appptr->mqtt_port = num_port;
    appptr->keep_alive = 60;
    strcpy(appptr->mqtt_host, str_server.c_str());
    memset(appptr->client_id, 0, 24);
    strcpy(appptr->client_id, device_id.c_str());
    appptr->logger_id = loggerid;
    appptr->downlink = dlink;
    strcpy(appptr->device_id, device_id.c_str());

    rc = mosquitto_lib_init();
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, "Could not initialize MQTT stack", LOG_ERROR);
        return false;
    }

    appptr->mosq = mosquitto_new(nullptr, clean_session, nullptr);

    if(!appptr->mosq)
    {
        WriteLog(logger_id, "Could not create a MQTT client", LOG_ERROR);
        return false;
    }

    return true;
}

bool ProtocolMQTT::Connect()
{
    int rc = 0;

    mosquitto_message_callback_set(appptr->mosq, OnMessage);

    rc = mosquitto_connect(appptr->mosq, appptr->mqtt_host, appptr->mqtt_port, appptr->keep_alive);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, "Could not connect to MQTT broker", LOG_ERROR);
        return false;
    }

    WriteInformation(logger_id, "Connected to MQTT broker");

    std::thread receiver(&ReceiverThread);
    receiver.detach();

    //Cloud / Web can send us request, response and data only
    //So we will subscribe for those topics

    std::string subscription_topic;

    subscription_topic.clear();
    subscription_topic += deviceid + "/request/+";
    rc = mosquitto_subscribe(appptr->mosq, nullptr, subscription_topic.c_str(), 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, (subscription_topic + " could not subscribe").c_str(), LOG_ERROR);
        return false;
    }

    subscription_topic.clear();
    subscription_topic += deviceid + "/response/+";
    rc = mosquitto_subscribe(appptr->mosq, nullptr, subscription_topic.c_str(), 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, (subscription_topic + " could not subscribe").c_str(), LOG_ERROR);
        return false;
    }

    subscription_topic.clear();
    subscription_topic += deviceid + "/data/+";
    rc = mosquitto_subscribe(appptr->mosq, nullptr, subscription_topic.c_str(), 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, (subscription_topic + " could not subscribe").c_str(), LOG_ERROR);
        return false;
    }

    WriteInformation(logger_id, "Subscribed all topics");

    return true;
}

bool ProtocolMQTT::Close()
{
    appptr->run = false;
    sleep(4);

    int rc = mosquitto_disconnect(appptr->mosq);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, "Could not disconnect from MQTT broker", LOG_ERROR);
        return false;
    }

    mosquitto_destroy(appptr->mosq);
    return true;
}

bool ProtocolMQTT::Release()
{
    mosquitto_lib_cleanup();

    delete  appptr;

    return true;
}

bool ProtocolMQTT::SendData(const std::string &str, const std::string &uri)
{
    std::string publish_topic = uri + "/data/";

    if(mosquitto_publish(appptr->mosq, nullptr, publish_topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, (publish_topic + " could not publish").c_str(), LOG_ERROR);
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendResponse(const std::string &str, const std::string &uri)
{
    std::string publish_topic = uri + "/response/";

    if(mosquitto_publish(appptr->mosq, nullptr, publish_topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, (publish_topic + " could not publish").c_str(), LOG_ERROR);
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendRequest(const std::string &str, const std::string &uri)
{
    std::string publish_topic = uri + "/request/";

    if(mosquitto_publish(appptr->mosq, nullptr, publish_topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        WriteLog(logger_id, (publish_topic + " could not publish").c_str(), LOG_ERROR);
        return false;
    }

    return true;
}

bool ProtocolMQTT::SendEvent(const std::string &str, const std::string &uri)
{
    std::string publish_topic = uri + "/event/";

    if(mosquitto_publish(appptr->mosq, nullptr, publish_topic.c_str(), str.length(), str.c_str(), 0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return true;
}

static void* ReceiverThread()
{
    int rc = 0;

    while(appptr->run)
    {
        rc = mosquitto_loop(appptr->mosq, -1, 1);

        if(appptr->run && rc)
        {
            WriteLog(appptr->logger_id, "MQTT connection error ... reconnecting in 10 seconds", LOG_ERROR);
            sleep(2);

            rc = mosquitto_reconnect(appptr->mosq);

            if(rc != MOSQ_ERR_SUCCESS)
            {
                WriteLog(appptr->logger_id, "MQTT ould not reconnect", LOG_ERROR);
            }
        }
    }

    return nullptr;
}

void OnConnect(struct mosquitto *mosq, void *obj, int result)
{
    WriteLog(appptr->logger_id, "MQTT connect callback", LOG_INFO);
}

void OnMessage(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    bool match_event = 0;
    bool match_data = 0;
    bool match_request = 0;
    bool match_response = 0;
    char mtype = 'U';

    std::string subscription_topic;

    subscription_topic.clear();
    subscription_topic += std::string(appptr->device_id) + "/request/";

    if(strcmp(subscription_topic.c_str(), message->topic) == 0)
    {
        match_request = true;
    }

    subscription_topic.clear();
    subscription_topic += std::string(appptr->device_id) + "/event/";
    if(strcmp(subscription_topic.c_str(), message->topic) == 0)
    {
        match_request = true;
    }

    subscription_topic.clear();
    subscription_topic += std::string(appptr->device_id) + "/data/";
    if(strcmp(subscription_topic.c_str(), message->topic) == 0)
    {
        match_request = true;
    }

    subscription_topic.clear();
    subscription_topic += std::string(appptr->device_id) + "/response/";
    if(strcmp(subscription_topic.c_str(), message->topic) == 0)
    {
        match_request = true;
    }

    if (match_event || match_data || match_request || match_response)
    {
        if(appptr->downlink)
        {
            appptr->downlink->OnDownLink((const char*)message->payload, message->payloadlen, mtype);
        }
    }
    else
    {
        WriteLog(appptr->logger_id, "Unsubscribed message received, ignored", LOG_WARNING);
    }
}



