#ifndef I_PROTOCOL_INET
#define I_PROTOCOL_INET

#include <string>

class IDownLink
{
public:
    IDownLink() {}
    virtual ~IDownLink() {}
    virtual bool OnDownLink(const std::string &message, long len, char mtype) = 0;
};

class IProtocolINET
{
public:
    IProtocolINET() {}
    virtual ~IProtocolINET() {}
    virtual bool Initialize(void* loggerid, const std::string &str_server, int num_port, const std::string device_id, IDownLink* dlink = nullptr) = 0;
    virtual bool Connect() = 0;
    virtual bool Close() = 0;
    virtual bool Release() = 0;
    virtual bool SendData(const std::string &str, const std::string &uri) = 0;
    virtual bool SendResponse(const std::string &str, const std::string &uri) = 0;
    virtual bool SendRequest(const std::string &str, const std::string &uri) = 0;
    virtual bool SendEvent(const std::string &str, const std::string &uri) = 0;
protected:
   std::string server;
   int port;
   std::string deviceid;
   IDownLink* downlink;
   void* logger_id;
};

#endif
