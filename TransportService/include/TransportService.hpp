#ifndef TRANSPORT_SERVICE
#define TRANSPORT_SERVICE

#include "IProtocolINET.hpp"
#include <string>

class IServiceCallback
{
public:
    IServiceCallback() {}
    virtual ~IServiceCallback() {}
    virtual void OnNodeOnline(const std::string &nodename) =0;
    virtual void OnNodeOffline(const std::string &nodename) =0;
    virtual void OnData(const std::string &nodename, const char* messagebuffer, char dtype) =0;
    virtual void OnEvent(const std::string &nodename, const char* messagebuffer, char dtype) =0;
    virtual void OnRequest(const std::string &nodename, const char* messagebuffer, char dtype) =0;
    virtual void OnResponse(const std::string &nodename, const char* messagebuffer, char dtype) =0;
};

class TransportService : public IServiceCallback, IDownLink
{
public:
    TransportService();
    virtual ~TransportService() override;
    bool Initialize();
    bool Start();
    bool ReStart();
    bool Stop();
    void* GetLogger();
protected:
    void OnNodeOnline(const std::string &nodename) override;
    void OnNodeOffline(const std::string &nodename) override;
    void OnData(const std::string &nodename, const char* messagebuffer, char dtype) override;
    void OnEvent(const std::string &nodename, const char* messagebuffer, char dtype) override;
    void OnRequest(const std::string &nodename, const char* messagebuffer, char dtype) override;
    void OnResponse(const std::string &nodename, const char* messagebuffer, char dtype) override;
    bool OnDownLink(const std::string &message, long len, char mtypechar) override;
private:
    void Upload(const std::string &nodename, const char* messagebuffer, char dtype, const char* mtype);

    void* message_bus;
    void* logger;
    void* config;

    IProtocolINET *inet_protocol;

    std::string version;
    std::string protocol;
    std::string server;
    std::string uri;
    std::string header_template;

    char device_id[25];

    int port;
    bool enabled;
    float latitude;
    float longitude;
};

#endif
