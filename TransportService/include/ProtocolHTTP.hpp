#ifndef PROTOCOL_HTTP
#define PROTOCOL_HTTP

#include "IProtocolINET.hpp"

class ProtocolHTTP : public IProtocolINET
{
public:
    ProtocolHTTP();
    virtual ~ProtocolHTTP() override;
    bool Initialize(void* loggerid, const std::string &str_server, int num_port, const std::string device_id, IDownLink* dlink = nullptr) override;
    bool Connect() override;
    bool SendData(const std::string &str, const std::string &uri) override;
    bool SendResponse(const std::string &str, const std::string &uri) override;
    bool SendRequest(const std::string &str, const std::string &uri) override;
    bool SendEvent(const std::string &str, const std::string &uri) override;
    bool Close() override;
    bool Release() override;
};


#endif
