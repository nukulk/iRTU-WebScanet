#ifndef DATA_ENTITY
#define DATA_ENTITY

#include <string>
#include <vector>
#include <list>
#include "Variant.hpp"

class Database;

class DataEntity
{
public:
    DataEntity(Database* db, std::string tabname);
    virtual ~DataEntity();
    void clear();
    std::vector<Variant*>* getAllRecords();
    std::vector<Variant*>* getSelectedRecords(std::string keyname, Variant value);
    bool remove(std::string keyname, Variant value);
    virtual bool create(std::vector<Variant> &record)=0;
    virtual bool update(std::vector<Variant> &record)=0;
    Variant maximumValue(std::string keyname);
    Variant maximumValue(std::string keyname, std::string filter, Variant filterval);
private:
    void substitueValue(Variant &var, std::string &str);
protected:
    Database*           _Database;
    std::string         _TableName;
    std::vector<std::vector<Variant>>   _RecordList;
};

#endif
