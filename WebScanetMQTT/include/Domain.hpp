#ifndef RTU_DOMAIN
#define RTU_DOMAIN

#include <utility>
#include <vector>
#include <string>

#include "Database.hpp"

typedef std::vector<std::pair<std::string, std::string>> PeripheralPayload;

typedef struct iRTUPayload
{
    char encoding[17];
    char device_id[33];
    char sender[33];
    char data_type[9];
    char version[12];
    char message_type[11];
    double latitude;
    double longitude;
    char timestamp[25];
    std::vector<PeripheralPayload> payload;
}iRTUPayload;

typedef enum DomainType
{
    Lighting=0,
    WaterPump=1
}DomainType;

class Domain
{
public:
    Domain();
    ~Domain();

    bool InitializeTelemetryDB(const std::string db_name, const std::string db_server, const std::string db_port, const std::string db_user, const std::string db_pass);
    bool ConnectTelemetryDB();

    bool InitializeMasterDB(const std::string db_name, const std::string db_server, const std::string db_port, const std::string db_user, const std::string db_pass);
    bool ConnectMasterDB();

    bool ProcessPayload(const std::string &payload_str, const std::string &device_id, const std::string &latitude, const std::string &longitude, const std::string &timestamp);
private:
    std::string telemetry_server;
    std::string telemetry_port;
    std::string telemetry_name;
    std::string telemetry_user;
    std::string telemetry_pass;

    Database telemetry_db;

    std::string master_server;
    std::string master_port;
    std::string master_name;
    std::string master_user;
    std::string master_pass;

    Database master_db;
};

#endif
