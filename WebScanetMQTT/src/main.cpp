#include <vector>
#include <string>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <unistd.h>
#include <dirent.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <pthread.h>
#include <mosquitto.h>

#include "Domain.hpp"
#include <RTUCore/Configuration.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>

#pragma pack(1)
typedef struct MQTTParams
{
    bool run;
    int mqtt_port;
    char mqtt_host[33];
    char client_id[33];
    struct mosquitto *mosq;
    pthread_t receiver_thread;
    int keep_alive;
}MQTTParams;
#pragma pop

static void OnSignalReceived(SignalType s);
static void OnConnect(struct mosquitto *mosq, void *obj, int result);
static void OnMessage(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);

static MQTTParams* appptr = nullptr;
static Domain domain;
static void* logger = nullptr;

static std::string water_pump="active_load:70, burn_hours:12, total_flow:500, is_tubewell_on:true, is_ol_trip:true, is_spp_trip:true, is_local:true, is_power_normal:true, cumulative_load:350, temperature:55, vibrationx:5, vibrationz:6, pump_efficiency:80, peripheral_id:500";
static std::string lighting_feeder_panel="active_load:50, rated_load:75, mcb_trip:false, is_local:true, is_door_open:false, contactor_feedback_error:false, power_status:true, cumulative_load:8000, energy_consumption_actual:0, energy_consumption_projected:0, installed_led_count:55, off_led_count:20, peripheral_id:200";

int main(int, char **)
{
    void* config = nullptr;
    int rc = 0;
    MQTTParams params;

    printf("00\n");
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        return -1;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    std::string mqtt_server;
    std::string mqtt_port;

    std::string telemetry_server;
    std::string telemetry_port;
    std::string telemetry_name;
    std::string telemetry_user;
    std::string telemetry_pass;

    std::string master_server;
    std::string master_port;
    std::string master_name;
    std::string master_user;
    std::string master_pass;

    config = configuration_allocate_default();

    if(config == nullptr)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    configuration_get_value_as_string(config, "default", "mqtt_server", mqtt_server);
    configuration_get_value_as_string(config, "default", "mqtt_port", mqtt_port);

    configuration_get_value_as_string(config, "default", "telemetry_server", telemetry_server);
    configuration_get_value_as_string(config, "default", "telemetry_port", telemetry_port);
    configuration_get_value_as_string(config, "default", "telemetry_name", telemetry_name);
    configuration_get_value_as_string(config, "default", "telemetry_user", telemetry_user);
    configuration_get_value_as_string(config, "default", "telemetry_pass", telemetry_pass);

    configuration_get_value_as_string(config, "default", "master_server", master_server);
    configuration_get_value_as_string(config, "default", "master_port", master_port);
    configuration_get_value_as_string(config, "default", "master_name", master_name);
    configuration_get_value_as_string(config, "default", "master_user", master_user);
    configuration_get_value_as_string(config, "default", "master_pass", master_pass);

    configuration_release(config);

    if(!domain.InitializeTelemetryDB(telemetry_name, telemetry_server, telemetry_port, telemetry_user, telemetry_pass))
    {
        WriteLog(logger, "Could not initialize telemetry database", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    if(!domain.ConnectTelemetryDB())
    {
        WriteLog(logger, "Could not connect to the telemetry database", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    if(!domain.InitializeMasterDB(master_name, master_server, master_port, master_user, master_pass))
    {
        WriteLog(logger, "Could not initialize master database", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    if(!domain.ConnectTelemetryDB())
    {
        WriteLog(logger, "Could not connect to the master database", LOG_ERROR);
        logger_release(logger);
        return -1;
    }

    appptr = &params;

    params.run = true;
    params.mqtt_port = atoi(mqtt_port.c_str());
    params.keep_alive = 60;
    strcpy(params.mqtt_host, mqtt_server.c_str());
    memset(params.client_id, 0, 24);
    strcpy(params.client_id, "webscanet");

    rc = mosquitto_lib_init();
    if(rc != MOSQ_ERR_SUCCESS)
    {
        printf("Could not initialize libmosquitto - %d\n", rc);
        return -1;
    }

    params.mosq = mosquitto_new(params.client_id, true, nullptr);

    if(params.mosq)
    {
        mosquitto_connect_callback_set(params.mosq, OnConnect);
        mosquitto_message_callback_set(params.mosq, OnMessage);

        rc = mosquitto_connect(params.mosq, params.mqtt_host, params.mqtt_port, params.keep_alive);

        if(rc != MOSQ_ERR_SUCCESS)
        {
            printf("Could not connect - %d\n", rc);
            return -1;
        }

        rc = mosquitto_subscribe(params.mosq, nullptr, "irtu/data/+", 0);
        if(rc != MOSQ_ERR_SUCCESS)
        {
            printf("Could not subscribe - %d\n", rc);
            return -1;
        }

        rc = mosquitto_subscribe(params.mosq, nullptr, "irtu/event/+", 0);
        if(rc != MOSQ_ERR_SUCCESS)
        {
            printf("Could not subscribe - %d\n", rc);
            return -1;
        }

        rc = mosquitto_subscribe(params.mosq, nullptr, "irtu/request/+", 0);
        if(rc != MOSQ_ERR_SUCCESS)
        {
            printf("Could not subscribe - %d\n", rc);
            return -1;
        }

        rc = mosquitto_subscribe(params.mosq, nullptr, "irtu/response/+", 0);
        if(rc != MOSQ_ERR_SUCCESS)
        {
            printf("Could not subscribe - %d\n", rc);
            return -1;
        }

        while(params.run)
        {
            rc = mosquitto_loop(params.mosq, -1, 1);

            if(params.run && rc)
            {
                printf("Connection error ... reconnecting in 10 seconds\n");
                sleep(10);

                rc = mosquitto_reconnect(params.mosq);

                if(rc != MOSQ_ERR_SUCCESS)
                {
                    printf("Could not reconnect - %d\n", rc);
                }
            }
        }

        mosquitto_destroy(params.mosq);
    }

    mosquitto_lib_cleanup();

    return rc;
}

void OnSignalReceived(SignalType s)
{
    appptr->run = false;
}

void OnConnect(struct mosquitto *mosq, void *obj, int result)
{
    printf("Connect callback, rc=%d\n", result);
}

void OnMessage(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    bool match_event = 0;
    bool match_data = 0;
    bool match_request = 0;
    bool match_response = 0;

    if(strcmp("irtu/request/", message->topic) == 0)
    {
        match_request = true;
    }

    if(strcmp("irtu/response/", message->topic) == 0)
    {
        match_response = true;
    }

    if(strcmp("irtu/data/", message->topic) == 0)
    {
        match_data = true;
    }

    if(strcmp("irtu/event/", message->topic) == 0)
    {
        match_event = true;
    }

    if (match_event || match_data || match_request || match_response)
    {
        std::vector<std::string> header_fields;
        std::string header;
        std::string device_payload = (char*)message->payload;
        std::string peripheral_payload;

        str_ex_keyvalue_with_str(device_payload, header, peripheral_payload, "\r\n");
        str_ex_split_with_char(header, header_fields, ',');

        if(header_fields.size() <9)
        {
            WriteLog(logger, "Rejecting downlink command due field count mismatch, number of header fields must be 9", LOG_ERROR);
            return;
        }

        char ptype = 'D';
        char dtype = 'R';

        std::string datatype = header_fields[4];
        std::string sender_application = header_fields[3];
        std::string sender_mqtt_client = header_fields[2];

        str_ex_alltrim(datatype);
        str_ex_alltrim(sender_application);
        str_ex_alltrim(sender_mqtt_client);

        if(header_fields[6] == "request")
        {
            ptype = 'Q';
        }

        if(header_fields[6] == "response")
        {
            ptype = 'R';
        }

        if(header_fields[6] == "data")
        {
            ptype = 'D';
        }

        if(header_fields[6] == "event")
        {
            ptype = 'E';
        }

        if(datatype == "text")
        {
            dtype = 'T';
        }
        else
        {
            if(datatype == "image")
            {
                dtype = 'I';
            }
            else
            {
                if(datatype == "audio")
                {
                    dtype = 'A';
                }
                else
                {
                    if(datatype == "video")
                    {
                        dtype = 'V';
                    }
                    else
                    {
                        dtype = 'R';
                    }
                }
            }
        }

        domain.ProcessPayload(peripheral_payload, sender_mqtt_client, header_fields[7], header_fields[8], header_fields[10]);
    }
    else
    {
        WriteLog(logger, "Got unsubscribed message. Ignored", LOG_WARNING);
    }
}
