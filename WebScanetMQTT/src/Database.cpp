#include "Database.hpp"
#include <memory.h>
#include <string.h>
#include <stdlib.h>
#include <memory.h>
#include <RTUCore/StringEx.h>

BindStruct::BindStruct()
{
    memset((void*)&columnname[0], 0, 33);
    columntype = Void;
    precision = 6;
}

void BindStruct::setColumnName(const char* ptr)
{
    memset((void*)&columnname[0], 0, 33);
    strncpy((char*)&columnname[0], ptr, 32);
}

void BindStruct::setColumnType(const VariantType vtype)
{
    columntype = vtype;
}

void BindStruct::setColumnPrecision(const unsigned int prec)
{
    precision = prec;
}


const char* BindStruct::getColumnName()
{
    return (const char*)&columnname[0];
}

VariantType BindStruct::getColumnType()
{
    return columntype;
}

unsigned short BindStruct::getColumnPrecision()
{
    return precision;
}

Column::Column()
{
    memset((void*)&_ColumnName[0],0,64);
}

Column::~Column()
{

}

const char *Column::getName()
{
    return (const char*)&_ColumnName[0];
}

long Column::getLength()
{
    return _ColumnLength;
}

long Column::getPosition()
{
    return _ColumnPosition;
}

VariantType Column::getType()
{
    return _ColumnType;
}

unsigned long Column::getNativeType()
{
    return _NativeType;
}


void Column::setName(std::string val)
{
    memset((void*)&_ColumnName[0],0,64);
    memcpy((void*)&_ColumnName[0],val.c_str(),63);
}

void Column::setLength(long val)
{
    _ColumnLength = val;
}

void Column::setType(VariantType val)
{
    _ColumnType = val;
}

void Column::setPosition(long val)
{
    _ColumnPosition = val;
}

void Column::setNativeType(unsigned long val)
{
    _NativeType = val;
}

void Column::setByteUse(bool buse)
{
    _ByteUsed = buse;
}

bool Column::getByteUse()
{
    return _ByteUsed;
}

int Column::getScale()
{
    return _Scale;
}

int Column::getPrecision()
{
    return _Precision;
}

void Column::setScale(int inscl)
{
    _Scale = inscl;
}

void Column::setPrecision(int prec)
{
    _Precision = prec;
}

void* Column::data()
{
    return (void*)&_Data[0];
}

ConnectionHandle::ConnectionHandle()
{
    conn = nullptr;
    res = nullptr;
}

ConnectionHandle::~ConnectionHandle()
{

}

void ConnectionHandle::acquire()
{

}

void ConnectionHandle::release()
{

}


StatementHandle::StatementHandle()
{
    rowset = nullptr;
    rowcount = 0;
    columnCount = 0;
}

StatementHandle::~StatementHandle()
{

}

Database::Database()
{
    commit_on = true;
    _arraysize = 64;
}

Database::~Database()
{

}

bool Database::initialize()
{
    //Dummy function
    return true;
}

bool Database::deinitialize()
{
    //Dummy function
    return false;
}

bool Database::setupDateFormat(std::string errmsg, int &errorCode)
{
    errorCode = 0;
    errmsg.clear();

    if(currentStatement.rowset)
    {
        PQclear(currentStatement.rowset);
        currentStatement.rowset = nullptr;
    }

    ConnStatusType res = PQstatus(currentConnection.conn);
    if (res != CONNECTION_OK)
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)res;
        return false;
    }

    currentStatement.rowset = PQexec(currentConnection.conn, "set datestyle = 'SQL, DMY'");

    ExecStatusType exres = PQresultStatus(currentStatement.rowset);

    if (exres == PGRES_BAD_RESPONSE || exres == PGRES_EMPTY_QUERY || exres == PGRES_FATAL_ERROR )
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)exres;
        return false;
    }

    PQclear(currentStatement.rowset);
    currentStatement.rowset = nullptr;

    return true;
}

void Database::rowLimitClause(std::string &str)
{

}

bool Database::createConnection(const std::string &dbname, const std::string &uname, const std::string &passwd, const std::string &host, const int &port, std::string &errmsg, int &errorCode)
{
    _dbname = dbname;
    _uname = uname;
    _passwd = passwd;
    _host = host;
    _port = port;

    Variant vport((const long)port);
    vport.getString(_portstr);

    currentConnection.conn = PQsetdbLogin(_host.c_str(), _portstr.c_str(), nullptr, nullptr, _dbname.c_str(), _uname.c_str(), _passwd.c_str());

    ConnStatusType res = PQstatus(currentConnection.conn);
    if (res != CONNECTION_OK)
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)res;
        return false;
    }

    //Do not return false yet, even if fails
    setupDateFormat(errmsg, errorCode);

    return true;
}

bool Database::reConnect()
{
    std::string errmsg;
    int errorCode;

    currentConnection.conn = PQsetdbLogin(_host.c_str(), _portstr.c_str(), nullptr, nullptr, _dbname.c_str(), _uname.c_str(), _passwd.c_str());

    ConnStatusType res = PQstatus(currentConnection.conn);
    if (res != CONNECTION_OK)
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)res;
        return false;
    }

    //Do not return false yet, even if fails
    setupDateFormat(errmsg, errorCode);

    return true;
}

bool Database::closeConnection()
{

    ConnStatusType stat = PQstatus(currentConnection.conn);

    if(stat != CONNECTION_NEEDED || stat != CONNECTION_BAD)
    {
        PQfinish(currentConnection.conn);
    }

    return true;
}

bool Database::executeDML(const char *sqlstr, std::string &errmsg, int &errorCode)
{
    errorCode = 0;
    errmsg.clear();

    ConnStatusType res = PQstatus(currentConnection.conn);
    if (res != CONNECTION_OK)
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)res;
        return false;
    }

    PGresult* dmlresult = PQexec(currentConnection.conn, sqlstr);

    ExecStatusType exres = PQresultStatus(dmlresult);

    PQclear(dmlresult);

    if (exres == PGRES_BAD_RESPONSE || exres == PGRES_EMPTY_QUERY || exres == PGRES_FATAL_ERROR )
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)exres;
        return false;
    }

    return true;
}

bool Database::executeSQL(const char *sqlstr, std::string &errmsg, int &errorCode)
{
    errorCode = 0;
    errmsg.clear();

    if(currentStatement.rowset)
    {
        PQclear(currentStatement.rowset);
        currentStatement.rowset = nullptr;
        currentStatement.rowFetched = -1;
        currentStatement.rowcount = -1;
    }

    ConnStatusType res = PQstatus(currentConnection.conn);
    if (res != CONNECTION_OK)
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)res;
        return false;
    }

    currentStatement.rowset = PQexec(currentConnection.conn, sqlstr);

    ExecStatusType exres = PQresultStatus(currentStatement.rowset);

    if (exres == PGRES_BAD_RESPONSE || exres == PGRES_EMPTY_QUERY || exres == PGRES_FATAL_ERROR )
    {
        errmsg =  PQerrorMessage(currentConnection.conn);
        errorCode = (int)exres;
        return false;
    }

    //Find out the number of columns returned, with their names, types and sizes
    if(!getColumns(errmsg, errorCode))
    {
        getError(errmsg, errorCode);
        PQclear(currentStatement.rowset);
        return false;
    }

    currentStatement.rowcount = PQntuples(currentStatement.rowset);

    if(currentStatement.rowcount < 1)
    {
        errmsg = "No rows returned";
        errorCode = -1;
        return false;
    }

    currentStatement.rowFetched = -1;

    return true;
}

Variant* Database::getRow(bool &fetchpending, std::string &errmsg, int &errorCode)
{
    currentStatement.rowFetched++;

    if(currentStatement.rowFetched >= currentStatement.rowcount)
    {
        errmsg = "All rows have been fetched";
        errorCode = -1;
        fetchpending = false;
        PQclear(currentStatement.rowset);
        currentStatement.rowset = nullptr;
        currentStatement.rowFetched = -1;
        currentStatement.rowcount = -1;
        return nullptr;
    }

    for(int ctr = 0; ctr < currentStatement.columnCount ; ctr++)
    {
        char* ptr = PQgetvalue(currentStatement.rowset, currentStatement.rowFetched, ctr);

        switch(currentStatement.columnList[ctr].getNativeType())
        {
            case 18:
            {
                currentStatement.dataRow[ctr].setType(Char);
                currentStatement.dataRow[ctr].setData((char)ptr[0]);
                break;
            }
            case 1043:
            {
                currentStatement.dataRow[ctr].setType(AsciiString);
                currentStatement.dataRow[ctr].setData((const char*)ptr,true);
                break;
            }
            case 1700:
            {
                if(strstr(ptr,"."))
                {
                    currentStatement.dataRow[ctr].setType(Decimal);
                    double fval = atof(ptr);
                    currentStatement.dataRow[ctr].setData(fval);
                }
                else
                {
                    currentStatement.dataRow[ctr].setType(Number);
                    long ival = atol(ptr);
                    currentStatement.dataRow[ctr].setData(ival);
                }
                break;
            }
            case 23:
            case 20:
            {
                currentStatement.dataRow[ctr].setType(Number);
                long ival = atol(ptr);
                currentStatement.dataRow[ctr].setData(ival);
                break;
            }
            case 701:
            {
                currentStatement.dataRow[ctr].setType(Decimal);
                double fval = atof(ptr);
                currentStatement.dataRow[ctr].setData(fval);
                break;
            }
            case 1114:
            {
                struct tm timeinfo;
                memset((void*)&timeinfo,0,sizeof(struct tm));
                translatePGTStoCTS(ptr, timeinfo);

                currentStatement.dataRow[ctr].setType(TimeStamp);
                currentStatement.dataRow[ctr].setData(timeinfo);
                break;
            }
            case 16:
            {
                currentStatement.dataRow[ctr].setType(Boolean);
                if(ptr[0] == 't' || ptr[0] == 'T')
                {
                    currentStatement.dataRow[ctr].setData(true);
                }
                else
                {
                    currentStatement.dataRow[ctr].setData(false);
                }
                break;
            }
            default:
            {
                currentStatement.dataRow[ctr].setType(Raw);
                currentStatement.dataRow[ctr].setData(ptr);
                break;
            }
        }
    }

    fetchpending = true;

    return &currentStatement.dataRow[0];
}


bool Database::startTransaction()
{
    ConnStatusType res = PQstatus(currentConnection.conn);

    if (res != CONNECTION_OK)
    {
        return false;
    }

    PGresult* dmlresult = PQexec(currentConnection.conn, "begin");

    ExecStatusType exres = PQresultStatus(dmlresult);

    PQclear(dmlresult);

    if (exres == PGRES_BAD_RESPONSE || exres == PGRES_EMPTY_QUERY || exres == PGRES_FATAL_ERROR )
    {
        return false;
    }

    return true;
}

bool Database::commit()
{
    ConnStatusType res = PQstatus(currentConnection.conn);

    if (res != CONNECTION_OK)
    {
        return false;
    }

    PGresult* dmlresult = PQexec(currentConnection.conn, "commit");

    ExecStatusType exres = PQresultStatus(dmlresult);

    PQclear(dmlresult);

    if (exres == PGRES_BAD_RESPONSE || exres == PGRES_EMPTY_QUERY || exres == PGRES_FATAL_ERROR )
    {
        return false;
    }

    return true;
}

bool Database::rollback()
{
    ConnStatusType res = PQstatus(currentConnection.conn);

    if (res != CONNECTION_OK)
    {
        return false;
    }

    PGresult* dmlresult = PQexec(currentConnection.conn, "rollback");

    ExecStatusType exres = PQresultStatus(dmlresult);

    PQclear(dmlresult);

    if (exres == PGRES_BAD_RESPONSE || exres == PGRES_EMPTY_QUERY || exres == PGRES_FATAL_ERROR )
    {
        return false;
    }

    return true;
}

void Database::enableAutoCommit()
{
	commit_on = true;
}

std::string Database::getNativeTimeStamp(DateTime &timestamp)
{
    std::string ostsstr = "";
    std::string tsbuffer = "";

    tsbuffer = "to_timestamp('_TS_', 'yyyy/mm/dd hh24:mi:ss')";
    ostsstr = timestamp.getDateString("yyyy/MM/dd hh:mm:ss");
    str_ex_replace_with_str(tsbuffer, "_TS_", ostsstr);
    return tsbuffer;
}

bool Database::isOpen()
{
    return false;
}

void Database::getError(std::string &str, int &errorCode)
{
    //Dummy function
    return;
}

int Database::errorDBToAppType(int errorcode)
{
	switch(errorcode)
	{
	case 1:
		return ERR_UNIQ_CONST_VIOLATION;
	case 1400:
        return ERR_NULL_CONST_VIOLATION;
	case 936:
		return ERR_MISSING_COLUMN;
	case 917:
		return ERR_INAVLID_SQL_STRING;
	case 984:
	case 911:
		return ERR_INVALID_INT;
    case 28:
    case 1001:
    case 1012:
    case 3113:
    case 3114:
    case 3135:
    case 2134:
        return ERR_NO_CONNECTION;
	default:
		return ERR_UNKNOWN_DB_ERROR;
	}
}

ConnectionHandle *Database::getConnectionHandle()
{
    return &currentConnection;
}

StatementHandle* Database::getStatementHandle()
{
    return &currentStatement;
}

void Database::releaseReadBuffers()
{

}

bool Database::getColumns(std::string &errmsg, int &errorCode)
{
    currentStatement.columnCount = PQnfields(currentStatement.rowset);

    for(int ctr = 0; ctr < currentStatement.columnCount; ctr++)
    {

        Column cl;
        memset((void*)&cl, 0, sizeof(Column));

        std::string clname = PQfname(currentStatement.rowset, ctr);
        Oid cltype = PQftype(currentStatement.rowset, ctr);
        int sz = PQfsize(currentStatement.rowset, ctr);
        int scl = PQfmod(currentStatement.rowset, ctr);
        VariantType ltype = translateToLocalType(cltype);

        cl.setPosition(ctr);
        cl.setName(clname);
        cl.setNativeType(cltype);
        cl.setLength(sz);
        cl.setPrecision(sz);
        cl.setScale(scl);
        cl.setType(ltype);

        if(cltype == 1043 && sz == -1)
        {
            cl.setLength(scl);
        }

        if(cltype == 1700)
        {
            cl.setLength(-1);
            cl.setPrecision(-1);
            cl.setScale(-1);
        }

        memcpy(&currentStatement.columnList[ctr],&cl,sizeof(Column));
    }

    return true;
}

VariantType Database::translateToLocalType(unsigned int dbtype)
{
    switch(dbtype)
    {
        case 18:
        {
            return Char;
        }
        case 1043:
        {
            return AsciiString;
        }
        case 1700:
        case 23:
        case 20:
        {
            return Number;
        }
        case 701:
        {
            return Decimal;
        }
        case 1114:
        {
            return TimeStamp;
        }
        case 16:
        {
            return Boolean;
        }
        default:
        {
            return Raw;
        }
    }
}

void Database::translatePGTStoCTS(const char* ptr, struct tm &tsstruct)
{
    typedef struct PGSQLNoTzDateTime
    {
        char dd[2];
        char dsep1;
        char MM[2];
        char dsep2;
        char yyyy[4];
        char spc1;
        char hh[2];
        char tsep11;
        char mm[2];
        char tsep21;
        char ss[2];
    }PGSQLNoTzDateTime;

    PGSQLNoTzDateTime pgts;

    memset((void*)&pgts,0,sizeof(pgts));

    memcpy(&pgts, ptr, sizeof(pgts));

    tsstruct.tm_year = atoi(pgts.yyyy);
    tsstruct.tm_mon = atoi(pgts.MM);
    tsstruct.tm_mday = atoi(pgts.dd);

    tsstruct.tm_hour = atoi(pgts.hh);
    tsstruct.tm_min = atoi(pgts.mm);
    tsstruct.tm_sec = atoi(pgts.ss);

}

bool Database::prepareBind(const char* sqlstring, std::string &errmsg, int &errorCode)
{
	return false;
}

bool Database::bindColumn(BindStruct &bstruct)
{
	return false;
}

bool Database::copyValue(Variant &val, int column, int row)
{
	return false;
}

bool Database::executeBulkDML(std::string &errmsg, int &errorCode)
{
	return false;
}

void Database::setBulkArraySize(long sz)
{
	_arraysize = sz;
}

std::string Database::getDatabaseTimeStamp(const std::string &timestamp, const std::string &format)
{
    DateTime tsval;
    DateTime::fromString(timestamp,format, tsval);

    return getNativeTimeStamp(tsval);
}
