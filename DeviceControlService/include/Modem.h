//
// Created by mukeshp on 5/9/2020.
//

#ifndef DEVELOPEMENT_MODEM_H
#define DEVELOPEMENT_MODEM_H

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include "SerialInterface.h"

#define MAX_SUPPORTED_MODEM     (4)

#define MODEM_TTY_DEVICE "/dev/ttyACM0"

#define PPP_PATH   "/etc/ppp/peers/"
#define DELAYMS (1000)
#define FILEPATH    (255)
#define COMMANDSIZE (64)
#define RESPONSESIZE (128)
#define MAX_APNNAME     32
#define MAX_MODELNAME   32
#define MAX_FILEAME     64
#define MAX_MODELID     32
#define MODEM_RETRY_COUNTER (3)

#define MODEM_WAIT_INTERVAL (3)
#define MODEM_NEXT_INTERVAL (1)

#define MODEM_ON_OFF_GPIO_STR   "gpio101"
#define MODEM_ON_OFF_GPIO       101

#define MODEM_RESET_GPIO_STR   "gpio146"
#define MODEM_RESET_GPIO       146

#define MODEM_4V_ENABLE_GPIO_STR   "gpio147"
#define MODEM_4V_ENABLE_GPIO       147

#define MODEM_STATUS_GPIO_STR   "gpio159"
#define MODEM_STATUS_GPIO       159

#define MODEM_RTS_GPIO_STR   "gpio151"
#define MODEM_RTS_GPIO       151

#define MODEM_DTR_GPIO_STR   "gpio150"
#define MODEM_DTR_GPIO       150

#define MODEM_TYPE_4G       (0)
#define MODEM_TYPE_2G       (1)

enum
{
    MODEM_PIN_CNFG = 0,
    MODEM_POWER_ON,
    MODEM_TTY_DETECT,
    MODEM_DETECT_MODEM_ID,
    MODEM_INIT,
    MODEM_SET_APN,
    MODEM_ACTIVATE_PPP,
    MODEM_CHECK_PPP_STATUS,
    MODEM_POWER_OFF,
    MODEM_WAIT_FOR_STATE,
    MODEM_TROUBLESHOOT,
    MODEM_READ_CONFIG,
    MODEM_IDLE,
    MODEM_MAX
};

typedef struct modempara
{
    char mEnable;
    char mSerialPort[256];
    int  mSerialBaudRate;
    int  mSignal;
    int  mRegistration;
    char mSimstatus[32];
    char mModelno[32];
    char mRevision[32];
    char mManufacturedid[32];
    char mApnName[32];
    char mImei[32];
    char mStatus;

    modempara()
    {
        this->mEnable = 1;
        strcpy(this->mSerialPort, "/dev/ttyACM0");
        this->mSerialBaudRate = B_115200;
        strcpy(this->mApnName, "airtelgprs.com");
    }
}ModemPara_t;

typedef struct modemCnfg
{
    char mModemName[MAX_MODELNAME];
    char mModelId[MAX_MODELID];
    char mFileName[MAX_FILEAME];
    int mModemType;

    //char mModemIndex;
    //23.0698353, 72.5187065
    //float mLatitude;
    //float mLongitude;

    modemCnfg()
    {
        strcpy(this->mModemName, "NL-SW-LTE-TC4EU");
        strcpy(this->mModelId, "LE910-EU V2");
        strcpy(this->mFileName, "NL-SW-LTE-TC4EU");
        this->mModemType = MODEM_TYPE_4G;
    }

}ModemCnfg_t;


void* ModemHandleThread(void* arg);

int GetModemIdTemp(int fd);
int ModemDetectModelID(void);
int ModemNameselect(char *pModelId, char *pModemName);
void ModemGetId(char* pModemId);

void ModemIOPinsConfigure(void);

void ModemPowerOn(void);
void ModemPowerOff(void);
int ModemInitialization(ModemPara_t *pModemPara);
int ModemSetApnNameToChatScript(ModemCnfg_t *pModemCnfg);
int ModemActivatePPPInterface(ModemCnfg_t *pModemCnfg);
int ModemCheckPPPInterfaceStatus(void);
int ModemReadConfigurationfromDB(ModemCnfg_t *pModemCnfg);
int ModemFindTroubleShootParameters(ModemCnfg_t *pModemCnfg);
int ModemTtyInterfaceDetection(void);

int ModemReqResponse(const char *pRequest, char* pResponse, int RespLen, int iDelayus);
int ModemReqResponse2G(const char *pRequest, char* pResponse, int RespLen, int iDelayus);
int GetSet_Apn_Name(const char *pFilePath, const char *oldapnname, const char *pNewApnName);
char ReplaceAPNName(const char *str, const char *keystr, char *apnstr);
int SeperateStringAndFind(char *string, const char *headstr, const char *filterstr, char counterfiter, char *resultstr);

#endif //DEVELOPEMENT_MODEM_H
