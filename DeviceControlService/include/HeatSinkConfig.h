//
// Created by mukeshp on 04-Aug-20.
//

#ifndef HEAT_SINK_CONFIG
#define HEAT_SINK_CONFIG

#include <errno.h>

#define HEAT_SINK_CONFIG_FILE_PATH     "../etc/"
#define HEAT_SINK_CONFIG_FILE         "HeatSinkConfig.ini"

typedef struct heatsinkCnfg
{
    int mCpuTempScanInterval; /* In seconds */
    int mThresoldTemperature; /* Temp(in celcius) * 1000 */

    heatsinkCnfg()
    {
        this->mCpuTempScanInterval = 5;
        this->mThresoldTemperature = 50000;
    }

}HeatsinkCnfg_t;

int Heatsink_ConfigurationRead(void);


#endif //IOSERVICE_HEATSINKCNFG_H

