//
// Created by mukeshp on 5/29/2020.
//

#ifndef DNS_RESOLVER
#define DNS_RESOLVER

#include <linux/route.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/inotify.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#define DNS_RESOLVE_FILE           "/etc/resolv.conf"
#define PPP_DNS_RESOLVE_FILE       "/etc/ppp/resolv.conf"
#define ETH0_DHCP_LEASE_FILE       "/var/lib/dhcp/dhclient.eth0.leases"
#define WLAN0_DHCP_LEASE_FILE      "/var/lib/dhcp/dhclient.wlan0.leases"

#define DNS_FILE_WRITE_RETRY_CNT   (3)

#define EVENT_SIZE        ( sizeof (struct inotify_event) )
#define EVENT_BUF_LEN     ( 256 * ( EVENT_SIZE + 16 ) )



void *DnsResolveNotifyThread(void *arg);
void ResolveUpdate(int iUpdateType);
void UpdateDns(int index);
void UpdateDnsForPpp0(void);
void UpdateDnsForEth0(void);
void UpdateDnsForWlan0(void);
int GetDNSAddrFromDHCPLeaseFile(const char *file, char *predns, char *altdns);
int GetDnsAddrFromPppResolvFile(const char *pFile, char *pPredns, char *pAltdns);
int WriteResolvFile(const char *PreDNS, const char *AltDNS,const char *ifname);

void UpdateRoute(int index);
void ReloadRouteForPpp0(short metric);
void ReloadRouteForEth0(short metric);
void ReloadRouteForWlan0(short metric);
int GetRouteEth0(char *route);
int GetRouteWlan0(char *route);
int GetRouteAddrFromDHCPLeaseFile(const char *file, char *gw_addr);
bool SetDefaultGateway(char *ifname, const char *defGateway, short metric);
int GetInterfaceFlags(char *ifname, short *ifrflag);

#endif //DEVELOPEMENT_DNS_RESOLVE_H
