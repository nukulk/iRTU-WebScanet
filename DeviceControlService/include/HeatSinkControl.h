//
// Created by mukeshp on 04-Aug-20.
//

#ifndef HEAT_SINK_CONTROL
#define HEAT_SINK_CONTROL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#define CPU_FAN_PWCN_GPIO           112
#define CPU_FAN_PWCN_GPIO_STR       "gpio112"

void* HeatSinkCntl_Thread(void* arg);

int CpuFan_GPIO_Init(void);
int CpuFan_GPIO_Deinit(void);
int CpuFan_On(void);
int CpuFan_Off(void);

int CpuTemperature_Read(int *pTemperature);

#endif //IOSERVICE_HEATSINKCNTL_H
