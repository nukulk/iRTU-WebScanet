//
// Created by mukeshp on 14-Aug-20.
//

#ifndef WIFI_INTERFACE
#define WIFI_INTERFACE

#include <string.h>

typedef struct wifiSetting
{
    int mEnable;
    char mSsid[256];
    char mPassword[256];

    wifiSetting()
    {
        this->mEnable = 0;
        strcpy(this->mSsid,"Cimcon 6th Floor Conf.");
        strcpy(this->mPassword, "csipl@123");
    }
}wifiSetting_t;

void* Wifi_Connectivity_Thread(void *arg);
int Wifi_setting(void);
int Wifi_Connect(void);
int Wifi_DisConnect(void);

#endif //IOSERVICE_WIFIINTERFACE_H
