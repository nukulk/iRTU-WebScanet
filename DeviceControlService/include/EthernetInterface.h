//
// Created by mukeshp on 20-Aug-20.
//

#ifndef ETHERNET_INTERFACE
#define ETHERNET_INTERFACE

#include <string.h>

typedef struct ethernetCnfg
{
    char mEnable;
    /* DHCP = 1, Static = 0 */
    char mIsEth0DhcpOrStatic;
    char mEth0IpAddr[20];
    char mEth0NetMask[20];
    char mEth0Gateway[24];
    char mEth0PreferDns[24];
    char mEth0AlterDns[24];

    ethernetCnfg()
    {
        this->mEnable = 1;
        this->mIsEth0DhcpOrStatic = 0;
        strcpy(this->mEth0IpAddr, "199.199.50.113");
        strcpy(this->mEth0NetMask, "255.255.254.0");
        strcpy(this->mEth0Gateway, "199.199.50.3");
        strcpy(this->mEth0PreferDns, "8.8.8.8");
        strcpy(this->mEth0AlterDns, "8.8.4.4");
    }
}EthernetCnfg_t;

int Ethernet_setting(void);
void* Ethernet_Connectivity_Thread(void *arg);
int Ethernet_Connect(void);
int Ethernet_DisConnect(void);

#endif //IOSERVICE_ETHERNETINTERFACE_H
