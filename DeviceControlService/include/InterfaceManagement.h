//
// Created by mukeshp on 5/29/2020.
//

#ifndef INTERFACE_MANAGEMENT
#define INTERFACE_MANAGEMENT

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <sys/types.h>
#include <ifaddrs.h>

#define IFLIST_REPLY_BUFFER         (8192)
#define MAX_INTERFACE               (3)

typedef enum
{
    UPDATE_DNS_ONLY = 1,
    UPDATE_ROUTE_ONLY = 2,
    UPDATE_DNS_ROUTE = 3,
}UpdateType_e;

typedef enum
{
    INTERFACE_NOT_PRESENT = 0,
    INTERFACE_PRESENT,
    INTERFACE_UP_ONLY,
    INTERFACE_UP_RUNNING,
}InterfaceStatus_e;

typedef struct
{
    char mIfname[16];
    InterfaceStatus_e mIfStatus;
    unsigned char mInfPriority;
    bool mIsInternet;
    bool mIsActive;
}InterfaceInfo_t;

#if 0
typedef struct NetworkCnfg
{
    /* DHCP = 1, Static = 0 */
    char mIsEth0DhcpOrStatic;
    char mEth0IpAddr[20];
    char mEth0NetMask[20];
    char mEth0Gateway[24];
    char mEth0PreferDns[24];
    char mEth0AlterDns[24];

    NetworkCnfg()
    {
        this->mIsEth0DhcpOrStatic = 0;
        strcpy(this->mEth0IpAddr, "199.199.50.113");
        strcpy(this->mEth0NetMask, "255.255.254.0");
        strcpy(this->mEth0Gateway, "199.199.50.3");
        strcpy(this->mEth0PreferDns, "8.8.8.8");
        strcpy(this->mEth0AlterDns, "8.8.4.4");
    }
}NetworkCnfg_t;
#endif

void* InterfacePriorityMgmtThread(void* arg);

char IsInternetAvailable(void);
void SelectAndSwitchInternet(UpdateType_e iUpdateType);
void SetInterfaceInfo(void);
int CreateNetlinkSocket(void);
int PollNetlinkEvent(int nl_socket);
int ParseNetlinkEvent(int nlsock);
void RouteEventProcess(const struct nlmsghdr *nlh);
void LinkEventProcess(const struct nlmsghdr *nlh);
void ParseRtattr(struct rtattr *rtattrarray[], int maxrtarraysize, struct rtattr *rtattval, int len);
bool DelDefaultGateway(char *ifname);
int Is_Interface_Present(char * ifname);
int Get_Interface_Status(const char * ifname);

#endif //DEVELOPEMENT_INTERFACE_H
