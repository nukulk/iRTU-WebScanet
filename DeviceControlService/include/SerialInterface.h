//
// Created by mukeshp on 5/8/2020.
//

#ifndef SERIAL_INTERFACE
#define SERIAL_INTERFACE

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/select.h>

#define MAX_RETRY   (3)

enum BaudRate {
    B_9600,
    B_38400,
    B_57600,
    B_115200,
};

int SerialPortOpen(const char *pDeviceName, int iBaudRate);
int SerialPortConfigureTermios(int iFileDesc, int iBaudRate, int iTimeOut_ms);
int SerialPortWriteRead(int iFileDesc, const char *pWrData, int iWrdataLen, char *pRdData, int iRdDataLen);
int SerialPortWrite(int iFileDesc, const char* data, int iDataLen);
int SerialPortRead(int iFileDesc, char* data, int iDataLen);
int SerialPortClose(int iFileDesc);

#endif //DEVELOPEMENT_SERIAL_INTERFACE_H
