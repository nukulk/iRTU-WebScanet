//
// Created by mukeshp on 13-Aug-20.
//

#ifndef DEVICE_CONTROL_CONFIG
#define DEVICE_CONTROL_CONFIG

//#include "Config.h"

#define DEVICE_CONTROL_CONFIG_FILE_PATH   "../etc/"
#define DEVICE_CONTROL_CONFIG_FILE       "DeviceControlConfig.ini"

#define GET_BAUDRATE_STRING(_arg_)  (((_arg_) == B_9600) ? "9600" : \
                                    ((_arg_) == B_38400) ? "38400" : \
                                    ((_arg_) == B_57600) ? "57600" : \
                                    ((_arg_) == B_115200) ? "115200" : "In Valid")

int IoService_ConfigurationRead(void);

#endif //IOSERVICE_CNFG_H
