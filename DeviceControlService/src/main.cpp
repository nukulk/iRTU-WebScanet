#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>

#include "Modem.h"
#include "DNSResolver.h"
#include "InterfaceManagement.h"
#include "HeatSinkControl.h"
#include "SerialInterface.h"
#include "DeviceControlConfig.h"
#include "WiFiInterface.h"
#include "EthernetInterface.h"

extern wifiSetting_t gWifiSetting;
extern EthernetCnfg_t gEthernetCnfg;
extern ModemPara_t gModemPara;

static pthread_t gModemIntefaceThread;
static pthread_t gDnsResolveMonitoringThread;
static pthread_t gInterfacePriorotyThread;
static pthread_t gHeatSinkThread;
static pthread_t gWifiThread;
static pthread_t gEthernetThread;

void SignalTrap(int signum)
{
    ModemPowerOff();
    CpuFan_Off();
    CpuFan_GPIO_Deinit();
    exit(0);
}

int main(int argc, char**argv)
{
    signal(SIGINT, SignalTrap);
    signal(SIGQUIT, SignalTrap);

    system("mkdir -p /var/log/rtu/modem");
    system("touch /var/log/rtu/modem/ppp-logfile.log");

    if(0 != IoService_ConfigurationRead())
    {
        printf("Error in IoService_ConfigurationRead at %s\n", __func__ );
    }

    if(gEthernetCnfg.mEnable == 1)
    {
        pthread_create(&gEthernetThread, NULL, Ethernet_Connectivity_Thread, NULL);
    }
    else
    {
        Ethernet_DisConnect();
    }

    if(gWifiSetting.mEnable == 1)
    {
        pthread_create(&gWifiThread, NULL, Wifi_Connectivity_Thread, NULL);
    }
    else
    {
        Wifi_DisConnect();
    }

    if(gModemPara.mEnable == 1)
    {
        pthread_create(&gModemIntefaceThread, NULL, ModemHandleThread, NULL);
    }

    sleep(10);
    pthread_create(&gDnsResolveMonitoringThread, NULL, DnsResolveNotifyThread, NULL);
    pthread_create(&gInterfacePriorotyThread, NULL, InterfacePriorityMgmtThread, NULL);
    //pthread_create(&gHeatSinkThread, NULL, HeatSinkCntl_Thread, NULL);

    while (1);
}
