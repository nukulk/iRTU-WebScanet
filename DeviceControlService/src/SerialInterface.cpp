/*
 * File:   SerialReadModem.c
 * Author: Mukesh Prajapati
 *
 * Created on May 08, 2020, 06:59 PM
 */

#include "SerialInterface.h"

int SerialPortOpen(const char *pDeviceName, int iBaudRate)
{
    int tFileDesc = -1;

    if((NULL == pDeviceName) || (iBaudRate < B_9600) || (iBaudRate > B_115200))
    {
        printf("Error pDeviceName-null or invalid iBaudRate(%d) at %s\n", iBaudRate, __func__ );
        return -1;
    }

    tFileDesc = open(pDeviceName, O_RDWR | O_NOCTTY | O_SYNC);
    if (tFileDesc <= -1)
    {
        printf("Could not open device %s. Is the device name correct and do you have read/write permission?\n", pDeviceName);
    }
    else
    {
        printf("Open port \"%s\" successfully\n", pDeviceName);
    }

    SerialPortConfigureTermios(tFileDesc, iBaudRate, 1000);

    return (tFileDesc);
}

int SerialPortConfigureTermios(int iFileDesc, int iBaudRate, int iTimeOut_ms)
{
    struct termios tty;

    if(iFileDesc <= -1)
    {
        printf("Error at iFileDesc(%d) at %s\n", __func__ );
        return -1;
    }

    /**
     * Get the current options for the port...
     * */
    if (tcgetattr(iFileDesc, &tty) != 0)
    {
        // Error occurred
        printf("Could not get terminal attributes for \"%s\" - %s\n", strerror(errno));
        return -1;
    }

    /**
     * Setting the character size
     */
    tty.c_cflag &= ~CSIZE;              // CSIZE is a mask for the number of bits per character
    tty.c_cflag |= CS8;                 // Set to 8 bits per character

    /**
     * Parity Bit Setting
     */
    tty.c_cflag &= ~(PARENB | PARODD);  // shut off parity, No parity bit is added to the output characters
    tty.c_cflag &= ~CSTOPB;             // Only one stop-bit is used

    /**
     * Hardware Flow control
     */
    tty.c_cflag &= ~CRTSCTS;            // Disable hadrware flow control (RTS/CTS)

    /**
     * Enable the receiver and set local mode...
     * */
    tty.c_cflag |= (CLOCAL | CREAD);    // ignore modem controls, enable reading

    /*
     * Set the baud rates...
     * */
    switch(iBaudRate)
    {
        case B_9600:
            cfsetispeed(&tty, B9600);
            cfsetospeed(&tty, B9600);
            break;
        case B_38400:
            cfsetispeed(&tty, B38400);
            cfsetospeed(&tty, B38400);
            break;
        case B_57600:
            cfsetispeed(&tty, B57600);
            cfsetospeed(&tty, B57600);
            break;
        case B_115200:
            cfsetispeed(&tty, B115200);
            cfsetospeed(&tty, B115200);
            break;
        default:
            printf("baudRate passed to function %s unrecognized\n", __func__);
            break;
    }


    tty.c_oflag     =   0;              // No remapping, no delays

    //================= CONTROL CHARACTERS (.c_cc[]) ==================//

    // c_cc[VTIME] sets the inter-character timer, in units of 0.1s.
    // Only meaningful when port is set to non-canonical mode
    // VMIN = 0, VTIME = 0: No blocking, return immediately with what is available
    // VMIN > 0, VTIME = 0: read() waits for VMIN bytes, could block indefinitely
    // VMIN = 0, VTIME > 0: Block until any amount of data is available, OR timeout occurs
    // VMIN > 0, VTIME > 0: Block until either VMIN characters have been received, or VTIME
    //                      after first character has elapsed
    // c_cc[WMIN] sets the number of characters to block (wait) for when read() is called.
    // Set to 0 if you don't want read to block. Only meaningful when port set to non-canonical mode

    if (iTimeOut_ms == -1)
    {
        // Always wait for at least one byte, this could
        // block indefinitely
        tty.c_cc[VTIME] = 0;
        tty.c_cc[VMIN] = 1;
    }
    else if (iTimeOut_ms == 0)
    {
        // Setting both to 0 will give a non-blocking read
        tty.c_cc[VTIME] = 0;
        tty.c_cc[VMIN] = 0;
    }
    else if (iTimeOut_ms > 0)
    {
        tty.c_cc[VTIME] = (cc_t)(iTimeOut_ms/100);    // 0.5 seconds read timeout
        tty.c_cc[VMIN] = 0;
    }

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);       // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);

    tty.c_lflag = 0;                // no signaling chars, no echo, no canonical processing
    // Canonical input is when read waits for EOL or EOF characters before returning. In non-canonical mode, the rate at which
    // read() returns is instead controlled by c_cc[VMIN] and c_cc[VTIME]
    tty.c_lflag &= ~ICANON;         // Turn off canonical input, which is suitable for pass-through
    tty.c_lflag &= ~ECHOE;          // Turn off echo erase (echo erase only relevant if canonical input is active)
    tty.c_lflag &= ~ECHONL;
    tty.c_lflag &= ~ISIG;           // Disables recognition of INTR (interrupt), QUIT and SUSP (suspend) characters

    /*
     * * Set the new options for the port...
     * */
    /* TCSANOW : Make changes now without waiting for data to complete */
    tcflush(iFileDesc, TCIFLUSH);

    if(tcsetattr(iFileDesc, TCSANOW, &tty) != 0)
    {
        // Error occurred
        printf("Could not apply terminal attributes for \"%s\" - %s\n", strerror(errno));
        return -1;
    }

    // Successful!
    return 0;
}

int SerialPortWriteRead(int iFileDesc, const char *pWrData, int iWrdataLen, char *pRdData, int iRdDataLen)
{
    int tWrittenData = 0;
    int tReadBytes = 0;
    int tExit = 0;
    char buffer[256]  = {0, };

    if (iFileDesc < 0)
    {
        printf("%s called but file descriptor < 0, indicating file has not been opened\n", __func__);
        return -1;
    }

    errno = 0;
    for(int tRetry = 0; tRetry < MAX_RETRY ; ++tRetry)
    {
        tWrittenData = write(iFileDesc, pWrData, iWrdataLen);
        if (tWrittenData != iWrdataLen)
        {
            printf("errno(%d) (%s)\n", errno, strerror(errno));
            printf("write failed (written bytes:%d) at %s\n", iWrdataLen, __func__);
            continue;
        }
        else
        {
            break;
        }
    }

    memset(buffer, 0, sizeof(buffer));
    tReadBytes = 0;
    char *bufptr = buffer;
    for(int tRetry = 0; tRetry < MAX_RETRY ; ++tRetry)
    {
        //char *bufptr = NULL;
        int  nbytes = 0;

        errno = 0;
        tExit = 0;


        while ((nbytes = read(iFileDesc, bufptr, buffer + sizeof(buffer) - bufptr - 1)) > 0)
        {
            bufptr += nbytes;
            tReadBytes += nbytes;
            //if (bufptr[-1] == '\n' || bufptr[-1] == '\r')
            if(strcasestr(bufptr, "OK\r\n"))
            {
                tExit = 1;
                break;
            }
        }

        printf("Retry:%d, tReadBytes(%d)\n", tRetry, tReadBytes);
        if((tExit == 1) && (tReadBytes > 2))
        {
            break;
        }
    }

    strcpy(pRdData, buffer);

    return (tReadBytes);
}

int SerialPortWrite(int iFileDesc, const char* data, int iDataLen)
{
    int tWriteResult = 0;
    //extern int errno;

    if (iFileDesc < 0)
    {
        //DebugLTEPrintLog(ERROR_LOG, "%s called but file descriptor < 0, indicating file has not been opened", __func__);
        printf("%s called but file descriptor < 0, indicating file has not been opened\n", __func__);
        return -1;
    }

    tWriteResult = write(iFileDesc, data, iDataLen);
    if (tWriteResult  !=  iDataLen)
    {
        printf("errno(%d) (%s)\n", errno, strerror(errno));
        printf("write failed (written bytes:%d) at %s\n", tWriteResult, __func__);
        return -1;
    }

    return tWriteResult;
}

int SerialPortRead(int iFileDesc, char* data, int iDataLen)
{
    ssize_t n;
    char buffer[256]  = {0, };
    char *bufptr;
    int  nbytes;

    if (iFileDesc < 0)
    {
        //DebugLTEPrintLog(ERROR_LOG, "%s() was called but file descriptor (fileDesc) was < 0, indicating file has not been opened", __func__);
        printf("Error at iFileDesc < 0 at %s\n", __func__ );
        return -1;
    }

#if 0
    fd_set fdSet;

    FD_ZERO(&fdSet);
    FD_SET(iFileDesc, &fdSet);

    select(iFileDesc+1, &fdSet, NULL, NULL, NULL);

    if(FD_ISSET(iFileDesc, &fdSet))
    {
        FD_CLR(iFileDesc, &fdSet);
    }
#endif

    // Read from file
    // We provide the underlying raw array from the readBuffer_ vector to this C api.
    // This will work because we do not delete/resize the vector while this method
    // is called
    bufptr = buffer;
    while ((nbytes = read(iFileDesc, bufptr, buffer + sizeof(buffer) - bufptr - 1)) > 0)
    {
        bufptr += nbytes;
        if (bufptr[-1] == '\n' || bufptr[-1] == '\r')
            break;
    }

#if 0
    errno = 0;
    n = read(iFileDesc, data, iDataLen);
    printf("read:(%s)\n", strerror(errno));
    // Error Handling
    if (n < 0)
    {
        // Read was unsuccessful
        //DebugLTEPrintLog(ERROR_LOG, "read get failed");
        printf("Error at read failed at %s\n", __func__ );
        return -1;
    }
    else if (n > 0)
    {
        //DebugLTEPrintLog(ALL_LOG, "Modem read successfull with %d bytes. string: %s", n, data);
        printf("Read success read %d bytes at %s\n", n, __func__ );
    }
    else if (n == 0)
    {
        printf("Read %d bytes at %s\n", n, __func__ );
    }
#endif

    strcpy(data, buffer);

    // If code reaches here, read must of been successful
    //return n;
    return nbytes;
}

int SerialPortClose(int iFileDesc)
{
    int tRetVal;

    if(iFileDesc <= -1)
    {
        return -1;
    }

    tRetVal = close(iFileDesc);
    if(tRetVal != 0)
    {
        // Error occurred
        //DebugLTEPrintLog(ERROR_LOG, "Tried to close serial port %s, but close() failed", device_);
        return -1;
    }
    //state_ = CLOSED;
    return 0;
}



#if 0
void serialstring(char *string)
{
    for(int i=0;i<strlen(string);i++)
    {	if(string[i]==0x0A || string[i]==0x0D)
            string[i]=' ';
    }
}
#endif



