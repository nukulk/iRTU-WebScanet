//
// Created by mukeshp on 5/9/2020.
//

#include "Modem.h"
#include "SerialInterface.h"
#include "InterfaceManagement.h"

ModemCnfg_t gModemCnfg[MAX_SUPPORTED_MODEM];
ModemPara_t gModemPara;
int gModemIndex = 0;

static time_t gsWaitTimeStamp = 0;
static int gsWaitTimeInterval = 0;
static unsigned int gsModemState = MODEM_PIN_CNFG;
static unsigned int gsModemNextState = MODEM_PIN_CNFG;
static int gsModemStatusFailed = 0;

static int ModemWaitIntervalCheck(void);

void* ModemHandleThread(void* arg)
{
    static int tsModemPPPInterfaceCounter = 0;

    pthread_detach(pthread_self());

    while (1)
    {
        switch (gsModemState)
        {
            case MODEM_PIN_CNFG:
            {
                printf("STATE: MODEM_PIN_CNFG\n");
                ModemIOPinsConfigure();
                gsModemState = MODEM_POWER_ON;
                break;
            }
            case MODEM_POWER_ON:
            {
                printf("STATE: MODEM_POWER_ON\n");
                ModemPowerOn();
                gsWaitTimeInterval = 30;
                gsModemState = MODEM_WAIT_FOR_STATE;
                gsModemNextState = MODEM_TTY_DETECT;
                time(&gsWaitTimeStamp);
                break;
            }
            case MODEM_TTY_DETECT:
            {
                printf("STATE: MODEM_TTY_DETECT\n");
                if (0 == ModemTtyInterfaceDetection())
                {
                    gsModemState = MODEM_DETECT_MODEM_ID;
                }
                else
                {
                    gsModemState = MODEM_IDLE;
                }
                break;
            }
            case MODEM_DETECT_MODEM_ID:
            {
                printf("STATE: MODEM_DETECT_MODEM_ID\n");
                if (0 == ModemDetectModelID())
                {
                    gsModemState = MODEM_INIT;
                }
                else
                {
                    gsModemState = MODEM_IDLE;
                }
                break;
            }
            case MODEM_INIT:
            {
                printf("STATE : MODEM_INIT\n");

                if (0 == ModemInitialization(&gModemPara))
                {
                    gsModemState = MODEM_SET_APN;
                }
                else
                {
                    gsModemStatusFailed++;
                    gsWaitTimeInterval = MODEM_WAIT_INTERVAL * (gsModemStatusFailed * 20);
                    gsModemState = MODEM_WAIT_FOR_STATE;

                    if (gsModemStatusFailed > MODEM_RETRY_COUNTER)  //if modem does not getting proper status ,again power on/off
                    {
                        gsModemNextState = MODEM_TTY_DETECT;
                        gsModemStatusFailed = 0;
                    }
                    else
                    {
                        gsModemNextState = MODEM_INIT;
                    }
                    time(&gsWaitTimeStamp);
                }
                break;
            }
            case MODEM_SET_APN:
            {
                printf("STATE: MODEM_SET_APN\n");
                if (0 == ModemSetApnNameToChatScript(&gModemCnfg[gModemIndex]))
                {
                    gsModemState = MODEM_ACTIVATE_PPP;
                }
                else
                {
                    gsModemNextState = MODEM_TTY_DETECT;
                }
                break;
            }
            case MODEM_ACTIVATE_PPP:
            {
                printf("STATE: MODEM_ACTIVATE_PPP\n");

                /* After configuration parameters set, activate PPP0 interface */
                if (0 == ModemActivatePPPInterface(&gModemCnfg[gModemIndex]))//success
                {
                    gsWaitTimeInterval = 60;
                    gsModemNextState = MODEM_CHECK_PPP_STATUS;
                    gsModemState = MODEM_WAIT_FOR_STATE;
                }
                else
                {
                    gsWaitTimeInterval = 60;
                    gsModemState = MODEM_INIT;
                }
                time(&gsWaitTimeStamp);
                break;
            }
            case MODEM_CHECK_PPP_STATUS:
            {
                /* After activating ppp0 interface, get the status of ppp0 interface */

                printf("STATE: MODEM_CHECK_PPP_STATUS\n");

                if (0 == ModemCheckPPPInterfaceStatus())
                {
                    gsWaitTimeInterval = 60;
                    gsModemState = MODEM_WAIT_FOR_STATE;

                    tsModemPPPInterfaceCounter = 0;

                    //sprintf(LteEventString,"{\"ppp\":\"connected\"}");
                    //PublisheEvent(lteSocket,LteEventString, strlen(LteEventString));
                }
                else
                {
                    ++tsModemPPPInterfaceCounter;
                    if (tsModemPPPInterfaceCounter > MODEM_RETRY_COUNTER)
                    {
                        //sprintf(LteEventString,"{\"ppp\":\"disconnected\"}");
                        //PublisheEvent(lteSocket,LteEventString, strlen(LteEventString));
                        tsModemPPPInterfaceCounter = 0;
                        gsModemState = MODEM_TROUBLESHOOT;
                    }
                    else
                    {
                        gsModemState = MODEM_ACTIVATE_PPP;
                    }
                }

                time(&gsWaitTimeStamp);
                break;
            }
            case MODEM_POWER_OFF:
            {
                printf("STATE: MODEM_POWER_OFF\n");
                ModemPowerOff();
                gsWaitTimeInterval = 60;
                gsModemState = MODEM_WAIT_FOR_STATE;
                gsModemNextState = MODEM_TTY_DETECT;
                time(&gsWaitTimeStamp);
                break;
            }
            case MODEM_IDLE:
            {
                printf("STATE: MODEM_IDLE\n");
                gsModemState = MODEM_WAIT_FOR_STATE;
                gsModemNextState = MODEM_TTY_DETECT;
                gsWaitTimeInterval = 30;
                time(&gsWaitTimeStamp);
                break;
            }
            case MODEM_WAIT_FOR_STATE:
            {
                //printf("MODEM_WAIT_FOR_STATE\n");
                if (0 == ModemWaitIntervalCheck())
                {
                    gsModemState = gsModemNextState;
                }
                break;
            }
            case MODEM_TROUBLESHOOT:
            {
                if (0 == ModemFindTroubleShootParameters(NULL))
                {
                    gsModemState = MODEM_WAIT_FOR_STATE;
                    gsModemNextState = MODEM_TTY_DETECT;
                    gsWaitTimeInterval = 30;
                    time(&gsWaitTimeStamp);
                }
                else
                {
                    gsModemState = MODEM_TTY_DETECT;
                }
                break;
            }
            case MODEM_READ_CONFIG:
            {
                /* Fully not implemented, will see once receive configuration */
                ModemReadConfigurationfromDB(NULL);
                break;
            }
            default:
            {
                break;
            }
        }
        /* 1 Second delay for cpu time optimization */
        sleep(1);
    }

    pthread_exit(0);
}

void ModemIOPinsConfigure(void)
{
    int tFd = -1;
    char tempString[256] = {0, };

    /* Modem ON-OFF GPIO */
    sprintf(tempString, "/sys/class/gpio/%s/direction", MODEM_ON_OFF_GPIO_STR);
    tFd = open(tempString, O_RDWR);
    if (tFd < 0)
    {
        //if not exported already than export it
        sprintf(tempString, "echo \"%d\" > /sys/class/gpio/export", MODEM_ON_OFF_GPIO);
        system(tempString);
    }
    close(tFd);

    /* Modem RESET */
    sprintf(tempString, "/sys/class/gpio/%s/direction", MODEM_RESET_GPIO_STR);
    tFd = open(tempString, O_RDWR);
    if (tFd < 0)
    {
        //if not exported already than export it
        sprintf(tempString, "echo \"%d\" > /sys/class/gpio/export", MODEM_RESET_GPIO);
        system(tempString);
    }
    close(tFd);

    /* +4V enable */
    sprintf(tempString, "/sys/class/gpio/%s/direction", MODEM_4V_ENABLE_GPIO_STR);
    tFd = open(tempString, O_RDWR);
    if (tFd < 0)
    {
        //if not exported already than export it
        sprintf(tempString, "echo \"%d\" > /sys/class/gpio/export", MODEM_4V_ENABLE_GPIO);
        system(tempString);
    }
    close(tFd);

    /* Modem STATUS GPIO */
    sprintf(tempString, "/sys/class/gpio/%s/direction", MODEM_STATUS_GPIO_STR);
    tFd = open(tempString, O_RDWR);
    if (tFd < 0)
    {
        //if not exported already than export it
        sprintf(tempString, "echo \"%d\" > /sys/class/gpio/export", MODEM_STATUS_GPIO);
        system(tempString);
    }
    close(tFd);

    /* Modem RTS GPIO */
    sprintf(tempString, "/sys/class/gpio/%s/direction", MODEM_RTS_GPIO_STR);
    tFd = open(tempString, O_RDWR);
    if (tFd < 0)
    {
        //if not exported already than export it
        sprintf(tempString, "echo \"%d\" > /sys/class/gpio/export", MODEM_RTS_GPIO);
        system(tempString);
    }
    close(tFd);

    /* Modem DTR GPIO */
    sprintf(tempString, "/sys/class/gpio/%s/direction", MODEM_DTR_GPIO_STR);
    tFd = open(tempString, O_RDWR);
    if (tFd < 0)
    {
        //if not exported already than export it
        sprintf(tempString, "echo \"%d\" > /sys/class/gpio/export", MODEM_DTR_GPIO);
        system(tempString);
    }
    close(tFd);

    sprintf(tempString, "echo \"out\" > /sys/class/gpio/%s/direction", MODEM_ON_OFF_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"out\" > /sys/class/gpio/%s/direction", MODEM_RESET_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"out\" > /sys/class/gpio/%s/direction", MODEM_4V_ENABLE_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"out\" > /sys/class/gpio/%s/direction", MODEM_RTS_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"out\" > /sys/class/gpio/%s/direction", MODEM_DTR_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"in\" > /sys/class/gpio/%s/direction", MODEM_STATUS_GPIO_STR);
    system(tempString);

    //make it off at initialization, Active low pins
    sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_ON_OFF_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_RESET_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_4V_ENABLE_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"0\" > /sys/class/gpio/%s/value", MODEM_DTR_GPIO_STR);
    system(tempString);

    sprintf(tempString, "echo \"0\" > /sys/class/gpio/%s/value", MODEM_RTS_GPIO_STR);
    system(tempString);

    printf("Modem IO pin configured in OFF condition\n");

    //to avoid toggle pin instantly to on
    sleep(1);
}

void ModemPowerOn(void)
{
    char tempString[256] = {0, };

    #if 0
    if(!StartUSBMonitorThread())
    {
	    DebugLTEPrintLog(ERROR_LOG, "Failed to start USB Monitor thread");
	    DebugLTEPrintLog(ERROR_LOG, "Application can't differntiate between absent modem and unknown modem");
	}
    #endif

    system("cat /sys/class/gpio/gpio159/value");

    sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_4V_ENABLE_GPIO_STR);
    system(tempString); usleep(250000);

    sprintf(tempString, "echo \"0\" > /sys/class/gpio/%s/value", MODEM_4V_ENABLE_GPIO_STR);
    system(tempString); usleep(250000);

    if(gModemCnfg[0].mModemType == MODEM_TYPE_4G)
    {
        sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_RESET_GPIO_STR);
        system(tempString);
        usleep(250000);

        sprintf(tempString, "echo \"0\" > /sys/class/gpio/%s/value", MODEM_RESET_GPIO_STR);
        system(tempString);
        usleep(250000);
    }
    else if(gModemCnfg[0].mModemType == MODEM_TYPE_2G)
    {
        sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_ON_OFF_GPIO_STR);
        system(tempString);
        sleep(2);

        sprintf(tempString, "echo \"0\" > /sys/class/gpio/%s/value", MODEM_ON_OFF_GPIO_STR);
        system(tempString);
        usleep(250000);
    }

    printf("Modem Switched on\n");

    // Read the value of GPIO 159
    system("echo \"in\" > /sys/class/gpio/gpio159/direction");
    system("cat /sys/class/gpio/gpio159/value");

    sleep(10);
#if 0
    char tmp[64] = {0, };
    ModemReqResponse("AT\r\n", tmp, sizeof(tmp), 100000);
    printf("Response:%s\n", tmp);
    sleep(2);
    ModemReqResponse("AT\r\n", tmp, sizeof(tmp), 100000);
    printf("Response:%s\n", tmp);
    sleep(2);
    ModemReqResponse("AT\r\n", tmp, sizeof(tmp), 100000);
    printf("Response:%s\n", tmp);
#endif
}

void ModemPowerOff(void)
{
    char tempString[256] ={0, };

    printf("STATUS PIN: \n");
    system("cat /sys/class/gpio/gpio159/value");

    //close all pppd instance
    system("poff -a");

    if(gModemCnfg[0].mModemType == MODEM_TYPE_4G)
    {
        //switch modem to off condition, Active low pins
        sprintf(tempString, "echo \"0\" > /sys/class/gpio/%s/value", MODEM_ON_OFF_GPIO_STR);
        system(tempString);

        usleep(250000);

        sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_ON_OFF_GPIO_STR);
        system(tempString);

        sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_RESET_GPIO_STR);
        system(tempString);
    }
    else if(gModemCnfg[0].mModemType == MODEM_TYPE_2G)
    {
        sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_ON_OFF_GPIO_STR);
        system(tempString);
        sleep(2);

        sprintf(tempString, "echo \"0\" > /sys/class/gpio/%s/value", MODEM_ON_OFF_GPIO_STR);
        system(tempString);
        usleep(250000);
    }

    printf("STATUS PIN: \n");
    system("cat /sys/class/gpio/gpio159/value");

    sprintf(tempString, "echo \"1\" > /sys/class/gpio/%s/value", MODEM_4V_ENABLE_GPIO_STR);
    system(tempString);

    printf("Modem IO pin switched off\n");
}

int ModemDetectModelID(void)
{
    char tModelString[128] = {0,};
    char buffer[256]  = {0, };
    char *bufptr;
    int  nbytes;

    ModemGetId(tModelString);

    for (int idx = 0; idx < MAX_SUPPORTED_MODEM; ++idx)
    {
        //printf("gModemCnfg[%d].mModelId:%s\n", idx, gModemCnfg[idx].mModelId);

        if (strstr(tModelString, gModemCnfg[idx].mModelId))
        {
            ModemNameselect(tModelString, gModemCnfg[idx].mModemName);
            printf("Detected Model from list name: %s\n", gModemCnfg[idx].mModemName);
            gModemIndex = idx;
            return 0;
        }
    }

    printf("Unknown model detected. Not from the list\n");
    gModemIndex = -1;

    return -1;
}

int ModemNameselect(char *pModelId, char *pModemName)
{
    if((NULL == pModelId) || (NULL == pModemName))
    {
        printf("Error pModelId(%p) or pModemName(%p) is null at %s\n", pModelId, pModemName, __func__ );
        return -1;
    }

    if(strstr(pModelId, "LE910-EU V2"))
    {
        strcpy(pModemName, "NL-SW-LTE-TC4EU");
    }
    else if(strstr(pModelId, "SIMCOM_SIM800C"))
    {
        strcpy(pModemName, "SIMCOM_2G_SIM800C");
    }

    return 0;
}

int ModemTtyInterfaceDetection(void)
{
    return 0;

    int tIsTtyInterfaceDetected = -1;

    //StopUSBMonitorThread();
    //DebugLTEPrintLog(NOTIFICATION_LOG, "\"%d\" USB device added and \"%d\" USB device removed During %d seconds after modem power on", addedUSBnum, removedUSBnum, INTERFACE_DETECT_INTERVAL);
#if 0
    if (access(TTY_DEVICE, R_OK | W_OK | F_OK) == -1)
    {
        #if 0
        if(addedUSBnum) {
            DebugLTEPrintLog(NOTIFICATION_LOG, "Not any known linux interface detected");
            DebugLTEPrintLog(NOTIFICATION_LOG, "Last added unknown USB device info: idVendor: [%s] idProduct: [%s]", USBvid, USBpid);
            ModemStatus = kUnknown;
            ismodemknown = false;
        } else {
            DebugLTEPrintLog(NOTIFICATION_LOG, "Not any modem or USB device detected");
            ModemStatus = KAbsent;
            ismodemknown = false;
        }
        #endif
        tIsTtyInterfaceDetected = -1;
    }
    else
    {
        //DebugLTEPrintLog(NOTIFICATION_LOG, "Last added known USB device info: idVendor: [%s] idProduct: [%s]", USBvid, USBpid);
        //ModemStatus = kEnable;
        tIsTtyInterfaceDetected = 0;
    }
#endif
    return tIsTtyInterfaceDetected;
}

/*
	function ModemInitialization()
	This function will get status of all parameter by which we can identify that all are workigng well like communicate
	AT command, +csq for getting proper signal stregth and + creg for registration of modem, + cpin for simcard is detected,
	and also getting Modem model , IMEI, Modem firmware no, revision no.  This will help us for troubleshooting modem if having issue
*/

int ModemInitialization(ModemPara_t *pModemPara)
{
    char response[RESPONSESIZE] = {0,};
    char tParavalue[128] = {0,};
    char tFilterStr[RESPONSESIZE] = {0,};
    FILE *tFp = NULL;
    char tCmd[256] = {0, };

#if 0
    //poff if any already  running 	pon service
    sprintf(command,"poff -a");			//close any running ppp interface
    DebugLTEPrintLog(ALL_LOG, "%d: %s --> comamnd:%s", __LINE__, __func__,command);
    fd = popen(command, "r");
    if(fd!=NULL)
    {
        if(fgets(filter_str, sizeof(filter_str), fd)!=NULL)		//OK
            DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> %s : %s ", __LINE__, __func__," poff command for Modem Status ",filter_str);
        pclose(fd);
    }
    else
    {
        DebugLTEPrintLog(ERROR_LOG, "%d: %s --> poff command error:%s", __LINE__, __func__,strerror(errno));
    }
#endif

    /* Find ttyUSB port  and it's link */
    sprintf(tCmd, "ls -l %s", pModemPara->mSerialPort);
    tFp = popen(tCmd, "r");
    if(NULL != tFp)
    {
        if(fgets(tFilterStr, sizeof(tFilterStr), tFp) != NULL)
        {
            printf("%d: %s --> %s : %s ", __LINE__, __func__, tCmd, tFilterStr);
            //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> %s : %s ", __LINE__, __func__,"ls -l /dev/ttyLTE",filter_str);
            //memset(paravalue,0,sizeof(paravalue));
            //SeperateStringAndFind(filter_str,"/dev/ttyLTE"," ",10,paravalue);
            //paravalue[strlen(paravalue)-1]='\0';
            //pthread_mutex_lock(&mutexLTEBuffer);
            //strcpy(lte.port,paravalue);
            //pthread_mutex_unlock(&mutexLTEBuffer);
        }
        pclose(tFp);
    }
    else
    {
        //pthread_mutex_lock(&mutexLTEBuffer);
        //strcpy(lte.port,"-1");
        //pthread_mutex_unlock(&mutexLTEBuffer);
        //DebugLTEPrintLog(ERROR_LOG, "%d: %s --> ls -l /dev/ttyLTE:%s", __LINE__, __func__,strerror(errno));
        //printf("%d: %s --> ls -l /dev/ttyLTE:%s", __LINE__, __func__,strerror(errno));
    }

    if (access(pModemPara->mSerialPort, R_OK | W_OK | F_OK) == -1)
    {
        printf("%s not detected, Power on sequence will be executed\n", pModemPara->mSerialPort);
        return -1;
    }

    /* First check Modem is responding or not for AT command */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT\r\n", response, RESPONSESIZE, DELAYMS*100);

    if(NULL != strstr(response,"OK"))
    {
        printf("request:AT, response:OK\n");
    }
    else
    {
        printf("request:AT, response:%s", response);
        return -1;
    }

    /* Disable Echo */
    memset(response,0,sizeof(response));
    ModemReqResponse("ATE0\r\n", response, RESPONSESIZE, DELAYMS*100);

    if(NULL != strstr(response,"OK"))
    {
        printf("request:ATE0, response:OK\n");
    }
    else
    {
        printf("request:ATE0, response:%s", response);
        return -1;
    }

    /* Check Signal strength of Modem */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CSQ\r\n", response, RESPONSESIZE, DELAYMS*100);

    if(NULL != strstr(response,"+CSQ"))
    {
        memset(tParavalue, 0, sizeof(tParavalue));
        SeperateStringAndFind(response,"+CSQ"," ,:",1,tParavalue);

        pModemPara->mSignal = atoi(tParavalue);

        printf("request:AT+CSQ, response:%s\n", response);
        printf("SIGNAL STRENGTH : %d\n", pModemPara->mSignal);
        if( (pModemPara->mSignal < 10) || (pModemPara->mSignal > 31) )
        {
            printf("request:AT+CSQ, response:%s\n", response);
        }
    }
    else
    {
        pModemPara->mSignal = -1;
        printf("request:AT+CSQ, response:%s\n", response);
        return -1;
    }

    /* Check SIM CARD Present in sim slot, if SIM card is connected then "READY" and if not "ERROR"
        Before checking registration , first check Sim card is inserted or not?
    */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CPIN?\r\n", response, RESPONSESIZE, DELAYMS*100);
    if(NULL != strstr(response,"+CPIN"))
    {
        memset(tParavalue,0, sizeof(tParavalue));
        SeperateStringAndFind(response,"+CPIN"," ,:",1,tParavalue);
        strcpy(pModemPara->mSimstatus, tParavalue);

        printf("request: AT+CPIN?, response:%s\n", response);

        /* If sim card not ready then gives error */
        if(NULL != strstr(response,"ERROR"))
        {
            printf("request:AT+CPIN?, response:%s", response);
        }
    }
    else
    {
        strcpy(pModemPara->mSimstatus, "-1");
        printf("request: AT+CPIN?, response:%s\n", response);
        return -1;
    }

    /* Modem Model No */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CGMM\r\n", response, RESPONSESIZE, DELAYMS*100);
    printf("request:AT+CGMM, response:%s\n", response);
    SeperateStringAndFind(response,"+CGMM"," ,:",0,tParavalue);
    strcpy(pModemPara->mModelno, tParavalue);

    /* Modem Revision NO */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CGMR\r\n", response, RESPONSESIZE, DELAYMS*100);
    printf("request:AT+CGMR, response:%s\n", response);
    SeperateStringAndFind(response,"+CGMR"," ,:",0,tParavalue);
    strcpy(pModemPara->mRevision, tParavalue);

    /* Now Modem manufacture ID */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CGMI\r\n", response, RESPONSESIZE, DELAYMS*100);
    SeperateStringAndFind(response,"+CGMI"," ,:",0,tParavalue);
    printf("request:AT+CGMI, response:%s\n", response);
    strcpy(pModemPara->mManufacturedid, tParavalue);

    /* Modem IMEI No */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CGSN\r\n", response, RESPONSESIZE, DELAYMS*100);
    printf("request:AT+CGSN, response:%s", response);
    SeperateStringAndFind(response,"+CGSN"," ,:",0,tParavalue);
    strcpy(pModemPara->mImei, tParavalue);

    /* Check Registration of Modem */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CREG?\r\n", response, RESPONSESIZE, DELAYMS*100);
    printf("request:AT+CREG?, response:%s", response);
    if(NULL != strstr(response,"+CREG"))
    {
        memset(tParavalue,0,sizeof(tParavalue));
        SeperateStringAndFind(response,"+CREG"," ,:",2,tParavalue);
        pModemPara->mRegistration = atoi(tParavalue);

        /* If 1 == Home registered and 5 for Roaming registered  card */
        if( (pModemPara->mRegistration != 1) && (pModemPara->mRegistration != 5) )
        {
            printf("SIM Card not registered\n");
            return -1;
        }
    }
    else
    {
        pModemPara->mRegistration = -1;
        return -1;
    }

    return 0;
}
/**
 * Set APN name to chat script
 * */
int ModemSetApnNameToChatScript(ModemCnfg_t *pModemCnfg)
{
    char tFilePath[FILEPATH] = {0,};
    char tNewApn[COMMANDSIZE];
    char tOldApn[COMMANDSIZE] = {0,};
    char tFileName[255] = {0,};

    if(NULL == pModemCnfg)
    {
        printf("Error pModemCnfg is null at %s\n", __func__ );
        return -1;
    }

    //strcpy(tFileName, "NL-SW-LTE-TC4EU");
    strcpy(tFileName, pModemCnfg->mModemName);
    sprintf(tFilePath, "%s%s-chat","/etc/ppp/peers/", tFileName);

    //strcpy(tNewApn, "airtelgprs.com");
    strcpy(tNewApn, gModemPara.mApnName);
    printf("FILE PATH: %s\n", tFilePath);
    printf("NEWAPN:%s\n", tNewApn);

    if(GetSet_Apn_Name(tFilePath, tOldApn, tNewApn) != 0)
    {
        printf("Failed to update APN name in chatscript\n");
        return -1;
    }

    return 0;
}

int GetSet_Apn_Name(const char *pFilePath, const char *oldapnname, const char *pNewApnName)
{
    FILE *fd;
    char tFilterStr[RESPONSESIZE] ={0,};
    char tApnStr[RESPONSESIZE] = {0,};
    char tCommand[COMMANDSIZE + FILEPATH] = {0,};
    char tSysCommand[COMMANDSIZE+FILEPATH] = {0,};

    if ((NULL == pNewApnName) || (strlen((char*)pNewApnName)) <= 0)
    {
        printf("Error pNewApnName is null or empty at %s\n", __func__ );
        return -1;
    }

    if ((NULL == pFilePath) || (strlen((char*)pFilePath) <= 0))
    {
        printf("Error pFilePath is null or empty at %s\n", __func__ );
        return -1;
    }

    sprintf(tCommand, "cat %s | grep AT+CGDCONT", pFilePath);
    fd = popen(tCommand, "r");
    if(NULL == fd)
    {
        return -1;
    }

    fgets(tFilterStr, sizeof(tFilterStr), fd);

    pclose(fd);

    ReplaceAPNName(tFilterStr, pNewApnName, tApnStr);

    /* if already apn name added, skip to editing */
    if(NULL != strstr(tFilterStr, (char*)pNewApnName))
    {
        return 0;
    }

    sprintf(tSysCommand,"sed -i '/AT+CGDCONT/c\%s' %s",tApnStr, pFilePath);
    system(tSysCommand);

    return 0;
}

/*
function : ModemCheckPPPInterfaceStatus
This function will find status of PPP interface usig socket command list.
Function try to find ppp0 interface, UP status and RUNNING status.
*/

int ModemCheckPPPInterfaceStatus(void)
{
    int ppp_ifr_flags;

    if(Is_Interface_Present((char*)"ppp0"))		//find ppp0 interface out of all interface
    {
        ppp_ifr_flags = Get_Interface_Status("ppp0");
        if(ppp_ifr_flags & IFF_UP)		//is interface UP status
        {
            if(ppp_ifr_flags & IFF_RUNNING)  //is interface RUNNING status
            {
                printf("ppp0 interface is up and running\n");
                return 0;
            }
            printf("ppp0 interface not RUNNING\n");
        }

        printf("ppp0 interface is not up\n");
        return -1;
    }

    printf("ppp0 interface not found\n");
    return -1;
}

/*
function : ModemFindTroubleShootParameters
This function will find all modem related parameter as well file list for troubleshooting
modem status.
*/

int ModemFindTroubleShootParameters(ModemCnfg_t *pModemCnfg)
{
    FILE *fd;
    char command[50],filter_str[64];
    char request[COMMANDSIZE],response[32];
    char ModemAPNName[32];

#if 0
    if(NULL == pModemCnfg)
    {
        return -1;
    }

    if(0 == strlen(pModemCnfg->mFileName))
    {
        //DebugLTEPrintLog(ERROR_LOG, "%d: %s --> Modem ID Null Found", __LINE__, __func__);
        return -1;
    }
#endif

    /* Run Poff script to stop pppd daemon if running */
    /* Close any running ppp interface */
    sprintf(command,"poff -a");
    //DebugLTEPrintLog(ALL_LOG, "%d: %s --> comamnd:%s", __LINE__, __func__,command);
    fd = popen(command, "r");
    if(fd != NULL)
    {
        if(fgets(filter_str, sizeof(filter_str), fd) != NULL)
        {
            //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> %s : %s ", __LINE__, __func__," poff command for troubleshoot",filter_str);
        }
        pclose(fd);
    }
    else
    {
        //DebugLTEPrintLog(ERROR_LOG, "%d: %s --> poff command error:%s", __LINE__, __func__,strerror(errno));
    }

    /* Find ttyUSB port  and it's link */
    sprintf(command,"ls -l /dev/ttyACM0");   //Added command in string and remove from popen
    fd = popen(command,"r");
    //DebugLTEPrintLog(ALL_LOG, "%d: %s --> comamnd:%s", __LINE__, __func__,command);
    if(fd != NULL)
    {
        if(fgets(filter_str, sizeof(filter_str), fd) != NULL)
        {
            //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> %s : %s ", __LINE__, __func__,"ls -l /dev/ttyLTE",filter_str);
            //char paravalue[VALUEBUFFER];
            //memset(paravalue,0,sizeof(paravalue));
            //SeperateStringAndFind(filter_str,"/dev/ttyLTE"," ",10,paravalue);
            //paravalue[strlen(paravalue)-1]='\0';
            //pthread_mutex_lock(&mutexLTEBuffer);
            //strcpy( lte.port,paravalue);
            //pthread_mutex_unlock(&mutexLTEBuffer);
        }
        pclose(fd);
    }
    else
    {
        //pthread_mutex_lock(&mutexLTEBuffer);
        //strcpy(lte.port,"-1");
        //pthread_mutex_unlock(&mutexLTEBuffer);
        //DebugLTEPrintLog(ERROR_LOG, "%d: %s --> ls -l /dev/ttyLTE:%s", __LINE__, __func__,strerror(errno));
    }

    /* Check Modem is responding for AT command or not */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT\r\n", response, sizeof(response), DELAYMS*100);
    //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,response);
    if(NULL != strstr(response,"OK"))
    {
        //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:AT, response:OK ", __LINE__, __func__,request,response);
    }
    else
    {
        //DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,"No response from modem");
        return 0;
    }

    /* Disable Echo */
    ModemReqResponse("ATE0\r\n", response, sizeof(response), DELAYMS*100);
    //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,response);
    if(strstr(response,"OK")!= NULL)
    {
        //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:ATE0, response:OK ", __LINE__, __func__,request,response);
    }
    else
    {
        //DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,"No response from modem");
        return 0;
    }


    //Now check Signal stregth of Modem
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CSQ\r\n", response, sizeof(response), DELAYMS*200);
    //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,response);
    if(strstr(response,"+CSQ") != NULL)
    {
#if 0
        char paravalue[VALUEBUFFER];
        memset(paravalue,0,sizeof(paravalue));
        SeperateStringAndFind(response,"+CSQ"," ,:",1,paravalue);
        pthread_mutex_lock(&mutexLTEBuffer);
        lte.signal=atoi(paravalue);
        pthread_mutex_unlock(&mutexLTEBuffer);
        if(atoi(paravalue)<10 || atoi(paravalue)>31)    // Signal stregth just for information
        {
            DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s %s", __LINE__, __func__,request,paravalue,"Signal strength out of range");
        }
#endif
    }
    else
    {
#if 0
        pthread_mutex_lock(&mutexLTEBuffer);
        lte.signal=-1;
        pthread_mutex_unlock(&mutexLTEBuffer);
        DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,"No response from modem");
#endif
        return 0;
    }

    /* Check is SIM CARD Present in sim slot */
    memset(response,0,sizeof(response));
    ModemReqResponse("AT+CPIN?\r\n", response, sizeof(response), DELAYMS*200);
    //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,response);
    if(strstr(response,"+CPIN") != NULL)
    {
#if 0
        char paravalue[VALUEBUFFER];
        memset(paravalue,0,sizeof(paravalue));
        SeperateStringAndFind(response,"+CPIN"," ,:",1,paravalue);
        pthread_mutex_lock(&mutexLTEBuffer);
        strcpy(lte.simstatus,paravalue);
        pthread_mutex_unlock(&mutexLTEBuffer);
        if(FindSubstr(response,"ERROR")!=-1)
            DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s %s", __LINE__, __func__,request,paravalue,"Simcard not detected");
#endif
    }
    else
    {
#if 0
        pthread_mutex_lock(&mutexLTEBuffer);
        strcpy(lte.simstatus,"-1");
        pthread_mutex_unlock(&mutexLTEBuffer);
        DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,"No response from modem");
#endif
        return 0;
    }
#if 0
    //Now check Regisration of Modem
    if(strcmp(&MModel->modelID[0],(const char*)"HE910")==0)
    {

        memset(request,0,sizeof(request));memset(response,0,sizeof(response));
        sprintf(request,"AT+CREG?\r\n");
        GetModemReqResponse(request,response,DELAYMS*200);
        DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,response);
        if(FindSubstr(response,"+CREG")!=-1)
        {
            char paravalue[VALUEBUFFER];
            memset(paravalue,0,sizeof(paravalue));
            SeperateStringAndFind(response,"+CREG"," ,:",2,paravalue);
            pthread_mutex_lock(&mutexLTEBuffer);
            lte.registration=atoi(paravalue);
            pthread_mutex_unlock(&mutexLTEBuffer);
            if(atoi(paravalue)!=1 && atoi(paravalue)!=5)  // 1for Home registered and 5 for Roaming registered  card
            {
                DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s %s", __LINE__, __func__,request,paravalue,"Simcard not registered");
                return 0;
            }

        }
        else
        {
            pthread_mutex_lock(&mutexLTEBuffer);
            lte.registration=-1;
            pthread_mutex_unlock(&mutexLTEBuffer);
            DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,"No response from modem");
            return 0;
        }
    }
    else
    {
        memset(request,0,sizeof(request));memset(response,0,sizeof(response));
        sprintf(request,"AT+CEREG?\r\n");
        GetModemReqResponse(request,response,DELAYMS*200);
        DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,response);
        if(FindSubstr(response,"+CEREG")!=-1)
        {
            char paravalue[VALUEBUFFER];
            memset(paravalue,0,sizeof(paravalue));
            SeperateStringAndFind(response,"+CEREG"," ,:",2,paravalue);
            pthread_mutex_lock(&mutexLTEBuffer);
            lte.registration=atoi(paravalue);
            pthread_mutex_unlock(&mutexLTEBuffer);
            if(atoi(paravalue)!=1 && atoi(paravalue)!=5)  // 1for Home registered and 5 for Roaming registered  card
            {
                DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s %s", __LINE__, __func__,request,paravalue,"Simcard not registered");
                return 0;
            }

        }
        else
        {
            pthread_mutex_lock(&mutexLTEBuffer);
            lte.registration=-1;
            pthread_mutex_unlock(&mutexLTEBuffer);
            DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,"No response from modem");
            return 0;
        }
    }
#endif


    //find cgdcont command response
    ModemReqResponse("AT+CPIN?\r\n", response, sizeof(response), DELAYMS*100);
    //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,response);
    if(strstr(response,"+CGDCONT") != NULL)
    {
        //DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> request:%s, response:%s %s", __LINE__, __func__,request,response,"Existing APN name");
    }
    else
    {
        //DebugLTEPrintLog(ERROR_LOG, "%d: %s --> request:%s, response:%s", __LINE__, __func__,request,"No response from modem");
        return 0;
    }

#if 0
    //find APN Name  from db config and compare with existing
    CSPDBQueryExecuteResponse("SELECT LTE_APN from CSP_LTE;",ModemAPNName,MAX_APNNAME);
    pthread_mutex_lock(&mutexLTEBuffer);
    strcpy(lte.apnname,ModemAPNName);
    pthread_mutex_unlock(&mutexLTEBuffer);
    DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> ModemAPNName:%s", __LINE__, __func__,ModemAPNName);
#endif
    //find ppp log for activity

    //find internet connection status

    //find TX and RX packet for data is transfering and ppp0 is active


    return 1;
}

/*
function ReadConfigurationfromDB
This function will read update_status flag from database if it is udpated by Bootsstrap application.
*/
int ModemReadConfigurationfromDB(ModemCnfg_t *pModemCnfg)
{
#if 0
    char Update_Status[32],isModemEnable[32],ModemAPNName[32];
    if(CSPDBQueryExecuteResponse("select Update_Status from CSP_LTE;",Update_Status,1)==true)
    {
        DebugLTEPrintLog(ALL_LOG, "%d: %s --> Update_Status:%s", __LINE__, __func__,Update_Status);
        if(atoi(Update_Status)==1)
        {
            DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> %s", __LINE__, __func__,"Updated config found");
            if(CSPDBQueryExecuteResponse("SELECT LTE_Enable from CSP_LTE where Update_Status=1;",isModemEnable,1)==true)
            {
                if(atoi(isModemEnable)==0 || atoi(isModemEnable)==1)
                {
                    modempara->modem_enable=atoi(isModemEnable);
                    DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> isModemEnable:%s", __LINE__, __func__,isModemEnable);
                }
                else
                {
                    DebugLTEPrintLog(ERROR_LOG, "%d: %s --> isModemEnable is out of range< 0 or 1 >:%s", __LINE__, __func__,isModemEnable);
                }
                if(CSPDBQueryExecuteResponse("SELECT LTE_APN from CSP_LTE where Update_Status=1;",ModemAPNName,MAX_APNNAME)==true)
                {
                    if(strlen(ModemAPNName)>0)
                    {
                        strcpy(modempara->modem_apnanme,ModemAPNName);
                        pthread_mutex_lock(&mutexLTEBuffer);
                        strcpy(lte.apnname,ModemAPNName);
                        pthread_mutex_unlock(&mutexLTEBuffer);
                        DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> ModemAPNName:%s", __LINE__, __func__,ModemAPNName);
                    }
                    else
                    {
                        DebugLTEPrintLog(ERROR_LOG, "%d: %s --> ModemAPNName is not valid:%s", __LINE__, __func__,ModemAPNName);
                    }

                    if(CSPDBQueryExecute("UPDATE CSP_LTE set Update_Status=0;",NULL)==true)
                    {
                        DebugLTEPrintLog(NOTIFICATION_LOG, "%d: %s --> Update_Status:%s", __LINE__, __func__,Update_Status);
                        return 1;
                    }
                    else
                    {
                        DebugLTEPrintLog(ERROR_LOG, "%d: %s --> Failed to Read Database", __LINE__, __func__);
                    }
                }
                else
                {
                    DebugLTEPrintLog(ERROR_LOG, "%d: %s --> Failed to Read Database", __LINE__, __func__);
                }
            }
            else
            {
                DebugLTEPrintLog(ERROR_LOG, "%d: %s --> Failed to Read Database", __LINE__, __func__);
            }
        }
    }
    else
    {
        DebugLTEPrintLog(ERROR_LOG, "%d: %s --> Failed to Read Database", __LINE__, __func__);
    }
#endif
    return 0;
}

static int ModemWaitIntervalCheck(void)
{
    //struct timespec tCurrentTimeStamp;
    //clock_gettime(CLOCK_MONOTONIC, &tCurrentTimeStamp);
    time_t tCurrentTimeStamp;

    time(&tCurrentTimeStamp);

    if( (tCurrentTimeStamp - gsWaitTimeStamp) >= gsWaitTimeInterval )
    //if( (tCurrentTimeStamp.tv_sec - gsWaitTimeStamp.tv_sec) >= gsWaitTimeInterval )
    {
        //DebugLTEPrintLog(ALL_LOG, "Wait period of %d second completed going to next state", WaitTime);
        gsWaitTimeInterval = 0;
        return 0;
    }
    return -1;
}




/*
Function:ReplaceAPNName
This function will first extract apn name from string and replace new apnname within string and return string.
*/
char ReplaceAPNName(const char *str, const char *keystr, char *apnstr)
{
    int i = 0, j = 0 , CommPosition = 0;
    char CommaStr[32][255] = {0,};

    for(i = 0, CommPosition = 0, j = 0 ; i < strlen(str) ; ++i)
    {
        if((str[i]==',' || str[i]=='\n') && CommPosition<32&& j<255)
        {
            CommaStr[CommPosition][j]=str[i];j++;
            CommaStr[CommPosition][j]=0;
            if(CommPosition==2) //position of 3 which is IP //AT+CGDCONT=3,"IP","[apn]",
            {
                strcat(apnstr,"\"");
                strcat(apnstr,keystr);
                if(str[i]==',')
                    strcat(apnstr,"\",");
                else
                    strcat(apnstr,"\"");
            }
            else
            {
                strcat(apnstr,&CommaStr[CommPosition][0]);
            }
            CommPosition++;j=0;
        }
        else
        {
            CommaStr[CommPosition][j]=str[i];j++;
        }
    }
    return 0;
}

/*
function : ModemActivatePPPInterface
This function will activate PPP interface using pppd utility command list pon <ppp option file>
*/
int ModemActivatePPPInterface(ModemCnfg_t *pModemCnfg)
{
    FILE *tFd;
    char tCommand[COMMANDSIZE] = {0,};
    char tFilterStr[64] = {0,};
    char tFileName[256] = {0,};

    if(NULL == pModemCnfg)
    {
        printf("Error at pModemCnfg is null at %s\n", __func__ );
        return -1;
    }

    /* APN File name string length should not be zero */
    if (!strlen(pModemCnfg->mFileName))
    {
        printf("PPP interface script file name empty found at %s\n", __func__);
        return -1;
    }

    /* Running pon script to start pppd daemon */

    //strcpy(tFileName, "NL-SW-LTE-TC4EU");
    strcpy(tFileName, pModemCnfg->mModemName);
    sprintf(tCommand, "pon %s", tFileName);
    tFd = popen(tCommand, "r");
    if(NULL == tFd)
    {
        printf("Error (%s)in pon command\n",strerror(errno));
        return -1;
    }

    //OK	AT+CGDCONT=1,"IP","airtelgprs.com","",0,0
    while(NULL != fgets(tFilterStr, sizeof(tFilterStr), tFd))
    {
        //DebugLTEPrintLog(ALL_LOG, "%d: %s --> pon:%s", __LINE__, __func__,tFilterStr);
        printf("%d: %s --> pon:%s\n", __LINE__, __func__,tFilterStr);
    }

    pclose(tFd);

    return 0;
}

int GetModemIdTemp(int fd)   /* I - Serial port file */
{
    char buffer[255];  /* Input buffer */
    char *bufptr;      /* Current char in buffer */
    int  nbytes;       /* Number of bytes read */
    int  tries;        /* Number of tries so far */
    char cmd[] = "AT+CGMM\r\n";

    for (tries = 0; tries < 3; tries ++)
    {
        /* send an AT command followed by a CR */
        if (write(fd, cmd, strlen(cmd)) < strlen(cmd))
            continue;

        /* read characters into our string buffer until we get a CR or NL */
        bufptr = buffer;
        while ((nbytes = read(fd, bufptr, buffer + sizeof(buffer) - bufptr - 1)) > 0)
        {
            bufptr += nbytes;
            if (bufptr[-1] == '\n' || bufptr[-1] == '\r')
                break;
        }

        printf("buffer:%s\n", buffer);

        /* nul terminate the string and see if we got an OK response */
        *bufptr = '\0';

        //if (strncmp(buffer, "OK", 2) == 0)
        return (0);
    }

    return (-1);
}

void ModemGetId(char* pModelId)
{
    int fd = -1;
    const char *request = "AT+CGMM\r\n";
    char tResponseData[128] = {0,};

    if(NULL == pModelId)
    {
        printf("Error at pModelId is NULL at %s\n", __func__ );
        return;
    }

    fd = SerialPortOpen(gModemPara.mSerialPort, gModemPara.mSerialBaudRate);
    if (fd <= -1)
    {
        printf("Failed at SerialPortOpen at %s\n", __func__);
        return;
    }

    SerialPortWriteRead(fd, request, strlen(request), tResponseData, sizeof(tResponseData));

    printf("tResponseData - %s\n", tResponseData);
    SerialPortClose(fd);

    strcpy(pModelId, tResponseData);
}

int ModemReqResponse(const char *pRequest, char* pResponse, int RespLen, int iDelayus)
{
    int fd;
    int tRetVal = -1;

    fd = SerialPortOpen(gModemPara.mSerialPort, gModemPara.mSerialBaudRate);
    if(fd <= -1)
    {
        printf("SerialPortOpen failed at %s\n", __func__ );
        return tRetVal;
    }

    SerialPortWriteRead(fd, pRequest, strlen(pRequest), pResponse, RespLen);

    SerialPortClose(fd);
    tRetVal = 0;

    printf("Serial read and write successfull\n");

    return (tRetVal);
}

/*  Function: SeperateStringAndFind
    This function will seperate string with given filterstr and return string which are matched with counterfiter position
*/
int SeperateStringAndFind(char *string, const char *headstr, const char *filterstr, char counterfiter, char *resultstr)
{
    char countstr=0;;
    char *p;

    p = strtok (string,filterstr);
    if(countstr == counterfiter)
    {
        strcpy(resultstr,p);
        return 1;
    }

    while (p!= NULL)
    {
        p = strtok (NULL, filterstr);
        countstr++;
        if(countstr==counterfiter)
        {
            strcpy(resultstr,p);
            return 1;
        }
    }
    return 0;
}


#if 0
void ModemCnfg_t::ModemCnfgInit(void)
{
    strcpy(this->mModemName[0], "NL-SW-LTE-GELS3-C");
    strcpy(this->mModemId[0], "ELS31-V");
    strcpy(this->mFileName[0], "vzw-GELS3");

    strcpy(this->mModemName[1], "NL-SW-HSPA");
    strcpy(this->mModemId[1], "HE910");
    strcpy(this->mFileName[1], "att-TNAG");

    strcpy(this->mModemName[2], "NL-SW-LTE-S7648");
    strcpy(this->mModemId[2], "HL7648");
    strcpy(this->mFileName[2], "att-S7588-T");

    strcpy(this->mModemName[3], "NL-SW-LTE-TC4EU");
    strcpy(this->mModemId[3], "LE910-EU V2");
    strcpy(this->mFileName[3], "NL-SW-LTE-TC4EU");
    this->mModemIndex = 3;
}
#endif
