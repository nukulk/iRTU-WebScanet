//
// Created by mukeshp on 5/29/2020.
//
#include "InterfaceManagement.h"
#include "DNSResolver.h"

InterfaceInfo_t gsInterfaceInfo[MAX_INTERFACE] =
        {
            {"ppp0", INTERFACE_NOT_PRESENT, 0, false, false},
            {"eth0", INTERFACE_NOT_PRESENT, 1, false, false},
            {"wlan0", INTERFACE_NOT_PRESENT, 2, false, false}
        };

void* InterfacePriorityMgmtThread(void* arg)
{
    int tNlSocket = -1;

    pthread_detach(pthread_self());

    printf("Interface info at boot\n");
    SetInterfaceInfo();

    tNlSocket = CreateNetlinkSocket();
    if(tNlSocket < 0)
    {
        printf("Error at CreateNetlinkSocket tNlSocket(%d) at %s\n", tNlSocket, __func__ );
    }
    else
    {
        PollNetlinkEvent(tNlSocket);
    }

    pthread_exit(NULL);
}

int CreateNetlinkSocket(void)
{
    int tRetVal = -1;
    int nl_socket = -1;
    struct sockaddr_nl  tNetLinkSock = {0, };  // local addr struct

    /** It creates a NETLINK_ROUTE netlink socket which will listen to the RTMGRP_LINK (network interface create/delete/up/down events) and
     * RTM-GRP_IPV4_IFADDR (IPv4 addresses add/delete events) multicast groups.
     * RTMGRP_IPV4_IFADDR for RTM_NEWLINK and RTM_DELLINK event
     * RTMGRP_IPV4_IFADDR for RTM_NEWADDR and RTM_DELADDR event
     * RTMGRP_IPV4_ROUTE for RTM_NEWROUTE and RTM_DELROUTE event
     * */

    tNetLinkSock.nl_family = AF_NETLINK;
    tNetLinkSock.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_ROUTE;

    // set groups we interested in
    tNetLinkSock.nl_pid = getpid();

    // set out id using current process id
    do
    {
        /* create NETLINK socket */
        nl_socket = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
        if (nl_socket < 0)
        {
            printf("Failed to create netlink socket: %s", (char*)strerror(errno));
            continue;
        }

        /* bind socket */
        tRetVal = bind(nl_socket, (struct sockaddr*)&tNetLinkSock, sizeof(tNetLinkSock));
        if (tRetVal < 0)
        {
            printf("Failed to bind netlink socket: %s", (char*)strerror(errno));
            sleep(1);
            close(nl_socket);
            nl_socket = -1;
        }
    } while (nl_socket < 0);

    return nl_socket;
}

int PollNetlinkEvent(int nl_socket)
{
    fd_set rtnetlinkfds;
    int tRetVal;

    FD_ZERO (&rtnetlinkfds);

    while (1)
    {
        FD_SET (nl_socket, &rtnetlinkfds);

        tRetVal = select(nl_socket + 1, &rtnetlinkfds, NULL, NULL, NULL);
        if (tRetVal <= 0)
        {
            printf("Error RT netlink select() (%s) at %s", (char *) strerror(errno), __func__);
        }
        else
        {
            ParseNetlinkEvent(nl_socket);
        }
    }

    close(nl_socket);

    return 0;
}

int ParseNetlinkEvent(int nlsock)
{
    char buf[IFLIST_REPLY_BUFFER] = {0, };
    struct sockaddr_nl snl;
    struct iovec iov = { buf, sizeof(buf) };
    struct msghdr msg = { (void *) &snl, sizeof(snl), &iov, 1, NULL, 0, 0 };

    int status = recvmsg(nlsock, &msg, 0);
    if(status < 0)
    {
        switch (errno)
        {
            case EBADF:
            case ENOTSOCK:
            case EINVAL:
            case EFAULT:
                printf("Error return for netlink socket recvmsg() API: (%s) at %s\n", (char*)strerror(errno), __func__ );
                exit(0);
                break;
            case EAGAIN:
            case EINTR:
            case ENOMEM:
            default:
                printf("Failed to read netlink socket recvmsg() API: (%s) at %s\n", (char*)strerror(errno), __func__ );
                usleep(250000);
                return -1;
                break;
        }
        return 0;
    }
    else if (status == 0)
    {
        printf("read_netlink: EOF at %s\n", __func__ );
    }

    // message parser
    struct nlmsghdr *nlh;
    // read all messagess headers
    for (nlh = (struct nlmsghdr *) buf; NLMSG_OK (nlh, (unsigned int)status) && (nlh->nlmsg_type != NLMSG_DONE); nlh = NLMSG_NEXT (nlh, status))
    {
        // Finish reading
        if (nlh->nlmsg_type == NLMSG_DONE)
            return 0;
        // Message is some kind of error
        if (nlh->nlmsg_type == NLMSG_ERROR)
        {
            struct nlmsgerr *err = (struct nlmsgerr*)NLMSG_DATA(nlh);
            printf("read_netlink: Message is an error %d\n", err->error);
            return -1;
        }

        int len = nlh->nlmsg_len - sizeof(*nlh);

        // validate received msg len
        if ((len < 0) || (nlh->nlmsg_len > status))
        {
            printf("read_netlink: Message length error %d\n", __LINE__);
            return -2;
        }

        switch (nlh->nlmsg_type)
        {
            case RTM_NEWADDR:
                break;
            case RTM_DELADDR:
                break;
            case RTM_NEWROUTE:
            case RTM_DELROUTE:
                printf("RT route Event receieved >>\n");
                RouteEventProcess(nlh);
                break;
            case RTM_NEWLINK:
            case RTM_DELLINK:
                printf("RT Netlink Event recieved >>\n");
                LinkEventProcess(nlh);
                break;
            default:
                printf("msg_handler: Unknown netlink nlmsg_type %d", nlh->nlmsg_type);
                break;
        }
    }
    return 0;
}

void RouteEventProcess(const struct nlmsghdr *nlh)
{
    struct rtmsg *route_entry; /* This struct represent a route entry in the routing table */
    int tInterfaceindex = -1;
    char ifname[10], datastr[100];

    bzero(ifname, sizeof(ifname));
    route_entry = (struct rtmsg *) NLMSG_DATA(nlh);

    if (route_entry->rtm_table != RT_TABLE_MAIN)
        return;

    for (int i = 0; i < MAX_INTERFACE; ++i)
    {
        if (gsInterfaceInfo[i].mIsActive == true)
        {
            tInterfaceindex = i;
            strcpy(ifname, gsInterfaceInfo[i].mIfname);
            break;
        }
    }

    if (tInterfaceindex == -1)
    {
        // not any interface up
        return;
    }

    FILE *routefd = popen("ip route | head -1", "r");
    if (routefd != NULL)
    {
        fgets(datastr, sizeof(datastr), routefd);
        pclose(routefd);
    }
    else
    {
        printf("%d: %s --> \"ip route | head -1\" command error:%s", __LINE__, __func__, strerror(errno));
    }

    if ( (!strstr(datastr, "default")) || (!strstr(datastr, ifname)) )
    {
        // if not any default route or not active interface route than reload route
        printf("Interface with internet: %s, Command: \"ip route | head -1\", Response: %s\n", ifname, datastr);
        printf("No route of valid interface at top, reloading ...\n");

        if (!DelDefaultGateway(NULL))
        {
            printf("failed to delete all default route");
        }

        SelectAndSwitchInternet(UPDATE_ROUTE_ONLY);
    }
}

void LinkEventProcess(const struct nlmsghdr *nlh)
{
    struct ifinfomsg *iface;
    char * ifname;
    char * macaddr;
    int len;
    int interfaceindex = -1;
    struct rtattr *rtattrptrarray[IFLA_MAX + 1];

    iface = (struct ifinfomsg*) NLMSG_DATA(nlh);

    ParseRtattr(rtattrptrarray, IFLA_MAX, IFLA_RTA(iface), nlh->nlmsg_len);

    if (rtattrptrarray[IFLA_IFNAME])
    {
        // get network interface name
        ifname = (char*)RTA_DATA(rtattrptrarray[IFLA_IFNAME]);
        for (int i = 0; i < MAX_INTERFACE; ++i)
        {
            if (strcmp(gsInterfaceInfo[i].mIfname, ifname) == 0)
            {
                interfaceindex = i;
                break;
            }
        }

        if (interfaceindex == -1)
        {
            // retun if not valid interface event
            return;
        }
    }
    else
    {
        return;
    }

    if (rtattrptrarray[IFLA_ADDRESS])
    {
        // get network interface name
        macaddr = (char*)RTA_DATA(rtattrptrarray[IFLA_ADDRESS]);
        printf("link: %s, mac: %02x:%02x:%02x:%02x:%02x:%02x",
                         ifname, macaddr[0], macaddr[1], macaddr[2],
                         macaddr[3], macaddr[4], macaddr[5]);
    }

    InterfaceStatus_e oldstatus = gsInterfaceInfo[interfaceindex].mIfStatus;

    if (nlh->nlmsg_type == RTM_NEWLINK)
    {
        gsInterfaceInfo[interfaceindex].mIfStatus = INTERFACE_PRESENT;
    }
    else if (nlh->nlmsg_type == RTM_DELLINK)
    {
        gsInterfaceInfo[interfaceindex].mIfStatus = INTERFACE_NOT_PRESENT;
    }

    if (iface->ifi_flags & IFF_UP)
    {
        if (iface->ifi_flags & IFF_RUNNING)
        {
            gsInterfaceInfo[interfaceindex].mIfStatus = INTERFACE_UP_RUNNING;
        }
        else
        {
            gsInterfaceInfo[interfaceindex].mIfStatus = INTERFACE_UP_ONLY;
            if (oldstatus == INTERFACE_UP_RUNNING)
            {
                if (!DelDefaultGateway(gsInterfaceInfo[interfaceindex].mIfname) )
                {
                    printf("failed to delete all route of interface %s", gsInterfaceInfo[interfaceindex].mIfname);
                }
            }
        }
    }

    if ( (gsInterfaceInfo[interfaceindex].mIsActive == false) && (gsInterfaceInfo[interfaceindex].mIfStatus != INTERFACE_UP_RUNNING) )
    {
        // no need to switch Internet
        return;
    }
    else
    {
        switch (gsInterfaceInfo[interfaceindex].mIfStatus)
        {
            case INTERFACE_PRESENT:
                printf("interface %s not up not running\n", ifname);
                break;
            case INTERFACE_NOT_PRESENT:
                printf("interface %s deleted\n", ifname);
                break;
            case INTERFACE_UP_RUNNING:
                printf("interface %s Up and Running\n", ifname);
                break;
            case INTERFACE_UP_ONLY:
                printf("interface %s Only Up\n", ifname);
                break;
        }

        printf("switching internet...\n");
        SelectAndSwitchInternet(UPDATE_DNS_ROUTE);
    }
}

void ParseRtattr(struct rtattr *rtattrarray[], int maxrtarraysize, struct rtattr *rtattval, int len)
{
    /* little helper function to parsing message using standard netlink macroses */
    memset(rtattrarray, 0, sizeof(struct rtattr *) * (maxrtarraysize + 1));

    // while not end of the message
    while (RTA_OK(rtattval, len))
    {
        if (rtattval->rta_type <= maxrtarraysize)
        {
            // read attr
            rtattrarray[rtattval->rta_type] = rtattval;
        }
        // get next attr
        rtattval = RTA_NEXT(rtattval, len);
    }
}

void SetInterfaceInfo(void)
{
    for (int i = 0 ; i < MAX_INTERFACE; ++i)
    {
        if (Is_Interface_Present(gsInterfaceInfo[i].mIfname))
        {
            int tInterfaceStatus = Get_Interface_Status(gsInterfaceInfo[i].mIfname);

            if (tInterfaceStatus & IFF_UP)
            {
                if (tInterfaceStatus & IFF_RUNNING)
                {
                    printf("%s interface is up and running\n", gsInterfaceInfo[i].mIfname);
                    gsInterfaceInfo[i].mIfStatus = INTERFACE_UP_RUNNING;
                }
                else
                {
                    printf("%s interface is up only, not running\n", gsInterfaceInfo[i].mIfname);
                    gsInterfaceInfo[i].mIfStatus = INTERFACE_UP_ONLY;
                }
            }
            else
            {
                printf("%s interface is not present\n", gsInterfaceInfo[i].mIfname);
                gsInterfaceInfo[i].mIfStatus = INTERFACE_NOT_PRESENT;
            }
        }
    }

    SelectAndSwitchInternet(UPDATE_DNS_ROUTE);
}

void SelectAndSwitchInternet(UpdateType_e iUpdateType)
{
    unsigned char tInterfaceIndex = 0;
    unsigned int tIndex = 0;
    char tSearchPriority = 0;

    for (tIndex = 0; tIndex < MAX_INTERFACE; ++tIndex)
    {
        gsInterfaceInfo->mIsInternet = true;
    }

    /* need to switch or reload resolve and route */
    do
    {
        for (tIndex = 0; tIndex < MAX_INTERFACE; ++tIndex)
        {
            if(tSearchPriority == MAX_INTERFACE)
            {
                tSearchPriority = MAX_INTERFACE + 1 ;//To exit from do..while loop.
            }
            if(gsInterfaceInfo[tIndex].mInfPriority == tSearchPriority)
            {
                tInterfaceIndex = tIndex;
                ++tSearchPriority;
                break;
            }
        }

        if(tSearchPriority > MAX_INTERFACE)
        {
            printf("any one interface is not up and not running with internet\n");
            break;
        }

        if(INTERFACE_UP_RUNNING == gsInterfaceInfo[tInterfaceIndex].mIfStatus)
        {
            switch (iUpdateType)
            {
                case UPDATE_DNS_ONLY:
                {
                    printf("Update DNS of interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
                    UpdateDns(tInterfaceIndex);
                    // dhclient reloaded route than
                    // helpful to add route on DNS event
                    UpdateRoute(tInterfaceIndex);
                    break;
                }
                case UPDATE_ROUTE_ONLY:
                {
                    printf("Update route of interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
                    UpdateRoute(tInterfaceIndex);
                    break;
                }
                case UPDATE_DNS_ROUTE:
                {
                    printf("Update route and DNS of interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
                    UpdateRoute(tInterfaceIndex);
                    UpdateDns(tInterfaceIndex);
                    break;
                }
                default:
                {
                    printf("Invalid option at %s\n", __func__ );
                    break;
                }
            }

            if (!IsInternetAvailable()) {
                gsInterfaceInfo[tInterfaceIndex].mIsInternet = false;
                gsInterfaceInfo[tInterfaceIndex].mIsActive = false;
                printf("No Internet available on interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
                UpdateRoute(tInterfaceIndex);
            } else {
                gsInterfaceInfo[tInterfaceIndex].mIsInternet = true;
                gsInterfaceInfo[tInterfaceIndex].mIsActive = true;
                printf("Internet available on interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
                UpdateRoute(tInterfaceIndex);
            }
        }
        else
        {
            gsInterfaceInfo[tInterfaceIndex].mIsInternet = false;
            gsInterfaceInfo[tInterfaceIndex].mIsActive = false;
            printf("No Internet available on interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
            #if 0
            if (!IsInternetAvailable()) {
                gsInterfaceInfo[tInterfaceIndex].mIsInternet = false;
                gsInterfaceInfo[tInterfaceIndex].mIsActive = false;
                printf("No Internet available on interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
                //UpdateRoute(tInterfaceIndex);
            } else {
                gsInterfaceInfo[tInterfaceIndex].mIsInternet = true;
                gsInterfaceInfo[tInterfaceIndex].mIsActive = true;
                printf("Internet available on interface %s\n", gsInterfaceInfo[tInterfaceIndex].mIfname);
                //UpdateRoute(tInterfaceIndex);
            }
            #endif
        }
    } while (!gsInterfaceInfo[tInterfaceIndex].mIsInternet);
}

char IsInternetAvailable(void)
{
    for (char i = 0; i < 3; ++i)
    {
        if(!system("ping4 www.google.com -c 1 -W 1 > /dev/null"))
        {
            // found internet so online
            return 1;
        }
    }

    /* three retry of internet check get failed */
    return 0;
}

/** Is_Interface_Present()
 *  On success, return 1.
 *  On Failure return -1 or 0.
 * */
int Is_Interface_Present(char * ifname)
{
    struct ifaddrs *ifaddr, *ifa;
    int n=0;

    if(getifaddrs(&ifaddr) == -1)
    {
        printf("Error in getifaddrs at %s (%s)\n",__func__, strerror(errno));
        return -1;
    }

    for(ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++)
    {
        if(strstr(ifa->ifa_name,ifname) == NULL)
        {
            //required interface found
            freeifaddrs(ifaddr);
            return 1;
        }
    }

    freeifaddrs(ifaddr);
    return 0;
}

/** function :Get_Interface_Status
    This function will status of given interface.
*/

int Get_Interface_Status(const char * ifname)
{
    int fd;
    struct ifreq ifr;

    /* AF_INET - to define network interface IPv4*/
    /* Creating soket for it.*/
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if(fd < 0)
    {
        printf("Get_Interface_Status error (%s) at %s\n", strerror(errno), __func__ );
        return 0;
    }

    /*AF_INET - to define IPv4 Address type.*/
    ifr.ifr_addr.sa_family = AF_INET;

    /*eth0 - define the ifr_name - port name where network attached.*/
    memcpy(ifr.ifr_name, ifname, IFNAMSIZ-1);

    /*Accessing network interface information by passing address using ioctl.*/
    if(ioctl(fd, SIOCGIFFLAGS, &ifr) < 0)
    {
        printf("ioctl error (%s) at %s\n", strerror(errno), __func__ );
        close(fd);
        return 0;
    }

    /*closing fd*/
    close(fd);
    if(ifr.ifr_flags)
    {
        return ifr.ifr_flags;
    }

    return 0;
}
