//
// Created by mukeshp on 04-Aug-20.
//

#include "HeatSinkControl.h"
#include "HeatSinkConfig.h"

extern HeatsinkCnfg_t gHeatsinkCnfg;

void* HeatSinkCntl_Thread(void* arg)
{
    int tCpuTemperature = 0;

    pthread_detach(pthread_self());

    Heatsink_ConfigurationRead();

    CpuFan_GPIO_Init();

    while (1)
    {
        if(0 != CpuTemperature_Read(&tCpuTemperature))
        {
            printf("CPU temperature reading failed\n");
            sleep(gHeatsinkCnfg.mCpuTempScanInterval);
            continue;
        }

        printf("tCpuTemperature:%d\n", tCpuTemperature);

        if(tCpuTemperature > gHeatsinkCnfg.mThresoldTemperature)
        {
            CpuFan_On();
        }
        else
        {
            CpuFan_Off();
        }

        sleep(gHeatsinkCnfg.mCpuTempScanInterval);
    }
}

int CpuFan_GPIO_Init(void)
{
    int tCpuFanPwcn = -1;
    char temp[256] = {0, };

    sprintf(temp, "/sys/class/gpio/%s/direction", CPU_FAN_PWCN_GPIO_STR);

    tCpuFanPwcn = open(temp, O_RDWR);
    if (tCpuFanPwcn < 0)
    {
        //if not exported already than export it
        sprintf(temp, "echo \"%d\" > /sys/class/gpio/export", CPU_FAN_PWCN_GPIO);
        system(temp);
    }
    close(tCpuFanPwcn);

    sprintf(temp, "echo \"out\" > /sys/class/gpio/%s/direction", CPU_FAN_PWCN_GPIO_STR);
    system(temp);

    return 0;
}

int CpuFan_GPIO_Deinit(void)
{
    char temp[256] = {0, };

    sprintf(temp, "echo \"%d\" > /sys/class/gpio/unexport", CPU_FAN_PWCN_GPIO);
    system(temp);

    return 0;
}

int CpuFan_On(void)
{
    char temp[256] = {0, };

    sprintf(temp, "echo \"1\" > /sys/class/gpio/%s/value", CPU_FAN_PWCN_GPIO_STR);
    system(temp);

    return 0;
}

int CpuFan_Off(void)
{
    char temp[256] = {0, };

    sprintf(temp, "echo \"0\" > /sys/class/gpio/%s/value", CPU_FAN_PWCN_GPIO_STR);
    system(temp);

    return 0;
}

int CpuTemperature_Read(int *pTemperature)
{
    char tCmd[256] = {0, };
    FILE *tFp = NULL;
    char tResp[256] = {0, };

    if(NULL == pTemperature)
    {
        printf("pTemperature is NULL at %s\n", __func__ );
        return -1;
    }

    strcpy(tCmd, "cat /sys/class/thermal/thermal_zone0/temp");

    errno = 0;
    tFp = popen(tCmd, "r");
    if(NULL == tFp)
    {
        printf("Temperature file (%s) does not exist\n", tCmd);
        return -1;
    }

    fgets(tResp, sizeof(tResp) - 1 , tFp);

    fclose(tFp);

    printf("tResp: %s\n", tResp);

    (*pTemperature) = atoi(tResp);

    return 0;
}

