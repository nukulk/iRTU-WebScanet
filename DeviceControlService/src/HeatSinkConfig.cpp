//
// Created by mukeshp on 04-Aug-20.
//

#include "HeatSinkConfig.h"
#include "iniparser.h"

HeatsinkCnfg_t gHeatsinkCnfg;

int Heatsink_ConfigurationRead(void)
{
    dictionary  *tpIni 		= NULL	;
    char     	*tpIniName 	= NULL 	;
    const char 	*tpName 	= NULL	;
    char 		*tmpPtr 	= NULL	;
    int 		tRetCode 	= -1	;
    char    	tCnfgFileName[512] = {0,} ;
    char        tag[256]    = {0,};

    sprintf(tCnfgFileName,"%s/%s", HEAT_SINK_CONFIG_FILE_PATH, HEAT_SINK_CONFIG_FILE);

    tpIniName = tCnfgFileName ;

    errno = 0;
    tpIni = iniparser_load(tpIniName);
    printf("%s\n", strerror(errno));
    if(tpIni == NULL)
    {
        printf("Error in opening file %s\n", tpIniName);
        return tRetCode;
    }

    do
    {
        tpName = iniparser_getstring((const dictionary*)tpIni,"Heatsink_cnfg:cpu_temp_scan_interval", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting Heatsink_cnfg:cpu_temp_scan_interval\n");
            tRetCode = -1;
            break;
        }

        gHeatsinkCnfg.mCpuTempScanInterval = atoi(tpName);

        tpName = iniparser_getstring((const dictionary*)tpIni,"Heatsink_cnfg:thresold_temperature", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting Heatsink_cnfg:thresold_temperature\n");
            tRetCode = -1;
            break;
        }

        gHeatsinkCnfg.mThresoldTemperature = atoi(tpName);

        tRetCode = 0;
    }while (0);

    iniparser_freedict(tpIni);

    printf("CPU temperature scan interval : %d\n", gHeatsinkCnfg.mCpuTempScanInterval);
    printf("Thresold temperature          : %d\n", gHeatsinkCnfg.mThresoldTemperature);

    return tRetCode;
}
