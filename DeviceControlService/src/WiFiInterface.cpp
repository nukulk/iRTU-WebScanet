//
// Created by mukeshp on 14-Aug-20.
//
#include "WiFiInterface.h"
#include "InterfaceManagement.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>

wifiSetting_t gWifiSetting;

void* Wifi_Connectivity_Thread(void *arg)
{
    int tInterfaceStatus = 0;

    pthread_detach(pthread_self());

    Wifi_setting();
    Wifi_Connect();

    while (1)
    {
        sleep(10);
        if(Is_Interface_Present((char *)"wlan0"))
        {
            tInterfaceStatus = Get_Interface_Status("wlan0");
            if ((tInterfaceStatus & IFF_UP) && (tInterfaceStatus & IFF_RUNNING))
            {
                printf("wlan0 interface is up and running\n");
            }
            else
            {
                printf("wlan0 interface is not present\n");
                Wifi_DisConnect();
                sleep(5);
                Wifi_Connect();
            }
        }
    }

    pthread_exit(NULL);
}

int Wifi_setting(void)
{
    char tString[512] = {0, };

    sprintf(tString, "echo \"wpa-ssid %s\nwpa-psk %s\" > /etc/network/interfaces.d/wlan0",
                            gWifiSetting.mSsid, gWifiSetting.mPassword);
    system(tString);

    return 0;
}


int Wifi_Connect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "nmcli device connect wlan0");
    system(tCommand);

    return 0;
}

int Wifi_DisConnect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "nmcli device disconnect wlan0");
    system(tCommand);

    return 0;
}

