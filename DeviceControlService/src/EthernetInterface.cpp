//
// Created by mukeshp on 20-Aug-20.
//
#include "EthernetInterface.h"
#include "InterfaceManagement.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>

EthernetCnfg_t gEthernetCnfg;

void* Ethernet_Connectivity_Thread(void *arg)
{
    int tInterfaceStatus = 0;

    pthread_detach(pthread_self());

    Ethernet_setting();
    Ethernet_Connect();

    while (1)
    {
        sleep(10);
        if(Is_Interface_Present((char *)"eth0"))
        {
            tInterfaceStatus = Get_Interface_Status("eth0");
            if ((tInterfaceStatus & IFF_UP) && (tInterfaceStatus & IFF_RUNNING))
            {
                printf("eth0 interface is up and running\n");
            }
            else
            {
                printf("eth0 interface is not present\n");
                Ethernet_DisConnect();
                sleep(5);
                Ethernet_Connect();
            }
        }
    }

    pthread_exit(NULL);
}

int Ethernet_setting(void)
{
    char tString[512] = {0, };

    if(0 == gEthernetCnfg.mIsEth0DhcpOrStatic) //Static IP
    {
        sprintf(tString, "echo \"auto eth0\n"
                         "iface eth0 inet static\n"
                         "  address %s\n"
                         "  netmask %s\n"
                         "  gateway %s\" > /etc/network/interfaces.d/eth0",
                gEthernetCnfg.mEth0IpAddr, gEthernetCnfg.mEth0NetMask, gEthernetCnfg.mEth0Gateway);
        system(tString);

        sprintf(tString, "echo \"nameserver %s\nnameserver %s\" > /etc/resolv.conf",
                gEthernetCnfg.mEth0PreferDns, gEthernetCnfg.mEth0AlterDns);
        system(tString);
    } // DHCP IP
    else
    {
        sprintf(tString, "echo \"auto eth0\n"
                         "iface eth0 inet dhcp\" > /etc/network/interfaces.d/eth0");
        system(tString);

    }

    return 0;
}

int Ethernet_Connect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "ifconfig eth0 up");
    system(tCommand);

    return 0;
}

int Ethernet_DisConnect(void)
{
    char tCommand[256] = {0,};

    strcpy(tCommand, "ifconfig eth0 down");
    system(tCommand);

    return 0;
}
