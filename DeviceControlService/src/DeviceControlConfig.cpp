//
// Created by mukeshp on 13-Aug-20.
//
#include "iniparser.h"
#include "DeviceControlConfig.h"
#include "Modem.h"
#include "WiFiInterface.h"
#include "EthernetInterface.h"
#include "SerialInterface.h"
#include <errno.h>

extern ModemCnfg_t gModemCnfg[];
extern ModemPara_t gModemPara;
extern EthernetCnfg_t gNwCnfg;
extern wifiSetting_t gWifiSetting;
extern EthernetCnfg_t gEthernetCnfg;

int IoService_ConfigurationRead(void)
{
    dictionary  *tpIni 		= NULL	;
    char     	*tpIniName 	= NULL 	;
    const char 	*tpName 	= NULL	;
    char 		*tmpPtr 	= NULL	;
    int 		tRetCode 	= -1	;
    char    	tCnfgFileName[512] = {0,} ;
    char        tag[256]    = {0,};

    sprintf(tCnfgFileName,"%s/%s", DEVICE_CONTROL_CONFIG_FILE_PATH, DEVICE_CONTROL_CONFIG_FILE);

    tpIniName = tCnfgFileName ;

    errno = 0;
    tpIni = iniparser_load(tpIniName);
    printf("%s\n", strerror(errno));
    if(tpIni == NULL)
    {
        printf("Error in opening file %s\n", tpIniName);
        return tRetCode;
    }

    do
    {
        tpName = iniparser_getstring((const dictionary *) tpIni, "modem:enable",
                                     (const char *) tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting modem:enable\n");
        }
        else
        {
            gModemPara.mEnable = atoi(tpName);
        }

        tpName = iniparser_getstring((const dictionary*)tpIni,"modem:serialport", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting modem:serialport\n");
        }
        else
        {
            strncpy(gModemPara.mSerialPort, tpName, sizeof(gModemPara.mSerialPort)-1);
        }

        tpName = iniparser_getstring((const dictionary*)tpIni,"modem:SerialBaudRate", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting modem:SerialBaudRate\n");
        }
        else
        {
            gModemPara.mSerialBaudRate = atoi(tpName);
        }

        tpName = iniparser_getstring((const dictionary*)tpIni,"modem:apn", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting modem:apn\n");
        }
        else
        {
            strncpy(gModemPara.mApnName, tpName, sizeof(gModemPara.mApnName)-1);
        }

        tpName = iniparser_getstring((const dictionary*)tpIni,"modem:model1", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting modem:model1\n");
        }
        else
        {
            strncpy(gModemCnfg[0].mModelId, tpName, sizeof(gModemCnfg[0].mModelId)-1);
        }

        tpName = iniparser_getstring((const dictionary*)tpIni,"modem:model2", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting modem:model2\n");
        }
        else
        {
            strncpy(gModemCnfg[1].mModelId, tpName, sizeof(gModemCnfg[1].mModelId)-1);
        }

        tpName = iniparser_getstring((const dictionary*)tpIni,"modem:modemType", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting modem:modemType\n");
        }
        else
        {
            gModemCnfg[0].mModemType = atoi(tpName);
        }

        tpName = iniparser_getstring((const dictionary *) tpIni, "ethernet:enable",
                                     (const char *) tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting ethernet:enable\n");
        }
        else
        {
            gEthernetCnfg.mEnable = atoi(tpName);
        }

        tpName = iniparser_getstring((const dictionary*)tpIni,"ethernet:mode", (const char*)tmpPtr);
        if (tpName == NULL)
        {
            printf("iniparser_getstring Failed For getting ethernet:mode\n");
        }
        else
        {
            gEthernetCnfg.mIsEth0DhcpOrStatic = atoi(tpName);
        }

        if(gEthernetCnfg.mIsEth0DhcpOrStatic == 0)
        {
            tpName = iniparser_getstring((const dictionary *) tpIni, "ethernet:ipaddr",
                                         (const char *) tmpPtr);
            if (tpName == NULL)
            {
                printf("iniparser_getstring Failed For getting ethernet:ipaddr\n");
            }
            else
            {
                strncpy(gEthernetCnfg.mEth0IpAddr, tpName, sizeof(gEthernetCnfg.mEth0IpAddr)-1);
            }

            tpName = iniparser_getstring((const dictionary *) tpIni, "ethernet:netmask",
                                         (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting ethernet:netmask\n");
            }
            else
            {
                strncpy(gEthernetCnfg.mEth0NetMask, tpName, sizeof(gEthernetCnfg.mEth0NetMask)-1);
            }

            tpName = iniparser_getstring((const dictionary *) tpIni, "ethernet:gateway",
                                         (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting ethernet:gateway\n");
            }
            else
            {
                strncpy(gEthernetCnfg.mEth0Gateway, tpName, sizeof(gEthernetCnfg.mEth0Gateway)-1);
            }

            tpName = iniparser_getstring((const dictionary *) tpIni, "ethernet:preferDns",
                                         (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting ethernet:preferDns\n");
            }
            else
            {
                strncpy(gEthernetCnfg.mEth0PreferDns, tpName, sizeof(gEthernetCnfg.mEth0PreferDns)-1);
            }

            tpName = iniparser_getstring((const dictionary *) tpIni, "ethernet:AlterDns",
                                         (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting ethernet:AlterDns\n");
            }
            else
            {
                strncpy(gEthernetCnfg.mEth0AlterDns, tpName, sizeof(gEthernetCnfg.mEth0AlterDns)-1);
            }

            tpName = iniparser_getstring((const dictionary *) tpIni, "wifi:enable",
                                         (const char *) tmpPtr);
            if (tpName == NULL)
            {
                printf("iniparser_getstring Failed For getting wifi:enable\n");
            }
            else
            {
                gWifiSetting.mEnable = atoi(tpName);
            }

            tpName = iniparser_getstring((const dictionary *) tpIni, "wifi:ssid",
                                                 (const char *) tmpPtr);
            if (tpName == NULL)
            {
                printf("iniparser_getstring Failed For getting wifi:ssid\n");
            }
            else
            {
                strncpy(gWifiSetting.mSsid, tpName, sizeof(gWifiSetting.mSsid)-1);
            }

            tpName = iniparser_getstring((const dictionary *) tpIni, "wifi:password",
                                         (const char *) tmpPtr);
            if (tpName == NULL) {
                printf("iniparser_getstring Failed For getting wifi:password\n");
            }
            else
            {
                strncpy(gWifiSetting.mPassword, tpName, sizeof(gWifiSetting.mPassword)-1);
            }
        }

        tRetCode = 0;
    }while (0);

    iniparser_freedict(tpIni);

    printf("Modem STATUS                : %s\n", (gModemPara.mEnable == 1) ? "ENABLE" : "DISABLE");
    printf("Modem Serial Port           : %s\n", gModemPara.mSerialPort);
    printf("Modem Serial BaudRate       : %s\n", GET_BAUDRATE_STRING(gModemPara.mSerialBaudRate));
    printf("Modem APN                   : %s\n", gModemPara.mApnName);
    printf("Modem Model-1               : %s\n", gModemCnfg[0].mModelId);
    printf("Modem Model-2               : %s\n", gModemCnfg[1].mModelId);
    printf("Modem Type                  : %s\n", (gModemCnfg[0].mModemType == MODEM_TYPE_4G) ? "4G" : "2G" );

    printf("Ethernet STATUS             : %s\n", (gEthernetCnfg.mEnable == 1) ? "ENABLE" : "DISABLE");
    printf("Ethernet Mode               : %s\n", (gEthernetCnfg.mIsEth0DhcpOrStatic == 1) ? "DHCP" : "STATIC");
    printf("Ethernet Static IP          : %s\n", gEthernetCnfg.mEth0IpAddr);
    printf("Ethernet Static Mask        : %s\n", gEthernetCnfg.mEth0NetMask);
    printf("Ethernet Static Gateway     : %s\n", gEthernetCnfg.mEth0Gateway);
    printf("Ethernet Static Prefered DNS: %s\n", gEthernetCnfg.mEth0PreferDns);
    printf("Ethernet Static Alternet DNS: %s\n", gEthernetCnfg.mEth0AlterDns);

    printf("WIFI STATUS                 : %s\n", (gWifiSetting.mEnable == 1) ? "ENABLE" : "DISABLE");
    printf("WIFI SSID                   : %s\n", gWifiSetting.mSsid);
    printf("WIFI Password               : %s\n", gWifiSetting.mPassword);

    return tRetCode;
}
