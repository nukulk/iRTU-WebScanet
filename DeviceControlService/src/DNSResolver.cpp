//
// Created by mukeshp on 5/29/2020.
//
#include "DNSResolver.h"
#include "EthernetInterface.h"
#include "InterfaceManagement.h"

extern InterfaceInfo_t gsInterfaceInfo[];
extern EthernetCnfg_t gEthernetCnfg;

void* DnsResolveNotifyThread(void *arg)
{
    static char buffer[EVENT_BUF_LEN];
    int tInitFd = -1;
    int tWatchFd = -1;

    do
    {
        /* creating the INOTIFY instance */
        tInitFd = inotify_init();
        if (tInitFd < 0)
        {
            printf("inotify_init Failed at (%s)errno(%d)(%s)\n", __func__, errno, strerror(errno));
            break;
        }

        /** adding the "/etc" directory into watch list. Here, the suggestion is to validate
          * the existence of the directory before adding into monitoring list.
        * */
        tWatchFd = inotify_add_watch(tInitFd, "/etc", IN_MODIFY | IN_CLOSE_WRITE | IN_MOVED_TO);
        if (tWatchFd < 0)
        {
            printf("inotify_add_watch Failed for /etc directory at (%s)\n", __func__);
            break;
        }

        /* read to determine the event change happens on "/etc" directory.
         * Actually this read blocks until the change event occurs
         * */
        while (1)
        {
            int tLength = 0;
            struct inotify_event *event = NULL;

            printf("Waiting to read inotify event\n");

            tLength = read(tInitFd, buffer, EVENT_BUF_LEN);
            if (tLength < 0)
            {
                printf("inotify read Failed at (%s)\n", __func__);
                break;
            }

            for (int tIndex = 0; tIndex < tLength; tIndex += EVENT_SIZE + event->len)
            {
                event = (struct inotify_event *) &buffer[tIndex];

                if ((event->len) &&
                    ((event->mask & IN_MODIFY) || (event->mask & IN_CLOSE_WRITE) || (event->mask & IN_MOVED_TO)) &&
                    (!(event->mask & IN_ISDIR)) &&
                    (strcmp(event->name, "resolv.conf") == 0)
                    )
                {
                    printf("The file %s was modified with WD %d\n", event->name, event->wd);
                    ResolveUpdate(UPDATE_DNS_ONLY);
                }
            }
        }
    }while (0);

    close(tInitFd);
    tInitFd = -1;
    printf("Exiting from (%s) thread\n", __func__ );
    pthread_exit(NULL);
}

#if 0
void StopResolvMonitorThread(void)
{
    pthread_mutex_lock(&dns_thread_mutex);
    if (isDNSthread) {
        // Check for thread is running
        isDNSthread = false;
        /*removing the "/etc' directory from the watch list.*/
        inotify_rm_watch(init_fd, watch_fd);
        /*closing the INOTIFY instance*/
        close(init_fd);
        if (pthread_join(dnsmanagethread, NULL)<0) {
            isDNSthread = true;
            DebugPriorityLog(NO_LOG, "DNS monitor thread joining get failed");
        } else {
            DebugPriorityLog(NOTIFICATION_LOG, "DNS monitor thread Stopped");
        }
    }
    pthread_mutex_unlock(&dns_thread_mutex);
}
#endif

void UpdateDns(int index)
{
    if ( (index < 0) || (index > MAX_INTERFACE) )
    {
        return;
    }

    if(strcmp(gsInterfaceInfo[index].mIfname, "ppp0") == 0)
    {
        UpdateDnsForPpp0();
    }
    else if(strcmp(gsInterfaceInfo[index].mIfname, "eth0") == 0)
    {
        UpdateDnsForEth0();
    }
    else if(strcmp(gsInterfaceInfo[index].mIfname, "wlan0") == 0)
    {
        UpdateDnsForWlan0();
    }
}

/*
 * function: ReloadActiveRoute
 * Description: reload route for all up and running
 * interface with priority as metric
 */
void UpdateRoute(int index)
{
    unsigned char tIndex;
    unsigned char applied_priority;
    char metricCommand[56] = {0,};

    if((index < 0) || (index > MAX_INTERFACE))
    {
        return;
    }

    // to avoid other interface action overlap
    sleep(1);

    tIndex = index;

    //for (tIndex = 0; tIndex < MAX_INTERFACE; ++tIndex)
    {
        printf("Index-%d at (%s)\n", tIndex, __func__ );

        if(INTERFACE_UP_RUNNING == gsInterfaceInfo[tIndex].mIfStatus)
        {
            printf("Index-%d running at (%s)\n", tIndex, __func__ );

            /* Internet connectivity is available */
            if (true == gsInterfaceInfo[tIndex].mIsInternet)
            {
                applied_priority = gsInterfaceInfo[tIndex].mInfPriority;
            }
            else
            {
                applied_priority = gsInterfaceInfo[tIndex].mInfPriority + 50;
            }

            printf("Interface name-%s applied_priority-%d at %s\n", gsInterfaceInfo[tIndex].mIfname, applied_priority, __func__);

            if (strcmp(gsInterfaceInfo[tIndex].mIfname, "ppp0") == 0)
            {
                sprintf(metricCommand,"/usr/sbin/ifmetric ppp0 %d", applied_priority);
                system(metricCommand);

                ReloadRouteForPpp0((short)applied_priority);
            }
            else if(strcmp(gsInterfaceInfo[tIndex].mIfname, "eth0") == 0)
            {
                sprintf(metricCommand,"/usr/sbin/ifmetric eth0 %d", applied_priority);
                system(metricCommand);

                ReloadRouteForEth0((short)applied_priority);
            }
            else if(strcmp(gsInterfaceInfo[tIndex].mIfname, "wlan0") == 0)
            {
                sprintf(metricCommand,"/usr/sbin/ifmetric wlan0 %d", applied_priority);
                system(metricCommand);

                ReloadRouteForWlan0((short)applied_priority);
            }
        }
    }

#if 0
    for (tIndex = 0; tIndex < MAX_INTERFACE; ++tIndex)
    {
        printf("Index-%d at (%s)\n", tIndex, __func__ );

        if(INTERFACE_UP_RUNNING == gsInterfaceInfo[tIndex].mIfStatus)
        {
            printf("Index-%d running at (%s)\n", tIndex, __func__ );

            /* Internet connectivity is available */
            if (true == gsInterfaceInfo[tIndex].mIsInternet)
            {
                applied_priority = gsInterfaceInfo[tIndex].mInfPriority;
            }
            else
            {
                applied_priority = gsInterfaceInfo[tIndex].mInfPriority + 50;
            }

            printf("Interface name-%s applied_priority-%d at %s\n", gsInterfaceInfo[tIndex].mIfname, applied_priority, __func__);

            if (strcmp(gsInterfaceInfo[tIndex].mIfname, "ppp0") == 0)
            {
                sprintf(metricCommand,"/usr/sbin/ifmetric ppp0 %d", applied_priority);
                system(metricCommand);

                ReloadRouteForPpp0((short)applied_priority);
            }
            else if(strcmp(gsInterfaceInfo[tIndex].mIfname, "eth0") == 0)
            {
                sprintf(metricCommand,"/usr/sbin/ifmetric eth0 %d", applied_priority);
                system(metricCommand);

                ReloadRouteForEth0((short)applied_priority);
            }
            else if(strcmp(gsInterfaceInfo[tIndex].mIfname, "wlan0") == 0)
            {
                sprintf(metricCommand,"/usr/sbin/ifmetric wlan0 %d", applied_priority);
                system(metricCommand);

                ReloadRouteForWlan0((short)applied_priority);
            }
        }
    }
#endif
}

void ResolveUpdate(int iUpdateType)
{
    SelectAndSwitchInternet(UPDATE_DNS_ONLY);

#if 0
    /** @todo: Need to add code for interface priority selection */

    gsInterfaceInfo[0].mIsInternet = 1;
    gsInterfaceInfo[1].mIsInternet = 0;
    gsInterfaceInfo[2].mIsInternet = 0;

    gsInterfaceInfo[0].mIfStatus = INTERFACE_UP_RUNNING;
    gsInterfaceInfo[1].mIfStatus = INTERFACE_UP_RUNNING;
    gsInterfaceInfo[2].mIfStatus = INTERFACE_UP_RUNNING;

    printf("Update DNS of interface %s\n", gsInterfaceInfo[0].mIfname);

    UpdateDns(0);
    UpdateRoute(0);
#endif
}

/**
 *
 *
 */
void UpdateDnsForPpp0(void)
{
    char tPreferDns[16] = {0,};
    char tAlterDns[16] = {0,};
    int tRetVal = -1;

    tRetVal = GetDnsAddrFromPppResolvFile(PPP_DNS_RESOLVE_FILE, tPreferDns, tAlterDns);
    if(tRetVal != 0)
    {
        printf("GetDnsAddrFromPppResolvFile failed at (%s)\n", __func__);
        return;
    }

    tRetVal = WriteResolvFile(tPreferDns, tAlterDns, "ppp0");
    if(tRetVal < 0)
    {
        printf("Failed to write resolv.conf file at (%s)\n", __func__);
        return;
    }

    printf("DNS changed successfully at (%s)\n", __func__);
}

void UpdateDnsForEth0(void)
{
    int ret;
    char predns[24] = {0,};
    char altdns[24] = {0,};

    /* eth0 dhcp mode than here */
    if(1 == gEthernetCnfg.mIsEth0DhcpOrStatic)
    {
        if(GetDNSAddrFromDHCPLeaseFile(ETH0_DHCP_LEASE_FILE, predns, altdns) < 0)
        {
            //DebugPriorityLog(ERROR_LOG, "Get DNS form lease file failed %d->%s", __LINE__, __func__);
            printf("Get DNS form lease file failed %d->%s\n", __LINE__, __func__);
            return;
        }

        ret = WriteResolvFile(predns, altdns, "eth0");
        if (ret != 0)
        {
            //DebugPriorityLog(ERROR_LOG, "Failed to write resolv.conf file %d->%s", __LINE__, __func__);
            printf("Failed to write resolv.conf file %d->%s\n", __LINE__, __func__);
            return;
        }
        else
        {
            printf("DNS changed successfully %d->%s\n", __LINE__, __func__);
        }
    }
    else
    {
        // eth0 in static mode than comes here
        ret = WriteResolvFile(gEthernetCnfg.mEth0PreferDns, gEthernetCnfg.mEth0AlterDns, "eth0");
        if (ret != 0)
        {
            //DebugPriorityLog(ERROR_LOG, "Failed to write resolv.conf file %d->%s", __LINE__, __func__);
            printf("Failed to write resolv.conf file %d->%s\n", __LINE__, __func__);
            return;
        }
        else
        {
            //DebugPriorityLog(NOTIFICATION_LOG, "DNS changed successfully %d->%s", __LINE__, __func__);
            printf("DNS changed successfully %d->%s\n", __LINE__, __func__);
        }
    }
}

void UpdateDnsForWlan0(void)
{
    int ret;
    char predns[24] = {0,};
    char altdns[24] = {0,};

    if (GetDNSAddrFromDHCPLeaseFile(WLAN0_DHCP_LEASE_FILE, predns, altdns) != 0)
    {
        //DebugPriorityLog(ERROR_LOG, "Get DNS form lease file failed %d->%s", __LINE__, __func__);
        printf("Get DNS form lease file failed %d->%s\n", __LINE__, __func__);
        return;
    }

    ret = WriteResolvFile(predns, altdns, "wlan0");
    if (ret != 0)
    {
        //DebugPriorityLog(ERROR_LOG, "Failed to write resolv.conf file %d->%s", __LINE__, __func__);
        printf("Failed to write resolv.conf file %d->%s\n", __LINE__, __func__);
        return;
    }
    else
    {
        //DebugPriorityLog(NOTIFICATION_LOG, "DNS changed successfully %d->%s", __LINE__, __func__);
        printf("DNS changed successfully %d->%s\n", __LINE__, __func__);
    }
}

void ReloadRouteForPpp0(short metric)
{
    // only one mode here for ppp0
    if (0 != SetDefaultGateway((char*)"ppp0", NULL, metric))
    {
        //DebugPriorityLog(ERROR_LOG, "ppp0 Route added get failed %d->%s", __LINE__, __func__);
        printf("ppp0 Route added get failed %d->%s\n", __LINE__, __func__);
        return;
    }
    else
    {
        //DebugPriorityLog(NOTIFICATION_LOG, "ppp0 Route added successfully %d->%s", __LINE__, __func__);
        printf("ppp0 Route added successfully %d->%s\n", __LINE__, __func__);
    }
}

void ReloadRouteForEth0(short metric)
{
    char route[24] = {0,};
    bzero(route, sizeof(route));

    if (0 != GetRouteEth0(route))
    {
        return;
    }

    if (0 != SetDefaultGateway((char*)"eth0", (const char*)route, metric))
    {
        printf("eth0 Route added get failed %s %d->%s\n", route, __LINE__, __func__);
        return;
    }
    else
    {
        printf("eth0 Route added successfully %d->%s\n", __LINE__, __func__);
    }
}

void ReloadRouteForWlan0(short metric)
{
    char route[16] = {0,};

    if (0 != GetRouteWlan0(route))
    {
        return;
    }

    if (0 != SetDefaultGateway((char*)"wlan0", route, metric))
    {
        //DebugPriorityLog(ERROR_LOG, "wlan0 Route added get failed %s %d->%s", route, __LINE__, __func__);
        printf("wlan0 Route added get failed %s %d->%s\n", route, __LINE__, __func__);
        return;
    }
    else
    {
        //DebugPriorityLog(NOTIFICATION_LOG, "wlan0 Route added successfully %d->%s", __LINE__, __func__);
        printf("wlan0 Route added successfully %d->%s\n", __LINE__, __func__);
    }
}

int GetRouteEth0(char *route)
{
    if(NULL == route)
    {
        return -1;
    }

    /* eth0 dhcp mode than here */
    if(1 == gEthernetCnfg.mIsEth0DhcpOrStatic)
    {
        if(GetRouteAddrFromDHCPLeaseFile(ETH0_DHCP_LEASE_FILE, route) < 0)
        {
            //DebugPriorityLog(ERROR_LOG, "Get route form lease file failed %d->%s", __LINE__, __func__);
            printf("Get route form lease file failed %d->%s\n", __LINE__, __func__);
            return -1;
        }
    }
    /* eth0 in static mode than comes here */
    else
    {
        strcpy(route, gEthernetCnfg.mEth0Gateway);
    }

    return 0;
}

int GetRouteWlan0(char *route)
{
    if(NULL == route)
    {
        return -1;
    }

    if (GetRouteAddrFromDHCPLeaseFile(WLAN0_DHCP_LEASE_FILE, route) < 0)
    {
        //DebugPriorityLog(ERROR_LOG, "Get route form lease file failed %d->%s", __LINE__, __func__);
        printf("Get route form lease file failed %d->%s\n", __LINE__, __func__);
        return -1;
    }

    return 0;
}

bool SetDefaultGateway(char *ifname, const char *defGateway, short metric)
{
    int sockfd, err;
    struct rtentry route = {0,};
    int tRetVal = -1;
    unsigned char numretry = 0;
    short flags = 0;

    if(NULL == ifname)
    {
        printf("argument are null in %s function for %s interface\n", __func__, ifname);
        return tRetVal;
    }

    /** Check whether interface is up or not */
    if(GetInterfaceFlags(ifname, &flags) < 0)
    {
        // if interface invalid than comes here
        printf("Get Interface flag failed at %s\n", __func__ );
        return tRetVal;
    }

    if(!(flags & IFF_UP))
    {
        // if interface down than comes here
        return tRetVal;
    }

    ((struct sockaddr_in*)&route.rt_gateway)->sin_family = AF_INET;
    //route.rt_gateway.sin_family = AF_INET;

    if (NULL != defGateway)
    {
        (( struct sockaddr_in*)&route.rt_gateway)->sin_addr.s_addr = inet_addr(defGateway);
        //route.rt_gateway.sin_addr.s_addr = inet_addr(defGateway);
    }
    else
    {
        (( struct sockaddr_in*)&route.rt_gateway)->sin_addr.s_addr = 0;
        //route.rt_gateway.sin_addr.s_addr = 0;
    }

    (( struct sockaddr_in*)&route.rt_gateway)->sin_port = 0;
    //route.rt_gateway.sin_port = 0;

    (( struct sockaddr_in*)&route.rt_dst)->sin_family = AF_INET;
    (( struct sockaddr_in*)&route.rt_dst)->sin_addr.s_addr = 0;
    (( struct sockaddr_in*)&route.rt_dst)->sin_port = 0;

    //route.rt_dst.sin_family = AF_INET;
    //route.rt_dst.sin_addr.s_addr = 0;
    //route.rt_dst.sin_port = 0;

    (( struct sockaddr_in*)&route.rt_genmask)->sin_family = AF_INET;
    (( struct sockaddr_in*)&route.rt_genmask)->sin_addr.s_addr = 0;
    (( struct sockaddr_in*)&route.rt_genmask)->sin_port = 0;

    //route.rt_genmask.sin_family = AF_INET;
    //route.rt_genmask.sin_addr.s_addr = 0;
    //route.rt_genmask.sin_port = 0;

    if(NULL != defGateway)
    {
        route.rt_flags = RTF_UP | RTF_GATEWAY;
    }
    else
    {
        route.rt_flags = RTF_UP;
    }

    route.rt_metric = metric+1;
    route.rt_dev = ifname;

    numretry = 0;
    do
    {
        if(numretry > 3)
        {
            //isFailed = true;
            tRetVal = -1;
            //logger(ERROR_LOG, "route write retry failed more than 3 times. exiting function");
            printf("route write retry failed more than 3 times. exiting function\n");
            break;
        }
        ++numretry;

        //logger(NOTIFICATION_LOG, "Route add retry : %d", numretry);
        printf("Route add retry : %d\n", numretry);

        /*AF_INET - to define network interface IPv4*/
        /*Creating socket for it.*/
        sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (sockfd == -1)
        {
            //logger(ERROR_LOG, "socket is -1, %d : %s", errno, strerror(errno));
            printf("socket is -1, %d : %s\n", errno, strerror(errno));
            sleep(1);
            continue;
        }

        errno = 0;
        if ((err = ioctl(sockfd, SIOCADDRT, &route)) < 0)
        {
            close(sockfd);
            if (errno!=17)
            {
                // file not already exists and failed than only retry continue
                //logger(ERROR_LOG, "SIOCADDRT route set failed , ret->%d for %s interface, %d : %s", err, ifname, errno, strerror(errno));
                printf("SIOCADDRT route set failed , ret->%d for %s interface, %d : %s\n", err, ifname, errno, strerror(errno));
                sleep(1);
                continue;
            }
        }

        /*closing fd*/
        close(sockfd);
        tRetVal = 0;
        break;

    } while(1);

    return (tRetVal);
}

bool DelDefaultGateway(char *ifname)
{
    int sockfd, err;
    struct rtentry route;
    bool isFailed = true;
    unsigned char numretry = 0;

    if (ifname != NULL)
    {
        short flags = 0;

        flags = Get_Interface_Status(ifname);
        if (flags < 0)
        {
            // if interface invalid than comes here
            return (!isFailed);
        }

        if (!(flags & IFF_UP))
        {
            // if interface down than comes here
            return (!isFailed);
        }
    }

    memset(&route, 0, sizeof(route));

    (( struct sockaddr_in*)&route.rt_gateway)->sin_family = AF_INET;
    (( struct sockaddr_in*)&route.rt_gateway)->sin_addr.s_addr = 0;
    (( struct sockaddr_in*)&route.rt_gateway)->sin_port = 0;

    (( struct sockaddr_in*)&route.rt_dst)->sin_family = AF_INET;
    (( struct sockaddr_in*)&route.rt_dst)->sin_addr.s_addr = 0;
    (( struct sockaddr_in*)&route.rt_dst)->sin_port = 0;

    (( struct sockaddr_in*)&route.rt_genmask)->sin_family = AF_INET;
    (( struct sockaddr_in*)&route.rt_genmask)->sin_addr.s_addr = 0;
    (( struct sockaddr_in*)&route.rt_genmask)->sin_port = 0;

    route.rt_flags = RTF_UP;
    if (ifname != NULL)
    {
        printf("deleting default route for %s interface\n", ifname);
        route.rt_dev = ifname;
    }
    else
    {
        printf("deleting all default routes\n");
    }

    do
    {
        if(numretry > 3)
        {
            isFailed = true;
            printf("route deleted more than 3 times. exiting function\n");
            break;
        }
        numretry++;

        /*AF_INET - to define network interface IPv4*/
        /*Creating soket for it.*/
        sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (sockfd == -1)
        {
            printf("socket is -1, %d : %s\n", errno, strerror(errno));
            sleep(1);
            continue;
        }

        if ((err = ioctl(sockfd, SIOCDELRT, &route)) < 0)
        {
            printf("SIOCDELRT route delete failed , ret->%d for %s interface, %d : %s\n", err, ifname!=NULL?ifname:"default top", errno, strerror(errno));
            if (errno != 3)
            {
                printf("all route of %s interface deleted\n", ifname!=NULL?ifname:"default top");
            }
        }
        else
        {
            // if more than one route than continue in loop
            close(sockfd);
            numretry = 0;
            continue;
        }
        /*closing fd*/
        close(sockfd);
        isFailed = false;
    } while(isFailed);

    return (!isFailed);
}

int GetInterfaceFlags(char *ifname, short *ifrflag)
{
    struct ifreq ifr = {0,};
    int sockfd = -1;

    if((NULL == ifname) || (NULL == ifrflag))
    {
        //logger(ERROR_LOG, "argument is null in %s function for %s interface", __func__, ifname);
        printf("argument is null in %s function for %s interface\n", __func__, ifname);
        return -1;
    }

    /*AF_INET - to define network interface IPv4*/
    /*Creating soket for it.*/
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0)
    {
        //logger(ERROR_LOG, "failed to open socket %d->%s, %d->%s", __LINE__, ifname, errno, (char*)strerror(errno));
        printf("failed to open socket %d->%s, %d->%s\n", __LINE__, ifname, errno, (char*)strerror(errno));
        return -1;
    }

    /*AF_INET - to define IPv4 Address type.*/
    ifr.ifr_addr.sa_family = AF_INET;

    /*interface - define the ifr_name - port name
    where network attached.*/
    memcpy(ifr.ifr_name, ifname, IFNAMSIZ-1);

    /*Accessing network interface information by
    passing address using ioctl.*/
    if (ioctl(sockfd, SIOCGIFFLAGS, &ifr)<0)
    {
        //logger(ERROR_LOG, "SIOCGIFFLAGS flag get failed for %s interface, %d : %s", ifname, errno, strerror(errno));
        printf("SIOCGIFFLAGS flag get failed for %s interface, %d : %s\n", ifname, errno, strerror(errno));
        close(sockfd);
        return -1;
    }

    /*closing sockfd*/
    close(sockfd);
    *ifrflag = ifr.ifr_flags;

    return 0;
}

/**
 * Its return preferred DNS and alternate DNS ip address from pFIle.
 * @param pFile
 * @param pPredns
 * @param pAltdns
 * @return : zero on success, else -1.
 */
int GetDnsAddrFromPppResolvFile(const char *pFile, char *pPredns, char *pAltdns)
{
    FILE *tFp = NULL;
    unsigned int tIndex = 0;
    char tReadBuf[256] = {0,};

    if ((NULL == pFile) || (NULL == pPredns) || (NULL == pAltdns))
    {
        // invalid pointer
        printf(/* ERROR_LOG, */ "pFile - null, pPredns - null or pAltdns - null at (%s)", __func__);
        return -1;
    }

    /* check given file present or not? */
    tFp = fopen(pFile, "r");
    if (NULL == tFp) {
        printf(/* ERROR_LOG,*/ "Failed to open (%s) file at (%s)", pFile, __func__);
        return -1;
    }

    while (fgets(tReadBuf, sizeof(tReadBuf), tFp))
    {
        if (tReadBuf[0] != '\0')
        {
            char *pRet = NULL;
            char *token = NULL;

            if ((pRet = strstr(tReadBuf, "nameserver ")))
            {
                /* needle string present in haystack string */
                pRet += strlen("nameserver ");
                token = strtok(pRet, "\n");
                switch (tIndex)
                {
                    case 0:
                        strcpy(pPredns, token);
                        break;
                    case 1:
                        strcpy(pAltdns, token);
                        break;
                    default:
                        break;
                }
                ++tIndex;
            }
        }
    }

    fclose(tFp);

    return 0;
}

int WriteResolvFile(const char *PreDNS, const char *AltDNS,const char *ifname)
{
    unsigned char tRetryCnt = 0;
    int ret = -1;
    bool isPreDNSverified, isAltDNSVerified;
    FILE *tResolvFd = NULL;

    do
    {
        if(tRetryCnt > DNS_FILE_WRITE_RETRY_CNT)
        {
            ret = -1;
            //DebugPriorityLog(ERROR_LOG, "resolv.conf write retry failed more than 3 times. exiting function");
            printf("/etc/resolv.conf file write retry failed > (%d) times in (%s)", tRetryCnt, __func__ );
            break;
        }
        ++tRetryCnt;

        /* @todo: If /etc/resolvConf file not present than what, need to verify */
        tResolvFd = fopen(DNS_RESOLVE_FILE, "r");
        if (tResolvFd != NULL)
        {
            /* Get the number of bytes */
            fseek(tResolvFd, 0, SEEK_END);
            long fsize = ftell(tResolvFd);

            /* reset the file position indicator to the beginning of the file */
            rewind(tResolvFd);

            /* grab sufficient memory for the buffer to hold the text */
            char *buffer = (char*)malloc(fsize + 1);
            if(buffer == NULL)
            {
                //DebugPriorityLog(ERROR_LOG, "Error in malloc %d : %s",__LINE__, strerror(errno));
                printf("Error in malloc %d : %s",__LINE__, strerror(errno));
                sleep(1);
                fclose(tResolvFd);
                continue;
            }
            /* copy all the text into the buffer */
            fread(buffer, 1, fsize, tResolvFd);
            fclose(tResolvFd);

            buffer[fsize] = 0;
            isPreDNSverified = false;
            isAltDNSVerified = false;

            if ( (strlen(PreDNS) > 0) && (PreDNS != NULL) )
            {
                if (strstr(buffer, PreDNS))
                {
                    // resolv.conf Write successfully with PreDNS
                    isPreDNSverified = true;
                }
            }
            else
            {
                isPreDNSverified = true;
            }

            if ( (strlen(AltDNS) > 0) && (AltDNS != NULL) )
            {
                if (strstr(buffer, AltDNS))
                {
                    // resolv.conf Write successfully with AltDNS
                    isAltDNSVerified = true;
                }
            }
            else
            {
                isAltDNSVerified = true;
            }

            if (isPreDNSverified && isAltDNSVerified)
            {
                // no need to write resolv conf
                ret = 0;
                /* free the memory we used for the buffer */
                free(buffer);
                break;
            }

            //DebugPriorityLog(NOTIFICATION_LOG, "Old resolv.conf content: %s", buffer);
            /* free the memory we used for the buffer */
            free(buffer);

            // read check failed than come here to write resolv.conf
            //DebugPriorityLog(ALL_LOG, "resolv.conf write retry number : %d", numretry);
            usleep(100000);

            //pthread_mutex_lock(&resolv_write_lock);

            tResolvFd = fopen(DNS_RESOLVE_FILE, "w+");
            if(tResolvFd != NULL)
            {
                fprintf(tResolvFd, "# Domain Name Servers Updated by iRTU_io_service for (%s) interface\n", ifname);

                if( (strlen(PreDNS) > 0) && (PreDNS != NULL) )
                {
                    fprintf(tResolvFd, "nameserver %s\n", PreDNS);
                }
                if ( (strlen(AltDNS) > 0) && (AltDNS != NULL) )
                {
                    fprintf(tResolvFd, "nameserver %s\n", AltDNS);
                }

                fclose(tResolvFd);

                //DebugPriorityLog(NOTIFICATION_LOG, "New DNS: %s %s", PreDNS, AltDNS);
                //DebugPriorityLog(ALL_LOG, "resolv.conf write successfully");

                printf("New DNS: %s %s", PreDNS, AltDNS);
                printf("resolv.conf write successfully");

                ret = 0;
            }
            else
            {
                //DebugPriorityLog(ERROR_LOG, "Failed to open resolv.conf file for write %d : %s",__LINE__, strerror(errno));
                printf(/*ERROR_LOG,*/ "Failed to open (%s) file for write at (%s)\n", DNS_RESOLVE_FILE, __func__ );
            }

            //pthread_mutex_unlock(&resolv_write_lock);
        }
        else
        {
            //DebugPriorityLog(ERROR_LOG, "Error in DNS Read %d:%s", __LINE__, strerror(errno));
            printf("Error in DNS Read %d:%s", __LINE__, strerror(errno));
        }

        if(ret < 0)
        {
            /* if failed than no immidiate retry, Hold and than retry */
            //DebugPriorityLog(ERROR_LOG, "resolv.conf write retry number %d failed", numretry);
            printf("(%s) file write retry count (%d) failed\n", DNS_RESOLVE_FILE, tRetryCnt);
            sleep(1);
        }
    } while (ret < 0);

    return ret;
}

int GetRouteAddrFromDHCPLeaseFile(const char *file, char *gw_addr)
{
    FILE *fp = NULL;
    char read_buf[256] = {0,};

    if((NULL == file) || (NULL == gw_addr))
    {
        // invalid pointer
        //logger(ERROR_LOG, "Null pointer in argument %d->%s", __LINE__, __func__);
        printf("Null pointer in argument %d->%s", __LINE__, __func__);
        return -1;
    }

    // check given file present or not?
    fp = fopen(file, "r");
    if (NULL == fp)
    {
        //logger(ERROR_LOG, "Failed to open lease file %d->%s, %d->%s", __LINE__, __func__, errno, (char*)strerror(errno));
        printf("Failed to open lease file %d->%s, %d->%s", __LINE__, __func__, errno, (char*)strerror(errno));
        return -1;
    }
    else
    {
        while (fgets(read_buf, sizeof(read_buf), fp))
        {
            if (read_buf[0] != '\0')
            {
                char *ret = NULL;
                if ((ret = strstr(read_buf, "option routers ")))
                {
                    // needle string present in haystack string
                    ret += sizeof("option routers ")-1;
                    char *token = strtok(ret,";");
                    strcpy(gw_addr, token);
                    // for taking last entry from lease file
                }
            }
        }

        fclose(fp);
    }
    return 0;
}

int GetDNSAddrFromDHCPLeaseFile(const char *file, char *predns, char *altdns)
{
    FILE *fp = NULL;
    char read_buf[256] = "";

    if ((NULL == file) || (NULL == predns) || (NULL == altdns))
    {
        // invalid pointer
        //logger(ERROR_LOG, "Null pointer in argument %d->%s", __LINE__, __func__);
        printf("Null pointer in argument %d->%s", __LINE__, __func__);
        return -1;
    }

    // check given file present or not?
    fp = fopen(file, "r");
    if(NULL == fp)
    {
        //logger(ERROR_LOG, "Failed to open lease file %d->%s, %d->%s", __LINE__, __func__, errno, (char*)strerror(errno));
        printf("Failed to open lease file %d->%s, %d->%s", __LINE__, __func__, errno, (char*)strerror(errno));
        return -1;
    }
    else
    {
        while (fgets(read_buf, sizeof(read_buf), fp)) {
            if (read_buf[0] != '\0') {
                char * ret = NULL;
                if ((ret = strstr(read_buf, "option domain-name-servers "))) {
                    // needle string present in haystack string
                    ret += sizeof("option domain-name-servers ")-1;
                    strtok(ret, ";");
                    char *token = strtok(ret, ",");
                    uint8_t i = 0;
                    /* walk through other tokens */
                    while( token != NULL ) {
                        switch (i) {
                            case 0:
                                strcpy(predns, token);
                                break;
                            case 1:
                                strcpy(altdns, token);
                                break;
                            default:
                                break;
                        }
                        i++;
                        token = strtok(NULL, ",");
                    }
                }
            }
        }
        fclose(fp);
    }
    return 0;
}

