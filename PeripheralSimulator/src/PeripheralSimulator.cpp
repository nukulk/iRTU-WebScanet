#include "PeripheralSimulator.hpp"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <MessageBus/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>

#include <iostream>

static void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void OnSignalReceived(SignalType type);

static PeripheralSimulator *appptr = nullptr;

PeripheralSimulator::PeripheralSimulator()
{
    message_bus = nullptr;
    logger = nullptr;
    appptr = this;
}

PeripheralSimulator::~PeripheralSimulator()
{

}

bool PeripheralSimulator::Initialize()
{
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        return false;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == nullptr)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    std::string value;
    configuration_get_value_as_string(config, "default", "destination", value);
    str_ex_split_with_char(value, destination_list, ',');

    std::vector<std::string> payloads;
    configuration_get_all_keys(config, "payloads", payloads);

    for(auto domain_name : payloads)
    {
        configuration_get_value_as_string(config, "payloads", domain_name, value);
        domain_payloads[domain_name] = value;
    }

    configuration_release(config);

    return true;
}

bool PeripheralSimulator::Destroy()
{
    return true;
}

bool PeripheralSimulator::Start()
{
    if(!message_bus_initialize(&message_bus, OnNetworkEvent))
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    bool continue_loop = true;

    while(continue_loop)
    {
        sleep(30);

        for(auto destination : destination_list)
        {
            for(auto kv: domain_payloads)
            {
                if(!message_bus_send(message_bus, destination.c_str(), Data, UserData, Text, kv.second.c_str(), kv.second.length()))
                {
                    continue_loop = false;
                    break;
                }
            }
        }
    }

    return true;
}

bool PeripheralSimulator::Restart()
{
    return false;
}

bool PeripheralSimulator::Stop()
{
    message_bus_close(message_bus);
    message_bus_release(message_bus);
    destination_list.clear();
    logger_release(logger);

    return false;
}

void *PeripheralSimulator::GetLogger()
{
    return logger;
}

void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    std::string str;

    if(mtype == NodeOnline)
    {
        str += messagebuffer;
        str += " is online";
        WriteInformation(appptr->GetLogger(), str);
        return;
    }

    if(mtype == NodeOffline)
    {
        str += messagebuffer;
        str += " is offline";
        WriteInformation(appptr->GetLogger(), str);
        return;
    }

    if(mtype == LoopBack)
    {
        WriteInformation(appptr->GetLogger(),"Loopback test successfull\n");
        return;
    }

    WriteInformation(appptr->GetLogger(),messagebuffer);
}

void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(appptr->GetLogger(), "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(appptr->GetLogger(), "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(appptr->GetLogger(), "SHUTDOWN SIGNAL", LOG_CRITICAL);
            appptr->Stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(appptr->GetLogger(), "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(appptr->GetLogger(), "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(appptr->GetLogger(), "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(appptr->GetLogger(), "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(appptr->GetLogger(), "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
