#ifndef PERIPHERAL_SIMULATOR
#define PERIPHERAL_SIMULATOR

#include <stdbool.h>
#include <string>
#include <vector>
#include <map>

class PeripheralSimulator
{
public:
    PeripheralSimulator();
    virtual ~PeripheralSimulator();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    void* GetLogger();
private:
    void* message_bus;
    void* logger;
    void* config;
    std::vector<std::string> destination_list;
    std::map<std::string, std::string> domain_payloads;
};

#endif
