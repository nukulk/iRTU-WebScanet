#include "WiFiService.hpp"

int main(int argc, char* argv[])
{
    WiFiService wifi;

    if(!wifi.Initialize())
    {
        return -1;
    }

    wifi.Start();

    return 0;
}
