cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(Project WiFiService)
project(${Project})

include_directories(./include/ /include/ /usr/include/ /usr/local/include/)
link_directories(/lib /usr/lib /usr/local/lib)
link_libraries(rt pthread dl rtucore messagebus)

set(SOURCE
${SOURCE}
./src/WiFiService.cpp
./src/main.cpp
)

set(HEADERS
${HEADERS}
./include/WiFiService.hpp
)

add_executable(service_wifi ${SOURCE} ${HEADERS})
