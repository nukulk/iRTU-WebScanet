#ifndef WIFI_SERVICE
#define WIFI_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>

class WiFiService
{
public:
    WiFiService();
    virtual ~WiFiService();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    void* GetLogger();
private:
    void* config;
    void* message_bus;
    void* logger;
    std::vector<std::string> destination_list;
};

#endif
