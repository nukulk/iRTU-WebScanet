#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define MAX_CPU_BUFF_DATA_LEN 255
#define MAX_CPU_TIME_LEN 10

//https://stackoverflow.com/questions/3017162/how-to-get-total-cpu-usage-in-linux-using-c

//Add all values cpu  132140 624 28990 1991476 23293 0 4427 0 0 0
int GetCpuTotalTime()
{
    char str[255] = {0};
    const char d[2] = " ";
    char* token;
    long int sum = 0;
    FILE *fstat = fopen("/proc/stat", "r");
    if (fstat == NULL) {
        printf("%d: %s --> Failed to open %s\n", __LINE__, __func__,"/proc/stat");
        return -1;
    }
    fgets(str,100,fstat);
    //printf("str = %s\n",str);
    fclose(fstat);
    token = strtok(str,d);
    while(token!=NULL)
    {
      token = strtok(NULL,d);
      if(token!=NULL)
      {
        sum += atoi(token);
        //printf("token = %s sum = %d\n",token,sum);
      }
    }
    return sum;
}

//read first 3 values of cpu  132140 624 28990 1991476 23293 0 4427 0 0 0
int ReadFirstThreeValue()
{
    int i = 1;
    char str[255] = {0};
    const char d[2] = " ";
    char* token;
    long int thardsum = 0;
    FILE *fstat = fopen("/proc/stat", "r");
    if (fstat == NULL) {
        printf("%d: %s --> Failed to open %s\n", __LINE__, __func__,"/proc/stat");
        return -1;
    }
    fgets(str,100,fstat);
    //printf("str = %s\n",str);
    fclose(fstat);
    token = strtok(str,d);
    while(token!=NULL)
    {
      token = strtok(NULL,d);
      if(token!=NULL)
      {
        thardsum += atoi(token);
        //printf("2-token = %s thardsum = %d\n",token,thardsum);
        if(i == 3)
        {
          return thardsum;
        }
      }
      i++;
    }
    return -1;
}

float AvgCpuUses()
{
    long int sum = 0, idle, lastSum = 0,lastIdle = 0, dsum = 0, didle = 0;
    lastSum = GetCpuTotalTime();
    lastIdle = ReadFirstThreeValue();
    //printf("1-cpu uses = %f\n",((float)lastIdle/lastSum)*100);
    sleep(1);
    sum = GetCpuTotalTime();
    idle = ReadFirstThreeValue();
    //printf("idle = %d lastIdle = %d sum = %d lastSum = %d\n",idle,lastIdle,sum,lastSum);
    dsum = (sum-lastSum);
    didle = (idle-lastIdle);
    //printf("didle = %d\n",didle);
    //printf("dSum = %d\n",dsum);
    //printf("devide = %f\n",(float)didle/dsum);
    return (((float)didle/dsum)*100);
}

void FanOn()
{
  char commands[100];
  sprintf((char *)commands,"echo \"1\" > /sys/class/gpio/gpio112/value");
  system((const char *)commands);
}

void FanOff()
{
    char commands[100];
    sprintf((char *)commands,"echo \"0\" > /sys/class/gpio/gpio112/value");
    system((const char *)commands);
}

void initFan()
{
  int gpiofd;
  char commands[100];
    gpiofd = open("/sys/class/gpio/gpio112/direction", O_RDWR);
    if (gpiofd < 0)
    {
        system("echo \'112\' > /sys/class/gpio/export");
    }
    close(gpiofd);
  sprintf((char *)commands,"echo \"out\" > /sys/class/gpio/gpio112/direction");
  system((const char *)commands);

}

int ReadTempFromConfigFile(char* configfile)
{
    char buf[20] = {0};
    int status = -1;
    int fd = -1;
    fd = open(configfile, O_RDONLY);
    if (fd < 0)
    {
      printf("failed func = %s line = %d\n",__func__,__LINE__);
      return 50;
    }
    memset(buf,0,sizeof(buf));
    status = read(fd, buf, 20);
    if(status < 0)
    {
      printf("failed func = %s line = %d status %d\n",__func__,__LINE__,status);
      return 50;
    }
    //printf("func = %s line = %d status %d buf = %s\n",__func__,__LINE__,status,buf);
    close(fd);
    return atoi(buf);
}

void WriteeDataInFile(int temp, float cpu, int fan)
{
    FILE * fp;
    time_t rawtime;
    struct tm *info;

    fp = fopen ("log.txt", "a+");
    if(fp == NULL)
    {
      printf("func = %s line = %d fp %d\n",__func__,__LINE__,fp);
    }
    time( &rawtime );
    info = localtime( &rawtime );
    fprintf(fp, "Temp : %d CpuUses : %f FanStatus %d Time :%s", temp,cpu,fan,asctime(info));
    printf("Temp : %d CpuUses : %f FanStatus %d Time :%s", temp,cpu,fan,asctime(info));
    fclose(fp);
}

int ReadCpuDieTemp()
{
    char buf[20] = {0};
    int status = -1;
    int fd = -1;
    fd = open("/sys/devices/virtual/thermal/thermal_zone0/temp", O_RDONLY);
    if (fd < 0)
    {
      printf("func = %s line = %d fd %d\n",__func__,__LINE__,fd);
      return -1;
    }
    memset(buf,0,sizeof(buf));
    status = read(fd, buf, 20);
    if(status < 0)
    {
      printf("func = %s line = %d status %d\n",__func__,__LINE__,status);
      return -1;
    }
    //printf("func = %s line = %d status %d buf = %s\n",__func__,__LINE__,status,buf);
    close(fd);
    return atoi(buf);
}

int main()
{
  int configtemp = -1;
  int cpudietemp = -1;
  float CpuUses = -1;
  int fanstatus = 0;
  initFan();
  configtemp = ReadTempFromConfigFile("tempconfig.txt");
  //printf("func = %s line = %d configtemp %d\n",__func__,__LINE__,configtemp);
  while(1)
  {
    cpudietemp = ReadCpuDieTemp();
    CpuUses = AvgCpuUses();
    //printf("2-CpuUses == %f\n",CpuUses);
    if((int)(cpudietemp/1000) > configtemp)
    {
      FanOn(); fanstatus = 1;
    }
    if((int)(cpudietemp/1000) < 45)
    {
      FanOff(); fanstatus = 0;
    }
    WriteeDataInFile(cpudietemp,CpuUses,fanstatus);
    sleep(2);
  }
}
