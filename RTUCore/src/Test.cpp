#include <iostream>
#include "Logger.h"
#include "SignalHandler.h"
#include "StringEx.h"
#include "Directory.h"
#include "Configuration.h"

static void OnSignalReceived(SignalType stype);
static void* config = nullptr;
static void* logger = nullptr;

class Test
{
public:
    Test();
    static void TestFunction()
    {
        WriteInformation(logger, "C++ class function name normalization test");
    }
};

static void TestFunction()
{
    WriteInformation(logger, "C function name normalization test");
}

int main(int, char**)
{
    printf("Allocating logger\n");
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        printf("*** ERROR could not allocate logger\n");
        return -1;
    }

    printf("Setting signal handler\n");
    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    printf("Testing function name handling\n");
    Test::TestFunction();
    TestFunction();

    printf("Allocating configuration\n");
    config = configuration_allocate_default();

    if(config == nullptr)
    {
        printf("*** ERROR could not allocate configuration\n");
        printf("Releasing logger\n");
        logger_release(logger);
        return -1;
    }

    std::vector<std::string> sections;
    std::vector<std::string> keys;
    std::string value;

    printf("Reading all configuration sections\n");
    configuration_get_all_sections(config, sections);

    for(auto section : sections)
    {
        WriteInformation(logger, section);
        printf("Reading all keys of configuration section %s\n", section.c_str());
        configuration_get_all_keys(config, section, keys);

        for(auto key : keys)
        {
            WriteInformation(logger, key);
            configuration_get_value_as_string(config, section, key, value);
            printf("%s => %s\n", key.c_str(), value.c_str());
            WriteInformation(logger, value);
        }
    }

    configuration_release(config);
    printf("Releasing configuration\n");
    logger_release(logger);
    printf("Releasing logger\n");

    return 0;
}

static void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_WARNING);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_WARNING);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            logger_release(logger);
            exit(0);
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_WARNING);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_WARNING);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_WARNING);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_WARNING);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "TERMINAL WINDOW RESIZED SIGNAL", LOG_WARNING);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_WARNING);
            break;
        }
    }

}
