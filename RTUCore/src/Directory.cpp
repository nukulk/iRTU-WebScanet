/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#include "Directory.h"
#include "StringEx.h"

#include <vector>
#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <fstream>

bool dir_is_directory_exists(const std::string &str)
{
    DIR *dirp;

    dirp = opendir(str.c_str());

    if(dirp == nullptr)
    {
        closedir(dirp);
        return false;
    }

    closedir(dirp);
    return true;
}

void dir_get_parent_directory(const std::string &filename, std::string &str)
{
    str = filename;

    size_t ctr = str.length() - 1;

    while(true)
    {
        if(str[ctr]== '/')
        {
            break;
        }

        str[ctr] = 0;
        ctr--;
    }
}

void dir_create_directory(const std::string &str)
{
    mkdir(str.c_str(), S_IRWXU);
}

bool dir_is_file_exists(const std::string &filename)
{
    bool ret = false;

    std::ifstream cfgfile(filename);

    if(cfgfile.is_open())
    {
        ret = true;;
    }

    cfgfile.close();

    return ret;
}

void dir_get_file_extension(const std::string &file_name, std::string &str)
{
    std::string base_name;

    str_ex_keyvalue_with_char(file_name, base_name, str, '.');

    return;
}

void dir_get_file_name(const std::string &path, std::string &str)
{
    std::vector<std::string> directory_tokens;

    str_ex_split_with_char(path, directory_tokens, '/');

    if(directory_tokens.size() > 0)
    {
        str = directory_tokens[directory_tokens.size() - 1].c_str();
    }
    else
    {
        str = path;
    }

    return;
}

void  dir_get_file_basename(const std::string &file_name, std::string &str)
{
    std::string extension;

    str_ex_keyvalue_with_char(file_name, str, extension, '.');

    return;
}

void dir_get_procfs_cmd(std::string &str)
{
    char config_file_name[1025] = {0};
    pid_t proc_id = getpid();
    std::string command_line;
    std::string startup_command;
    std::vector<std::string> command_arguements;
    std::vector<std::string> directory_tokens;

    sprintf(config_file_name, "/proc/%d/cmdline", proc_id);

    std::ifstream cfgfile(config_file_name);

    if(!cfgfile.is_open())
    {
        return;
    }

    if(cfgfile.good())
    {
        std::getline(cfgfile, command_line);
    }

    cfgfile.close();

    if(command_line.length() < 1)
    {
        return;
    }

    str_ex_split_with_char(command_line, command_arguements, ' ');

    if(command_arguements.size() > 0)
    {
        startup_command = command_arguements[0];
    }
    else
    {
        startup_command = command_line;
    }

    str_ex_split_with_char(startup_command, directory_tokens, '/');

    if(directory_tokens.size() > 0)
    {
        str = directory_tokens[directory_tokens.size() - 1].c_str();
    }

    return;
}

void  dir_get_log_directory(std::string &str)
{
    pid_t parent = getppid();

    if(parent == 0)
    {
        str =  "/var/log/";
    }
    else
    {
        str.clear();
        char wd_path[1025] = { 0 };
        size_t wd_len = 1024;
        getcwd(wd_path, wd_len);
        dir_get_parent_directory(wd_path, str);
        strcpy(wd_path, str.c_str());
        strcat(wd_path, "log/");
        str = wd_path;
    }

}

void  dir_get_config_directory(std::string &str)
{
    char wd_path[1025] = { 0 };
    size_t wd_len = 1024;
    getcwd(wd_path, wd_len);
    dir_get_parent_directory(wd_path, str);
    strcpy(wd_path, str.c_str());
    strcat(wd_path, "etc/");
    str = wd_path;
}

void  dir_get_default_config_file(std::string &str)
{
    std::string temp;
    dir_get_procfs_cmd(temp);
    dir_get_config_directory(str);
    str += temp;
    str += ".conf";
}
