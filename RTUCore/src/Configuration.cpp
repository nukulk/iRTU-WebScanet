/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#include "Configuration.h"
#include "StringEx.h"
#include "Directory.h"

#include <map>
#include <fstream>
#include <vector>
#include <algorithm>
#include <limits.h>
#include <float.h>

typedef struct Configuration
{
    std::map<std::string, std::map<std::string, std::string>> ConfigurationMap;
    std::string ConfigFileName;
}Configuration;

void configuration_internal_add_section(Configuration *conf_ptr, std::string &str, std::map<std::string, std::string> &list);
void configuration_internal_get_value(Configuration *conf_ptr, const std::string &section, const std::string &key, std::string &value);

void* configuration_allocate_default()
{
    std::string defualt_config_file;

    dir_get_default_config_file(defualt_config_file);

    bool ret = dir_is_file_exists(defualt_config_file);

    if(!ret)
    {
        return nullptr;
    }

    return configuration_allocate(defualt_config_file);
}

void* configuration_allocate(const std::string &filename)
{
    Configuration* conf_ptr = new  Configuration();

    conf_ptr->ConfigFileName = filename;

    std::ifstream cfgfile(conf_ptr->ConfigFileName);
    std::string line, leftstr, rightstr;
    std::vector<std::string> linelist;

    // Following is a Windows INI style configuration file parsing algorithm
    // The first iteration only loads relevent lines from as a list of strings
    if(!cfgfile.is_open())
    {
        delete conf_ptr;
        return nullptr;
    }
    else
    {
        while(cfgfile.good())
        {
              line.erase();
              std::getline(cfgfile,line);
              str_ex_alltrim(line);

              if(line.length() < 1 || line[0]==';' || line[0]=='#' || line.empty())
              {
                  //Skip comment or blank lines;
                  continue;
              }

              if(!isalnum(line[0]))
              {
                  if(line[0]=='[' && line[line.length()-1]==']')
                  {
                      //Section header
                      linelist.push_back(line);
                  }
                  //Garbage or Invalid line
                  continue;
              }
              else
              {
                  //Normal line
                  linelist.push_back(line);
              }
        }
        // The file can be closed off
        cfgfile.close();
    }

    //Now we would iterate the string list and segregate key value pairs by section groups
    std::string curSecHeader = "";
    std::map<std::string, std::string> kvlist;

    for(std::vector<std::string>::size_type i = 0; i != linelist.size(); i++)
    {
        line = linelist[i];
        //Section header line
        if(line[0]=='[' && line[line.length()-1]==']')
        {
            //Check whether this is the first instance of a section header
            if(conf_ptr->ConfigurationMap.size()<1)
            {
                //Don't need to do anything
                if(curSecHeader.length() > 0)
                {
                    //We reach here when a section is being read for the first time
                    configuration_internal_add_section(conf_ptr, curSecHeader, kvlist);
                }
            }
            else
            {
                //Before staring a new section parsing we need to store the last one
                configuration_internal_add_section(conf_ptr, curSecHeader, kvlist);
            }

            //Store the string as current section header and clear the key value list
            curSecHeader = line;
            kvlist.clear();
        }
        else
        {
            leftstr = rightstr = "";
            str_ex_keyvalue_with_char(line, leftstr, rightstr,'=');
            str_ex_alltrim(leftstr);
            str_ex_alltrim(rightstr);
            kvlist[leftstr]=rightstr;
        }
    }
    configuration_internal_add_section(conf_ptr, curSecHeader,kvlist);

    return conf_ptr;
}

void  configuration_release(void* config)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return;
    }

    delete conf_ptr;
}

void  configuration_get_all_sections(void* config, std::vector<std::string> &sectionlist)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return;
    }

    sectionlist.clear();

    std::map<std::string, std::map<std::string, std::string>>::const_iterator section_iter;

    section_iter = conf_ptr->ConfigurationMap.begin();

    while(section_iter != conf_ptr->ConfigurationMap.end())
    {
        sectionlist.push_back(section_iter->first);
        section_iter++;
    }
}

void  configuration_get_all_keys(void* config, const std::string &section, std::vector<std::string> &keylist)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return;
    }

    keylist.clear();

    std::map<std::string, std::map<std::string, std::string>>::const_iterator section_iter;

    section_iter = conf_ptr->ConfigurationMap.find(section);

    if(section_iter != conf_ptr->ConfigurationMap.end())
    {
        std::map<std::string, std::string>::iterator keyiter;

        std::map<std::string, std::string> temp_key_list = section_iter->second;

        keyiter = temp_key_list.begin();

        while(keyiter != temp_key_list.end())
        {
            keylist.push_back(keyiter->first);
            keyiter++;
        }
    }
}

bool  configuration_has_section(void* config, const std::string &section)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return false;
    }

    std::map<std::string, std::map<std::string, std::string>>::const_iterator confmapiter;

    confmapiter = conf_ptr->ConfigurationMap.find(section);
    if(confmapiter == conf_ptr->ConfigurationMap.end())
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool  configuration_has_key(void* config, const std::string &section, std::string &key)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return false;
    }

    std::map<std::string, std::map<std::string, std::string>>::const_iterator confmapiter;

    confmapiter = conf_ptr->ConfigurationMap.find(section);
    if(confmapiter == conf_ptr->ConfigurationMap.end())
    {
        return false;
    }
    else
    {
        std::map<std::string, std::string> list = confmapiter->second;
        std::map<std::string, std::string>::const_iterator kviter;
        kviter = list.find(key);

        if(kviter == list.end())
        {
            return false;
        }
        return true;
    }
}


long  configuration_get_value_as_integer(void* config, const std::string &section, const std::string &key)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return INT64_MAX;
    }

    std::string value;
    configuration_internal_get_value(conf_ptr, section, key, value);

    return atol(value.c_str());
}

bool  configuration_get_value_as_boolean(void* config, const std::string &section, const std::string &key)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return INT64_MAX;
    }

    std::string value;
    configuration_internal_get_value(conf_ptr, section, key, value);

    if(value == "0")
    {
        return false;
    }

    if(value == "FALSE" || value == "false")
    {
        return false;
    }

    if(value == "1")
    {
        return false;
    }

    if(value == "TRUE" || value == "true")
    {
        return true;
    }

    return false;
}

double configuration_get_value_as_real(void* config, const std::string &section, const std::string &key)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return (double)INT64_MAX;
    }

    std::string value;
    configuration_internal_get_value(conf_ptr, section, key, value);

    return atof(value.c_str());
}

void configuration_get_value_as_string(void* config, const std::string &section, const std::string &key, std::string &value)
{
    Configuration* conf_ptr = static_cast<Configuration*>(config);

    if(conf_ptr == nullptr)
    {
        return;
    }

    configuration_internal_get_value(conf_ptr, section, key, value);
}

void configuration_internal_get_value(Configuration *conf_ptr, const std::string &section, const std::string &key, std::string &value)
{
    std::map<std::string, std::map<std::string, std::string>>::const_iterator confmapiter;

    confmapiter = conf_ptr->ConfigurationMap.find(section);
    if(confmapiter != conf_ptr->ConfigurationMap.end())
    {
        std::map<std::string, std::string> list = confmapiter->second;
        std::map<std::string, std::string>::const_iterator kviter;
        kviter = list.find(key);

        if(kviter != list.end())
        {
            value = kviter->second;
        }
    }
}

void configuration_internal_add_section(Configuration *conf_ptr, std::string &str, std::map<std::string, std::string> &list)
{
    str.erase(0,1);
    str.erase(str.length()-1,1);
    str_ex_alltrim(str);
    conf_ptr->ConfigurationMap[str] = list;
}

