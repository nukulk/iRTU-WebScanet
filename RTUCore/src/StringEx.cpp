/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#include "StringEx.h"
#include <sstream>
#include <string.h>
#include <time.h>

void str_ex_split_with_char(const std::string &str, std::vector<std::string> &tokens, char delim)
{
    std::stringstream ss(str); //convert string to stream
    std::string item;

    while(getline(ss, item, delim))
    {
        tokens.push_back(item); //add token to vector
    }
}

void str_ex_split_with_str(const std::string &str, std::vector<std::string> &tokens, const std::string &delimiters)
{
    // Skip delimiters at beginning
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);

    // Find first non-delimiter
    std::string::size_type pos = str.find_first_of(delimiters, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
        // Found a token, add it to the vector
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next non-delimiter
        pos = str.find_first_of(delimiters, lastPos);
    }
}

void str_ex_keyvalue_with_char(const std::string &str, std::string &keystr, std::string &valuestr, char delim)
{
    std::stringstream ss(str); //convert string to stream
    std::string item;

    int ctr = 0;

    while(getline(ss, item, delim))
    {
        if(ctr==0)
        {
            keystr = item;
            ctr++;
            continue;
        }

        if(ctr==1)
        {
            valuestr = item;
            ctr++;
            continue;
        }

        if(ctr>1)
        {
            valuestr += delim;
            valuestr += item;
            ctr++;
        }
    }
}

int str_ex_keyvalue_with_str(const std::string &str, std::string &keystr, std::string &valuestr, const std::string &delim)
{
    int pos = str.find(delim);

    if(pos == -1)
    {
        return pos;
    }

    char *tptr = new char[pos+1];
    memset(tptr,0, pos+1);

    memcpy(tptr, str.c_str(), pos);

    keystr = tptr;

    delete [] tptr;

    tptr = nullptr;

    const char *ptr = str.c_str();

    valuestr = &ptr[pos+delim.length()];

    return pos;
}

void str_ex_replace_char(std::string &srcstr, const char oldchar, const char newchar)
{
    int len = srcstr.length();
    for(int ctr = 0 ; ctr < len ; ctr++)
    {
        if(srcstr[ctr] == oldchar)
        {
            srcstr[ctr] = newchar;
        }
    }
}

void str_ex_replace_with_char(std::string &srcstr, const std::string oldpattern, const char newchar)
{
    char buff[2]={0};
    buff[0] = newchar;
    str_ex_replace_with_str(srcstr, oldpattern, buff);
}

void  str_ex_replace_with_str(std::string &srcstr, const std::string oldpattern, const std::string newpattern)
{
    if (oldpattern.length() == 0 || srcstr.length() == 0)
    {
        return;
    }

    size_t idx = 0;

    for(;;)
    {
        idx = srcstr.find( oldpattern, idx);

        if (idx == std::string::npos)
        {
            break;
        }

        srcstr.replace( idx, oldpattern.length(), newpattern);
        idx += newpattern.length();
    }
}

void str_ex_replace_with_integer(std::string &srcstr, const std::string oldpattern, const long npattern)
{
    if (oldpattern.length() == 0 || srcstr.length() == 0)
    {
        return;
    }

    size_t idx = 0;

    char ptr[64] = {0};
    sprintf(ptr,"%lu", npattern);
    std::string newpattern = ptr;

    for(;;)
    {
        idx = srcstr.find( oldpattern, idx);

        if (idx == std::string::npos)
        {
            break;
        }

        srcstr.replace( idx, oldpattern.length(), newpattern);
        idx += newpattern.length();
    }
}

void str_ex_replace_with_real(std::string &srcstr, const std::string oldpattern, const double npattern)
{
    if (oldpattern.length() == 0 || srcstr.length() == 0)
    {
        return;
    }

	char ptr[64] = {0};
    sprintf(ptr,"%10.6lf", npattern);
    std::string newpattern = ptr;

    size_t idx = 0;

    for(;;)
    {
        idx = srcstr.find( oldpattern, idx);

        if (idx == std::string::npos)
        {
            break;
        }

        srcstr.replace( idx, oldpattern.length(), newpattern);
        idx += newpattern.length();
    }
}


void  str_ex_lefttrim(std::string &str)
{
   const char *buf=str.c_str();

   for ( NULL; *buf && isspace(*buf); buf++);
   return;
}

void  str_ex_righttrim(std::string &str)
{

}

void str_ex_alltrim(std::string &str)
{
    char buffer[4096];
    memset((char*)&buffer,0,4096);
    strcpy(buffer,str.c_str());

    int len = strlen(buffer);

    if(len<1)
        return;

    for(int i = len-1;  ; i--)
    {
        if(!isspace(buffer[i]) || i < 0)
        {
            break;
        }
        buffer[i] = '\0';
    }

    len = strlen(buffer);

    if(len<1)
    {
        str = buffer;
        return;
    }

    const char *buf=(const char*)&buffer[0];

    for ( NULL; *buf && isspace(*buf); buf++);

    str = buf;
}

bool str_ex_char_is_space(int in)
{
   if ((in == 0x20) || (in >= 0x09 && in <= 0x0D)) return true;
   return false;
}

int str_ex_char_count(const std::string &str, char ch)
{
    int c=0;
    for(int i=0; str[i] != '\0' ;i++)
        if(str[i]==ch)
            c++;

    return c;
}

int str_ex_str_count(const std::string &str, char ch)
{
    return -1;
}

int	str_ex_char_pos(const std::string &str,const char ch)
{
    for(int ctr = 0; str[ctr] != '\0'; ctr++)
    {
        if(str[ctr]==ch)
        {
            return ctr;
        }
    }
    return -1;
}

int str_ex_sub_string_pos(const std::string &str,const std::string &substr)
{
    char* pdest = (char*)strstr(str.c_str(), substr.c_str());
    if(pdest == 0)
    {
        return -1;
    }
    int result = pdest - str.c_str();
    return result;
}

void str_ex_integer_to_string(std::string &str, const long val)
{
    char ptr[255];
    memset((void*)&ptr[0],0,255);
    sprintf(ptr,"%ld",val);
    str = ptr;
}

void str_ex_real_to_string(std::string &str, const double val)
{
    char ptr[255];
    memset((void*)&ptr[0],0,255);
    sprintf(ptr,"%10.6lf",val);
    str = ptr;
}

void str_ex_get_default_timestamp(std::string &str)
{
    char buffer[33] = {0};

    time_t t ;
    struct tm *tmp ;
    time(&t);
    tmp = localtime(&t);

    sprintf(buffer, "%04d%02d%02d%02d%02d%02d",
             (tmp->tm_year+1900), (tmp->tm_mon+1),tmp->tm_mday,
             tmp->tm_hour, tmp->tm_min, tmp->tm_sec);

    str = buffer;
}

void str_ex_get_formatted_timestamp(const std::string &format, std::string &str)
{
    char buffer[33] = {0};

    time_t t ;
    struct tm *tmp ;
    time(&t);
    tmp = localtime(&t);

    sprintf(buffer, "%04d%02d%02d%02d%02d%02d",
             (tmp->tm_year+1900), (tmp->tm_mon+1),tmp->tm_mday,
             tmp->tm_hour, tmp->tm_min, tmp->tm_sec);

    str = buffer;
}
