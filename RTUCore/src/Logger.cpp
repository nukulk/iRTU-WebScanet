/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#include "Logger.h"
#include "Directory.h"
#include "StringEx.h"

#include <unistd.h>
#include <string.h>
#include <stdio.h>

static char log_level_names[5][16] = {"Information", "Error", "Warning", "Critical"};
#define END_OF_LINE "\n"

#pragma pack(1)
typedef struct Logger
{
    size_t logFileSize;
    char logfilename[1025];
    FILE* logFile;
}Logger;
#pragma pop

void logger_internal_normalize_function_name(std::string &str);

void* logger_allocate(size_t flieszmb)
{
    Logger* logger_ptr = new Logger();

    if(!logger_ptr)
    {
        return nullptr;
    }

    if(flieszmb < 1 || flieszmb > 10)
    {
        flieszmb = 10;
    }

    logger_ptr->logFileSize = flieszmb;

    std::string temp;

    dir_get_log_directory(temp);

    if(!dir_is_directory_exists(temp))
    {
        dir_create_directory(temp);
    }

    strcat(logger_ptr->logfilename, temp.c_str());
    dir_get_procfs_cmd(temp);
    strcat(logger_ptr->logfilename, temp.c_str());
    strcat(logger_ptr->logfilename, ".log");

    return logger_ptr;
}

void  logger_release(void* logger_ptr)
{
    Logger* logger = static_cast<Logger*>(logger_ptr);

    if(!logger)
    {
        return;
    }

    if(logger->logFile)
    {
        fflush(logger->logFile);
        fclose(logger->logFile);
    }

    delete logger;
}

void  logger_write(void* logger_ptr, const std::string &logEntry, LogLevel llevel, const char* func, const char* file, int line)
{
    Logger* logger = (Logger*)logger_ptr;

    if(!logger)
    {
        return;
    }

    if(logger->logFile == nullptr)
    {
        logger->logFile = fopen(logger->logfilename, "w");

        if(logger->logFile == nullptr)
        {
            return;
        }
    }

    // Check the file size
    size_t sz = (size_t)ftell(logger->logFile);

    // If it exceeds the set size
    if(sz >= logger->logFileSize*1024*1024)
    {
        // Stop logging
        fflush(logger->logFile);
        fclose(logger->logFile);

        // Rename the file
        char old_log_filename[1025] = {0};
        strcat(old_log_filename, logger->logfilename);
        strcat(old_log_filename, ".old");

        rename(logger->logfilename, old_log_filename);

        // Reopen the log file with original name
        logger->logFile = fopen(logger->logfilename, "w");

        if(logger->logFile == nullptr)
        {
            return;
        }
    }

    std::string temp_str;

    time_t t;
    struct tm *tmp ;
    time(&t);
    tmp = localtime(&t);

    // Timestamp
    fprintf(logger->logFile, "%02d-%02d-%04d %02d:%02d:%02d\t",
             tmp->tm_mday, (tmp->tm_mon+1), (tmp->tm_year+1900),
             tmp->tm_hour, tmp->tm_min, tmp->tm_sec);

    // Level
    fprintf(logger->logFile, "%s\t", log_level_names[llevel]);

    // File
    dir_get_file_name(file, temp_str);
    fprintf(logger->logFile, "%s\t", temp_str.c_str());

    // Line
    fprintf(logger->logFile, "%d\t", line);

    // Function
    temp_str = func;
    logger_internal_normalize_function_name(temp_str);
    fprintf(logger->logFile, "%s\t", temp_str.c_str());

    // Message
    fprintf(logger->logFile, "%s", logEntry.c_str());

    // End of line
    fprintf(logger->logFile, END_OF_LINE);

    // Flush th contents
    fflush(logger->logFile);

    return;
}

void logger_internal_normalize_function_name(std::string &str)
{
    std::string left;
    std::string right;
    std::vector<std::string> tokens;

    str_ex_keyvalue_with_char(str, left, right, '(');

    str =  left;

    str_ex_split_with_char(str, tokens, ' ');

    if(tokens.size() > 0)
    {
        str = tokens[tokens.size() - 1];
    }

    str_ex_alltrim(str);

    int pos = str_ex_sub_string_pos(str, "::");

    if(pos >= 0)
    {
        str_ex_keyvalue_with_str(str, left, right, "::");

        str =  right;
    }
}
