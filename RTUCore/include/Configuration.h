/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef CONFIGURATION_CPP
#define CONFIGURATION_CPP

#include <string>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

extern LIBRARY_EXPORT void* configuration_allocate_default();
extern LIBRARY_EXPORT void* configuration_allocate(const std::string &filename);
extern LIBRARY_EXPORT void  configuration_release(void* config);

extern LIBRARY_EXPORT void  configuration_get_all_sections(void* config, std::vector<std::string> &sectionlist);
extern LIBRARY_EXPORT void  configuration_get_all_keys(void* config, const std::string &section, std::vector<std::string> &keylist);

extern LIBRARY_EXPORT bool  configuration_has_section(void* config, const std::string &section);
extern LIBRARY_EXPORT bool  configuration_has_key(void* config, const std::string &section, std::string &key);

extern LIBRARY_EXPORT long  configuration_get_value_as_integer(void* config, const std::string &section, const std::string &key);
extern LIBRARY_EXPORT bool  configuration_get_value_as_boolean(void* config, const std::string &section, const std::string &key);
extern LIBRARY_EXPORT double configuration_get_value_as_real(void* config, const std::string &section, const std::string &key);
extern LIBRARY_EXPORT void configuration_get_value_as_string(void* config, const std::string &section, const std::string &key, std::string &value);

#ifdef __cplusplus
}
#endif

#endif
