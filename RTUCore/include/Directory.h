/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef DIRECTORY_CPP
#define DIRECTORY_CPP

#include <string>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

extern LIBRARY_EXPORT bool  dir_is_directory_exists(const std::string &str);
extern LIBRARY_EXPORT void  dir_get_parent_directory(const std::string &filename, std::string &str);
extern LIBRARY_EXPORT void  dir_create_directory(const std::string &str);
extern LIBRARY_EXPORT bool  dir_is_file_exists(const std::string &filename);
extern LIBRARY_EXPORT void  dir_get_file_extension(const std::string &file_name, std::string &str);
extern LIBRARY_EXPORT void  dir_get_file_name(const std::string &path, std::string &str);
extern LIBRARY_EXPORT void  dir_get_file_basename(const std::string &file_name, std::string &str);
extern LIBRARY_EXPORT void  dir_get_procfs_cmd(std::string &str);
extern LIBRARY_EXPORT void  dir_get_log_directory(std::string &str);
extern LIBRARY_EXPORT void  dir_get_config_directory(std::string &str);
extern LIBRARY_EXPORT void  dir_get_default_config_file(std::string &str);

#ifdef __cplusplus
}
#endif

#endif

