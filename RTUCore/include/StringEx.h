/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef STRING_EX_CPP
#define STRING_EX_CPP

#include <string>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

extern LIBRARY_EXPORT void  str_ex_split_with_char(const std::string &str, std::vector<std::string> &tokens, char delim=' ');
extern LIBRARY_EXPORT void  str_ex_split_with_str(const std::string &str, std::vector<std::string> &tokens, const std::string &delimiters = " ");
extern LIBRARY_EXPORT void  str_ex_keyvalue_with_char(const std::string &str, std::string &keystr, std::string &valuestr, char delim);
extern LIBRARY_EXPORT int   str_ex_keyvalue_with_str(const std::string &str, std::string &keystr, std::string &valuestr, const std::string &delim);

extern LIBRARY_EXPORT void  str_ex_replace_char(std::string &srcstr, const char oldchar, const char newchar);
extern LIBRARY_EXPORT void  str_ex_replace_with_char(std::string &srcstr, const std::string oldpattern, const char newchar);
extern LIBRARY_EXPORT void  str_ex_replace_with_str(std::string &srcstr, const std::string oldpattern, const std::string newpattern);
extern LIBRARY_EXPORT void  str_ex_replace_with_real(std::string &srcstr, const std::string oldpattern, const double npattern);
extern LIBRARY_EXPORT void  str_ex_replace_with_integer(std::string &srcstr, const std::string oldpattern, const long npattern);

extern LIBRARY_EXPORT void  str_ex_lefttrim(std::string &str);
extern LIBRARY_EXPORT void  str_ex_righttrim(std::string &str);
extern LIBRARY_EXPORT void  str_ex_alltrim(std::string &str);

extern LIBRARY_EXPORT bool  str_ex_char_is_space(int in);

extern LIBRARY_EXPORT int   str_ex_char_count(const std::string &str, char ch);
extern LIBRARY_EXPORT int   str_ex_str_count(const std::string &str, char ch);

extern LIBRARY_EXPORT int   str_ex_char_pos(const std::string &str, const char ch);
extern LIBRARY_EXPORT int   str_ex_sub_string_pos(const std::string &str,const std::string &substr);

extern LIBRARY_EXPORT void  str_ex_integer_to_string(std::string &str, const long val);
extern LIBRARY_EXPORT void  str_ex_real_to_string(std::string &str, const double val);

extern LIBRARY_EXPORT void  str_ex_get_default_timestamp(std::string &str);
extern LIBRARY_EXPORT void  str_ex_get_formatted_timestamp(const std::string &format, std::string &str);

#ifdef __cplusplus
}
#endif

#endif

