/*

Copyright (c) 2020, CIMCON Automation
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef LOGGER_CPP
#define LOGGER_CPP

#include <string>

#ifdef __cplusplus
extern "C" {
#endif

#define LIBRARY_EXPORT __attribute__((visibility("default")))

typedef enum LogLevel
{
    LOG_INFO=0,
    LOG_ERROR=1,
    LOG_WARNING=2,
    LOG_CRITICAL=3
}LogLevel;

extern LIBRARY_EXPORT void* logger_allocate(size_t flieszmb);
extern LIBRARY_EXPORT void  logger_release(void* logger_ptr);
extern LIBRARY_EXPORT void  logger_write(void* logger_ptr, const std::string &logEntry, LogLevel llevel, const char* func, const char* file, int line);

#define WriteLog(logger, str, level) logger_write(logger, str, level, __PRETTY_FUNCTION__, __FILE__, __LINE__);
#define WriteInformation(logger, str) logger_write(logger, str, LOG_INFO, __PRETTY_FUNCTION__, __FILE__, __LINE__);

#ifdef __cplusplus
}
#endif

#endif

