#ifndef DIGITAL_IO_SERVICE
#define DIGITAL_IO_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>

class DIOService
{
public:
    DIOService();
    virtual ~DIOService();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    void* GetLogger();
private:
    void* config;
    void* message_bus;
    void* logger;
    std::vector<std::string> destination_list;

    int di_max_enable;
    int di_scan_rate;
    int do_max_enable;
    std::vector<std::string> input_pin_nos;
    std::vector<std::string> input_pin_names;
};

#endif
