#ifndef DIGITAL_IN
#define DIGITAL_IN

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_DI  (16)

#define DI16_GPIO_MAP     79    //CN1_64
#define DI15_GPIO_MAP     78    //CN1_66
#define DI14_GPIO_MAP     77    //CN1_68
#define DI13_GPIO_MAP     76    //CN1_70
#define DI12_GPIO_MAP     75    //CN1_72
#define DI11_GPIO_MAP     74    //CN1_74
#define DI10_GPIO_MAP     73    //CN1_76
#define DI9_GPIO_MAP      72    //CN1_78
#define DI8_GPIO_MAP      71    //CN1_80
#define DI7_GPIO_MAP      70    //CN1_82
#define DI6_GPIO_MAP      69    //CN1_84
#define DI5_GPIO_MAP      68    //CN1_86
#define DI4_GPIO_MAP      67    //CN1_88
#define DI3_GPIO_MAP      66    //CN1_90
#define DI2_GPIO_MAP      65    //CN1_92
#define DI1_GPIO_MAP      64    //CN1_94

int digital_in_initialize(char* error_string);
int digital_in_release(char *error_string);
int digital_in_read(int pin, char *error_string);

#endif
