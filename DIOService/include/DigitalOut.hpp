#ifndef DIGITAL_OUT
#define DIGITAL_OUT

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_DO  (8)

#define DO1_GPIO_MAP    120  //CN1_30
#define DO2_GPIO_MAP    121  //CN1_32
#define DO3_GPIO_MAP    122  //CN1_34
#define DO4_GPIO_MAP    123  //CN1_38
#define DO5_GPIO_MAP    124  //CN1_40
#define DO6_GPIO_MAP    143  //CN1_42
#define DO7_GPIO_MAP    144  //CN1_44
#define DO8_GPIO_MAP    145  //CN1_46
#define DO_EN_GPIO_MAP  51   //CN1_48 (Logic-0 to enable it)

int digital_out_initialize(char *error_string);
int digital_out_release(char *error_string);
int digital_out_write(int pin, int value, char *error_string);

#endif
