#include "DIOService.hpp"

int main(int argc, char* argv[])
{
    DIOService dio;

    if(!dio.Initialize())
    {
        return -1;
    }

    if(dio.Start())
    {
        dio.Stop();
    }

    return 0;
}
