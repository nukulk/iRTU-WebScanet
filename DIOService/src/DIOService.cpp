#include "DIOService.hpp"
#include "DigitalIn.hpp"
#include "DigitalOut.hpp"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <MessageBus/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>
#include <RTUCore/Directory.h>

#include <string>
#include <thread>
#include <chrono>

static void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void OnSignalReceived(SignalType type);

static DIOService *appptr = nullptr;

DIOService::DIOService()
{
    message_bus = nullptr;
    logger = nullptr;
    config = nullptr;
    appptr = this;
}

DIOService::~DIOService()
{

}

bool DIOService::Initialize()
{
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        return false;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == nullptr)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    std::string value;
    configuration_get_value_as_string(config, "default", "destination", value);

    if(str_ex_char_count(value, ',') > 0)
    {
        str_ex_split_with_char(value, destination_list, ',');
    }
    else
    {
        destination_list.clear();
        destination_list.push_back(value);
    }

    di_max_enable = configuration_get_value_as_integer(config, "default", "di_max_enable");
    di_scan_rate = configuration_get_value_as_integer(config, "default", "di_scan_rate");
    do_max_enable = configuration_get_value_as_integer(config, "default", "do_max_enable");

    configuration_get_value_as_string(config, "default", "input_pin_nos", value);

    if(str_ex_char_count(value, ',') > 0)
    {
        str_ex_split_with_char(value, input_pin_nos, ',');
    }
    else
    {
        input_pin_nos.clear();
        input_pin_nos.push_back(value);
    }

    configuration_get_value_as_string(config, "default", "input_pin_names", value);

    if(str_ex_char_count(value, ',') > 0)
    {
        str_ex_split_with_char(value, input_pin_names, ',');
    }
    else
    {
        input_pin_names.clear();
        input_pin_names.push_back(value);
    }

    configuration_release(config);

    return true;
}

bool DIOService::Destroy()
{
    return true;
}

bool DIOService::Start()
{
    if(!message_bus_initialize(&message_bus, OnNetworkEvent))
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    char error_message[64];

    memset(error_message, 0, sizeof (error_message));
    digital_in_initialize(error_message);

    memset(error_message, 0, sizeof (error_message));
    digital_out_initialize(error_message);

    bool loop = true;

    while(loop)
    {
        sleep(di_scan_rate);

        std::string payload;

        // Read from all configured pins
        size_t configured_pins = input_pin_nos.size();
        for(size_t index = 0; index < configured_pins; index++)
        {
            int pin_no = atoi(input_pin_nos[index].c_str());
            std::string pin_name = input_pin_names[index];

            memset(error_message, 0, sizeof (error_message));
            int pin_value = digital_in_read(pin_no, error_message);

            if(pin_value == -1)
            {
                WriteLog(appptr->GetLogger(), error_message, LOG_ERROR);
                continue;
            }

            std::string temp;
            str_ex_integer_to_string(temp, pin_value);

            payload += pin_name;
            payload += ":";
            payload += temp;
            payload += ",";
        }

        payload =  payload.erase(payload.length()-1,1);

        for(auto destination : destination_list)
        {
            loop = message_bus_send(message_bus, destination.c_str(), Data, UserData, Text, payload.c_str(), payload.length());
            if(!loop)
            {
                break;
            }
       }
    }

    return true;
}

bool DIOService::Restart()
{
    return false;
}

bool DIOService::Stop()
{
    char error_message[64];

    memset(error_message, 0, sizeof (error_message));
    digital_in_release(error_message);

    memset(error_message, 0, sizeof (error_message));
    digital_out_release(error_message);

    message_bus_close(message_bus);
    message_bus_release(message_bus);
    destination_list.clear();
    logger_release(logger);

    return false;
}

void* DIOService::GetLogger()
{
    return logger;
}

void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    if(ptype == Data || mtype == LoopBack)
    {
        WriteInformation(appptr->GetLogger(), "Loopback successfull");
    }

    //We assume Text and Userdata for data type and message type
    if(ptype == Request)
    {
        char error_message[64];

        std::vector<std::string> command_lines;

        str_ex_split_with_str(messagebuffer, command_lines, "\r\n");

        std::string command_name = command_lines[0];
        std::string command_string = command_lines[1];

        //Ignore the command name, assume that we need to write out

        std::vector<std::string> command_values;

        //Make a list of PIN:SIGNAL combinations
        str_ex_split_with_char(command_string, command_values, ',');

        for(auto kv : command_values)
        {
            // String is composed as PIN:SIGNAL
            unsigned char pin = kv[0];
            unsigned char signal = kv[2];

            memset(error_message, 0, sizeof (error_message));

            if(digital_out_write(pin, signal, error_message) == -1)
            {
                WriteLog(appptr->GetLogger(), error_message, LOG_ERROR);
            }
        }
    }
}

void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(appptr->GetLogger(), "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(appptr->GetLogger(), "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(appptr->GetLogger(), "SHUTDOWN SIGNAL", LOG_CRITICAL);
            appptr->Stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(appptr->GetLogger(), "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(appptr->GetLogger(), "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(appptr->GetLogger(), "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(appptr->GetLogger(), "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(appptr->GetLogger(), "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
