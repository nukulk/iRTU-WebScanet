#include "DigitalIn.hpp"

#define BUFFER_MAX      (5)
#define DIRECTION_MAX   (35)
#define VALUE_MAX       (30)

#define IN      (0)
#define OUT     (1)

#define LOW     (0)
#define HIGH    (1)

#define PIN  24 /* P1-18 */
#define POUT 4  /* P1-07 */

static int GPIOExport(int pin);
static int GPIOUnexport(int pin);
static int GPIODirection(int pin, int dir);

static unsigned char digital_in_pin_map[] =
                    {
                        DI1_GPIO_MAP, DI2_GPIO_MAP, DI3_GPIO_MAP, DI4_GPIO_MAP,
                        DI5_GPIO_MAP, DI6_GPIO_MAP, DI7_GPIO_MAP, DI8_GPIO_MAP,
                        DI9_GPIO_MAP, DI10_GPIO_MAP, DI11_GPIO_MAP, DI12_GPIO_MAP,
                        DI13_GPIO_MAP, DI14_GPIO_MAP, DI15_GPIO_MAP, DI16_GPIO_MAP
                    };

int digital_in_initialize(char *error_string)
{
    for(int tIndex = 0 ; tIndex < MAX_DI ; ++tIndex)
    {
        if (0 == GPIOExport(digital_in_pin_map[tIndex]) )
        {
            if (-1 == GPIODirection(digital_in_pin_map[tIndex], IN))
            {
                sprintf(error_string, "Error at GPIODirection DI(%d)", tIndex+1);
            }
        }
        else
        {
            sprintf(error_string, "Error at GPIOExport DI(%d)", tIndex+1);
        }
    }
    return 0;
}

int digital_in_release(char *error_string)
{
    for(int tIndex = 0 ; tIndex < MAX_DI ; ++tIndex)
    {
        if (-1 == GPIOUnexport(digital_in_pin_map[tIndex]) )
        {
            sprintf(error_string, "Error at GPIOUnexport Di(%d)", tIndex+1);
        }
    }

    return 0;
}

int digital_in_read(int pin, char *error_string)
{
    char path[VALUE_MAX] = {0,};
    char value_str[3] = {0,};
    int fd;

    snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
    fd = open(path, O_RDONLY);
    if (-1 == fd)
    {
        sprintf(error_string, "Failed to open gpio value for reading!\n");
        return(-1);
    }

    if (-1 == read(fd, value_str, 3))
    {
        sprintf(error_string, "Failed to read value!\n");
        return(-1);
    }

    close(fd);

    return(atoi(value_str));
}

static int GPIOExport(int pin)
{
    char buffer[BUFFER_MAX] = {0,};
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/export", O_WRONLY);
    if (-1 == fd)
    {
        fprintf(stderr, "Failed to open export for writing!\n");
        return(-1);
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);

    write(fd, buffer, bytes_written);

    close(fd);

    return(0);
}

static int GPIOUnexport(int pin)
{
    char buffer[BUFFER_MAX] = {0,};
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (-1 == fd)
    {
        fprintf(stderr, "Failed to open unexport for writing!\n");
        return(-1);
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);

    write(fd, buffer, bytes_written);

    close(fd);

    return(0);
}

static int GPIODirection(int pin, int dir)
{
    static const char s_directions_str[]  = "in\0out";
    char path[DIRECTION_MAX];
    int fd;

    snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
    fd = open(path, O_WRONLY);
    if (-1 == fd)
    {
        fprintf(stderr, "Failed to open(%s) gpio direction for writing!\n", path);
        return(-1);
    }

    if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3))
    {
        fprintf(stderr, "Failed to set direction!\n");
        return(-1);
    }

    close(fd);

    return(0);
}


