#include "DigitalOut.hpp"

#define BUFFER_MAX      (5)
#define DIRECTION_MAX   (35)
#define VALUE_MAX       (30)

#define IN      (0)
#define OUT     (1)

#define LOW     (0)
#define HIGH    (1)

#define PIN  24 /* P1-18 */
#define POUT 4  /* P1-07 */

static int GPIOExport(int pin);
static int GPIOUnexport(int pin);
static int GPIODirection(int pin, int dir);

static unsigned char digital_out_pin_map[] =
                    {
                        DO1_GPIO_MAP, DO2_GPIO_MAP, DO3_GPIO_MAP, DO4_GPIO_MAP,
                        DO5_GPIO_MAP, DO6_GPIO_MAP, DO7_GPIO_MAP, DO8_GPIO_MAP
                    };

int digital_out_initialize(char *error_string)
{
    for(int index = 0 ; index < MAX_DO ; ++index)
    {
        if (0 == GPIOExport(digital_out_pin_map[index]) )
        {
            if (-1 == GPIODirection(digital_out_pin_map[index], OUT))
            {
                sprintf(error_string, "Error at GPIODirection DO(%d)", index+1);
            }
        }
        else
        {
            sprintf(error_string, "Error at GPIOExport DO(%d)", index+1);
        }
    }

    if (0 == GPIOExport(DO_EN_GPIO_MAP) )
    {
        if (-1 == GPIODirection(DO_EN_GPIO_MAP, OUT))
        {
            sprintf(error_string, "Error at GPIODirection DO_EN");
        }
    }
    else
    {
        sprintf(error_string, "Error at GPIOExport DO_EN");
    }

    digital_out_write(DO_EN_GPIO_MAP, 0, error_string);

    return 0;
}

int digital_out_release(char *error_string)
{
    for(int index = 0 ; index < MAX_DO ; ++index)
    {
        if (-1 == GPIOUnexport(digital_out_pin_map[index]) )
        {
            sprintf(error_string, "Error at GPIOUnexport DO(%d)", index+1);
        }
    }

    if (-1 == GPIOUnexport(DO_EN_GPIO_MAP) )
    {
        sprintf(error_string, "Error at GPIOUnexport DO_EN");
    }

    return 0;
}

int digital_out_write(int pin, int value, char *error_string)
{
    static const char s_values_str[] = "01";

    char path[VALUE_MAX];
    int fd;

    snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);

    fd = open(path, O_WRONLY);

    if (-1 == fd)
    {
        sprintf(error_string, "Failed to open gpio value for writing!\n");
        return(-1);
    }

    if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1))
    {
        sprintf(error_string, "Failed to write value!\n");
        return(-1);
    }

    close(fd);

    return(0);
}

static int GPIOExport(int pin)
{
    char buffer[BUFFER_MAX] = {0,};
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/export", O_WRONLY);

    if (-1 == fd)
    {
        fprintf(stderr, "Failed to open export for writing!\n");
        return(-1);
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);

    write(fd, buffer, bytes_written);

    close(fd);

    return(0);
}

static int GPIOUnexport(int pin)
{
    char buffer[BUFFER_MAX] = {0,};
    ssize_t bytes_written;
    int fd;

    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (-1 == fd)
    {
        fprintf(stderr, "Failed to open unexport for writing!\n");
        return(-1);
    }

    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);

    write(fd, buffer, bytes_written);

    close(fd);

    return(0);
}

static int GPIODirection(int pin, int dir)
{
    static const char s_directions_str[]  = "in\0out";
    char path[DIRECTION_MAX];
    int fd;

    snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
    fd = open(path, O_WRONLY);
    if (-1 == fd)
    {
        fprintf(stderr, "Failed to open(%s) gpio direction for writing!\n", path);
        return(-1);
    }

    if (-1 == write(fd, &s_directions_str[IN == dir ? 0 : 3], IN == dir ? 2 : 3))
    {
        fprintf(stderr, "Failed to set direction!\n");
        return(-1);
    }

    close(fd);

    return(0);
}
