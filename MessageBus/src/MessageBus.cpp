// Declaration header
#include "MessageBus.h"
#include "MessageBusUtility.h"

// C Headers
#include <mosquitto.h>
#include <limits.h>
#include <memory.h>
#include <string.h>
#include <algorithm>

// C++ Headers
#include <string>
#include <list>
#include <thread>
#include <chrono>

// Will be called when the shared library loads for the first time
LIBRARY_ENTRY void library_load()
{

}

// Will be called when the shared library unloads
LIBRARY_EXIT void library_unload()
{

}

#pragma pack(1)
typedef struct MQTTParams
{
    bool run;
    char process_name[33];
    struct mosquitto *mosq;
    int keep_alive;
    int payload_sequence;
    messabus_bus_callback callback;
    std::list<std::string> node_list;
}MQTTParams;

static void message_bus_internal_connect_callback(struct mosquitto *mosq, void *obj, int result);
static void message_bus_internal_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);
static void message_bus_internal_receiver_thread(MQTTParams *message_bus_ptr);
static bool message_bus_internal_register(MQTTParams *message_bus_ptr);
static bool message_bus_internal_deregister(MQTTParams *message_bus_ptr);

bool message_bus_initialize(void** pptr, messabus_bus_callback cl)
{
    MQTTParams* message_bus_ptr = new MQTTParams();

    if (message_bus_ptr == nullptr)
    {
        return false;
    }

    int rc = 0;

    *pptr = message_bus_ptr;

    bool clean_session = true;

    message_bus_ptr->run = true;
    message_bus_ptr->keep_alive = 60;
    message_bus_ptr->callback = cl;
    message_bus_ptr->payload_sequence = 0;
    memset(message_bus_ptr->process_name, 0, 33);
    messgae_bus_utility_current_process_name(message_bus_ptr->process_name);
    message_bus_ptr->node_list.clear();
    rc = mosquitto_lib_init();

    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    message_bus_ptr->mosq = mosquitto_new(message_bus_ptr->process_name, clean_session, message_bus_ptr);

    if(!message_bus_ptr->mosq)
    {
        return false;
    }

    return true;
}

bool message_bus_release(void* ptr)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr)
    {
        return false;
    }

    mosquitto_lib_cleanup();

    delete message_bus_ptr;

    return true;
}

bool message_bus_open(void* ptr)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr)
    {
        return false;
    }

    mosquitto_message_callback_set(message_bus_ptr->mosq, message_bus_internal_message_callback);
    mosquitto_connect_callback_set(message_bus_ptr->mosq, message_bus_internal_connect_callback);

    int rc = 0;

    rc = mosquitto_connect(message_bus_ptr->mosq, "127.0.0.1", 1883, message_bus_ptr->keep_alive);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    std::thread receiver(&message_bus_internal_receiver_thread, message_bus_ptr);
    receiver.detach();

    std::string subscription_topic;

    subscription_topic.clear();
    subscription_topic += message_bus_ptr->process_name;
    subscription_topic += "/+";
    rc = mosquitto_subscribe(message_bus_ptr->mosq, nullptr, subscription_topic.c_str(), 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    subscription_topic.clear();
    subscription_topic = "NODEONLINE/+";
    rc = mosquitto_subscribe(message_bus_ptr->mosq, nullptr, subscription_topic.c_str(), 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    subscription_topic.clear();
    subscription_topic = "NODEOFFLINE/+";
    rc = mosquitto_subscribe(message_bus_ptr->mosq, nullptr, subscription_topic.c_str(), 0);
    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    return message_bus_internal_register(message_bus_ptr);
}


bool message_bus_close(void* ptr)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr)
    {
        return false;
    }

    message_bus_internal_deregister(message_bus_ptr);

    message_bus_ptr->run = false;

    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    int rc = mosquitto_disconnect(message_bus_ptr->mosq);

    if(rc != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    mosquitto_destroy(message_bus_ptr->mosq);

    message_bus_ptr->mosq = nullptr;

    if(message_bus_ptr->node_list.size() > 0)
    {
        message_bus_ptr->node_list.clear();
    }

    return false;
}

// Messaging
bool message_bus_send(void* ptr, const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr)
    {
        return false;
    }

    message_bus_ptr->payload_sequence++;
    if(message_bus_ptr->payload_sequence > INT_MAX -1)
    {
        message_bus_ptr->payload_sequence = 1;
    }

    // Take a big enough buffer
    char publish_topic[64] = { 0 };
    sprintf(publish_topic, "%s/%s:%c%c%c", node_name, message_bus_ptr->process_name, ptype, mtype, dtype);

    long datalen = 0;
    char* dataptr = nullptr;

    if(dtype == Text)
    {
        dataptr = (char*)messagebuffer;
        datalen = buffersize;
    }
    else
    {
        dataptr = messgae_bus_utility_base64_encode((const unsigned char*)messagebuffer, (unsigned long)buffersize, dataptr, (unsigned long*)&datalen);
        datalen = strlen(dataptr);
    }

    if(mosquitto_publish(message_bus_ptr->mosq, &message_bus_ptr->payload_sequence, publish_topic, datalen, dataptr,  0, 0) != MOSQ_ERR_SUCCESS)
    {
        return false;
    }

    if(dtype != Text)
    {
        free(dataptr);
    }

    return true;
}


bool message_bus_send_loopback(void* ptr)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr)
    {
        return false;
    }

    return message_bus_send(message_bus_ptr, message_bus_ptr->process_name, Event, LoopBack, Text, message_bus_ptr->process_name, strlen(message_bus_ptr->process_name));
}


const char *message_bus_localname(void* ptr)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr)
    {
        return nullptr;
    }

    return message_bus_ptr->process_name;
}

long message_bus_has_node_like(void* ptr, const char* node_name)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr)
    {
        return -1;
    }

    if(message_bus_ptr->node_list.size() < 1)
    {
        return -1;
    }

    long index = 0;

    for (auto str_node : message_bus_ptr->node_list)
    {
        if(strstr(str_node.c_str(), node_name))
        {
            break;
        }

        index++;
    }

    return index;
}

const char* message_bus_node_fullname(void* ptr, long node_index)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(ptr);

    if(message_bus_ptr == nullptr || node_index < 0)
    {
        return nullptr;
    }

    if(node_index > message_bus_ptr->node_list.size() || message_bus_ptr->node_list.size() < 1)
    {
        return nullptr;
    }

    const char* str = nullptr;

    long index = 0;

    for (auto str_node : message_bus_ptr->node_list)
    {
        if(index == node_index)
        {
            str = str_node.c_str();
            break;
        }

        index++;
    }

    return str;
}

void message_bus_internal_receiver_thread(MQTTParams* message_bus_ptr)
{
    if(message_bus_ptr == nullptr)
    {
        return;
    }

    int rc = 0;

    while(message_bus_ptr->run)
    {
        rc = mosquitto_loop(message_bus_ptr->mosq, -1, 1);

        if(message_bus_ptr->run && rc)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(2000));

            rc = mosquitto_reconnect(message_bus_ptr->mosq);

            if(rc != MOSQ_ERR_SUCCESS)
            {
                message_bus_ptr->run = false;
                break;
            }
        }
    }
}

/*------------------------------------------------------------*/
// Internal functions
// Node management

bool message_bus_internal_register(MQTTParams *message_bus_ptr)
{
    return message_bus_send(message_bus_ptr, "NODEONLINE", Event, Register, Text, message_bus_ptr->process_name, strlen(message_bus_ptr->process_name));
}

bool message_bus_internal_deregister(MQTTParams *message_bus_ptr)
{
    return message_bus_send(message_bus_ptr, "NODEOFFLINE", Event, DeRegister, Text, message_bus_ptr->process_name, strlen(message_bus_ptr->process_name));
}

void message_bus_internal_connect_callback(struct mosquitto *mosq, void *obj, int result)
{
}

void message_bus_internal_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    MQTTParams* message_bus_ptr = static_cast<MQTTParams*>(obj);

    if (message_bus_ptr == nullptr)
    {
        return;
    }

    size_t tlen = strlen(message->topic);

    DataType dtype = (DataType)message->topic[tlen-1];
    MessageType mtype = (MessageType)message->topic[tlen-2];
    PayloadType ptype = (PayloadType)message->topic[tlen-3];
    message->topic[tlen-4] = 0;

    std::string topic;
    std::string sender;

    std::vector<std::string> tokens;

    messgae_bus_utility_split_string(std::string(message->topic), tokens, '/');
    topic = tokens[0];
    sender = tokens[1];

    if(topic ==  "NODEONLINE")
    {
        if(strcmp((char*)message->payload, message_bus_ptr->process_name) !=0 )
        {
            std::list<std::string>::iterator it;

            it = std::find(message_bus_ptr->node_list.begin(), message_bus_ptr->node_list.end(), sender);

            if(it == message_bus_ptr->node_list.end())
            {
                message_bus_ptr->node_list.push_back(sender);
                message_bus_internal_register(message_bus_ptr);
		mtype = NodeOnline;
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }
    }

    if(topic == "NODEOFFLINE")
    {
        if(strcmp((char*)message->payload, message_bus_ptr->process_name) !=0 )
        {
            std::list<std::string>::iterator it;

            it = std::find(message_bus_ptr->node_list.begin(), message_bus_ptr->node_list.end(), sender);

            if(it != message_bus_ptr->node_list.end())
            {
                message_bus_ptr->node_list.remove(sender);
		mtype = NodeOffline;
            }
            else
            {
                return;
            }
        }
        else
        {
            return;
        }
    }



    if(message_bus_ptr->callback)
    {
        unsigned char* buffer = nullptr;
        unsigned long buffer_size = 0;

        if(dtype != Text)
        {
            buffer = messgae_bus_utility_base64_decode((const char*)message->payload, message->payloadlen, buffer, &buffer_size);
        }
        else
        {
            buffer = (unsigned char*)message->payload;
            buffer_size = message->payloadlen;
        }

        message_bus_ptr->callback(sender.c_str(), ptype, mtype, dtype, (char*)buffer, buffer_size, message->mid);

        if(dtype != Text)
        {
            free(buffer);
        }
    }
}
