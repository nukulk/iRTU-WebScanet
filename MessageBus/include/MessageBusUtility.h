#ifndef MESSAGE_BUS_UTILITY
#define MESSAGE_BUS_UTILITY

#include <string>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif

extern char* messgae_bus_utility_base64_encode(const unsigned char *data, unsigned long inputlength, char *encodedString, unsigned long *outputlength);
extern unsigned char* messgae_bus_utility_base64_decode(const char *encodedString, unsigned long inputlength, unsigned char *decodedData, unsigned long *outputlength);
extern char*  messgae_bus_utility_current_process_name(char* ptr);
extern void messgae_bus_utility_split_string(const std::string &str, std::vector<std::string> &tokens, char delim);

#ifdef __cplusplus
}
#endif

#endif
