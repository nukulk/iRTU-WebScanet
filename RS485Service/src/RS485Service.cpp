#include "RS485Service.hpp"
#include "ProtocolBACNET.hpp"
#include "ProtocolMODBUS.hpp"

#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <memory.h>
#include <malloc.h>

#include <MessageBus/MessageBus.h>
#include <RTUCore/Logger.h>
#include <RTUCore/SignalHandler.h>
#include <RTUCore/Directory.h>
#include <RTUCore/StringEx.h>
#include <RTUCore/Configuration.h>

#include <string>
#include <thread>
#include <chrono>

static std::string payload_template = "{\"device_id\":\"000\", \"timestamp\":\"<timestamp>\", \"vibrationX\":\"<vibrationX>\", \"vibrationZ\":\"<vibrationZ>\", \"temperatureC\":\"<temperatureC>\"}";
static void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void OnSignalReceived(SignalType type);

static RS485Service *appptr = nullptr;

RS485Service::RS485Service()
{
    params_modbus = nullptr;
    params_bacnet = nullptr;
    params_canbus = nullptr;
    params_dnp3 = nullptr;
    message_bus = nullptr;
    logger = nullptr;
    config = nullptr;
    appptr = this;
}

RS485Service::~RS485Service()
{

}

bool RS485Service::Initialize()
{
    logger = logger_allocate(10);

    if(logger == nullptr)
    {
        return false;
    }

    signals_register_callback(OnSignalReceived);
    signals_initialize_handlers();

    config = configuration_allocate_default();

    if(config == nullptr)
    {
        WriteLog(logger, "Could not load configuration", LOG_ERROR);
        logger_release(logger);
        return false;
    }

    std::string value;
    configuration_get_value_as_string(config, "default", "destination", value);
    str_ex_split_with_char(value, destination_list, ',');
    configuration_get_value_as_string(config, "default", "device_id", device_id);
    configuration_get_value_as_string(config, "default", "protocol", protocol_name);
    configuration_get_value_as_string(config, "default", "device", device);
    enabled = configuration_get_value_as_boolean(config, "default", "enabled");
    baud_rate = configuration_get_value_as_integer(config, "default", "baud_rate");
    data_bits = configuration_get_value_as_integer(config, "default", "data_bits");
    stop_bits = configuration_get_value_as_integer(config, "default", "stop_bits");
    xon_off = configuration_get_value_as_integer(config, "default", "xon_off");
    slave_id = configuration_get_value_as_integer(config, "default", "slave_id");
    port_timeout = configuration_get_value_as_integer(config, "default", "port_timeout");
    configuration_get_value_as_string(config, "default", "parity", value);
    parity = value.at(0);

    if(protocol_name == "modbus")
    {
        params_modbus = new ModbusParameters();
        params_modbus->num_of_query = configuration_get_value_as_integer(config, "modbus", "query_count");

        std::string query_paramns;
        configuration_get_value_as_string(config, "modbus", "query_params", query_paramns);
        std::vector<std::string> queries;
        str_ex_split_with_char(query_paramns, queries, ',');

        if(params_modbus->num_of_query != queries.size())
        {
            configuration_release(config);
            WriteLog(logger, "Invalid MODBUS confuiguration. Query count and paramter list count mismatch", LOG_ERROR);
            logger_release(logger);
            logger = nullptr;
            config = nullptr;
            return false;
        }

        if(params_modbus->num_of_query > MAX_MODBUS_QUERY)
        {
            params_modbus->num_of_query = MAX_MODBUS_QUERY;
        }
    }

    configuration_release(config);

    return true;
}

bool RS485Service::Destroy()
{
    return true;
}

bool RS485Service::Start()
{
    if(enabled)
    {
        if(protocol_name == "modbus")
        {
            io_protocol = new ProtocolModbus();
            io_protocol->SetProtocolConfigutaion(params_modbus);
        }
        else
        {
            if(protocol_name == "bacnet")
            {
                io_protocol = new ProtocolBacnet();
                io_protocol->SetProtocolConfigutaion(params_bacnet);
            }
        }
    }

    io_protocol->SetDeviceName(device, slave_id);
    io_protocol->SetPortConfiguration(baud_rate, parity, stop_bits, data_bits, xon_off, port_timeout);

    if(!io_protocol->Initialize())
    {
        WriteLog(logger, protocol_name + " => could not initialize port or no device found", LOG_ERROR);
        return false;
    }

    if(!message_bus_initialize(&message_bus, OnNetworkEvent))
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(message_bus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    //Test payload
    message_bus_send_loopback(message_bus);

    bool continue_loop = true;

    while(continue_loop && enabled)
    {
        std::this_thread::sleep_for (std::chrono::seconds(10));

        char *buffer = nullptr;
        float temperatureC = 0;
        float vibrationZ = 0;
        float vibrationX = 0;

        for (unsigned int idx = 0; idx < ((ProtocolModbus*)io_protocol)->GetQueryCount(); ++idx)
        {
            io_protocol->Read(idx, (void**)&buffer);

            if(idx == 0)
            {
                temperatureC = ((float)((buffer[1] << 8 ) | buffer[0])) / 100;
            }

            if(idx == 2)
            {
                vibrationZ = ((float)((buffer[9] << 8 ) | buffer[8])) / 10;
            }

            if(idx == 3)
            {
                vibrationX = ((float)((buffer[9] << 8 ) | buffer[8])) / 10;
            }

            free(buffer);

            buffer = nullptr;
        }

        std::string payload = payload_template;

        std::string timestamp;
        str_ex_get_default_timestamp(timestamp);

        timestamp.resize(timestamp.size() - 1);
        str_ex_replace_with_str(payload, "<timestamp>", timestamp);

        str_ex_replace_with_real(payload, "<temperatureC>", temperatureC);
        str_ex_replace_with_real(payload, "<vibrationX>", vibrationX);
        str_ex_replace_with_real(payload, "<vibrationZ>", vibrationZ);

        for (auto destination : destination_list)
        {
            long payload_id = 0;

            if(!message_bus_send(message_bus, destination.c_str(), Data, UserData, Text, payload.c_str(), payload.length()))
            {
                continue_loop = false;
                break;
            }
        }
    }

    return true;
}

bool RS485Service::Restart()
{
    return false;
}

bool RS485Service::Stop()
{
    if(io_protocol)
    {
        io_protocol->Destroy();
        delete io_protocol;
    }

    if(message_bus)
    {
        message_bus_close(message_bus);
        message_bus_release(message_bus);
    }

    if(config)
    {
        configuration_release(config);
    }

    if(logger)
    {
        logger_release(logger);
    }

    return false;
}

void *RS485Service::GetLogger()
{
    return logger;
}

void OnNetworkEvent(const char* node_name, PayloadType ptype, MessageType mtype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{
    printf("%s %c %c %c %ld %s %ld\n", node_name, ptype, mtype, dtype, buffersize, messagebuffer, payload_id);
}

void OnSignalReceived(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(appptr->GetLogger(), "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(appptr->GetLogger(), "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(appptr->GetLogger(), "SHUTDOWN SIGNAL", LOG_CRITICAL);
            appptr->Stop();
            exit(0);
        }
        case Alarm:
        {
            WriteLog(appptr->GetLogger(), "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(appptr->GetLogger(), "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(appptr->GetLogger(), "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(appptr->GetLogger(), "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(appptr->GetLogger(), "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}
