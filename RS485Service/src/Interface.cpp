//
// Created by mukeshp on 07-Jul-20.
//
#include "Interface.hpp"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <modbus/modbus.h>

#define GET_MODBUS_QUERY_RESP_SIZE(_arg_)   ((_arg_) == MODBUS_FC_READ_COILS) ? (sizeof(uint8_t)) :\
                                            ((_arg_) == MODBUS_FC_READ_DISCRETE_INPUTS) ? (sizeof(uint8_t)) : sizeof(uint16_t)

modbusInterface *pModbus = nullptr;

modbus_t *gsCtx = nullptr;

SerialPortCnfg::SerialPortCnfg()
{
    strcpy(this->mPortName, "/dev/ttymxc3");
    this->mBaudRate = 9600;
    this->mParity = 'N';
    this->mDataBits = 8;
    this->mStopBits = 1;
}

SerialPortCnfg::~SerialPortCnfg()
{
}

ModbusQueryCnfg::ModbusQueryCnfg()
{
    this->mQueryType = MODBUS_FC_READ_HOLDING_REGISTERS;
    this->modbusStartAddr = 2;
    this->mNoOfRegister = 17;
}

ModbusQueryCnfg::~ModbusQueryCnfg()
{

}

int modbusInterface::modbus_init(void)
{
    modbus_t *tCtx = NULL;

    printf("pModbus->mSerialPortCnfg.mPortName:%s\n", pModbus->mSerialPortCnfg.mPortName);
    printf("pModbus->mSerialPortCnfg.mBaudRate:%d\n", pModbus->mSerialPortCnfg.mBaudRate);
    printf("pModbus->mSerialPortCnfg.mParity:%c\n", pModbus->mSerialPortCnfg.mParity);
    printf("pModbus->mSerialPortCnfg.mDataBits:%d\n", pModbus->mSerialPortCnfg.mDataBits);
    printf("pModbus->mSerialPortCnfg.mStopBits:%d\n", pModbus->mSerialPortCnfg.mStopBits);

    tCtx = modbus_new_rtu(pModbus->mSerialPortCnfg.mPortName, pModbus->mSerialPortCnfg.mBaudRate,
                          pModbus->mSerialPortCnfg.mParity, pModbus->mSerialPortCnfg.mDataBits,
                          pModbus->mSerialPortCnfg.mStopBits);
    if (NULL == tCtx)
    {
        fprintf(stderr, "Unable to create the libmodbus context(%s)\n", modbus_strerror(errno));
        return -1;
    }

    printf("this->GetSlaveAddress():%d\n", this->GetSlaveAddress());
    printf("pModbus->GetSlaveAddress():%d\n", pModbus->GetSlaveAddress());

    modbus_set_slave(tCtx, pModbus->GetSlaveAddress());

    modbus_set_response_timeout(tCtx, pModbus->modbusCnfg.mTimeOutSec, 0);
    //modbus_set_indication_timeout(tCtx, pModbus->modbusCnfg.mTimeOutSec, 0);
    //modbus_set_indication_timeout(tCtx, pModbus->modbusCnfg.mTimeOutSec, 0);

    if (-1 == modbus_connect(tCtx))
    {
        fprintf(stderr, "Connection failed: %s\n", modbus_strerror(errno));
        modbus_free(tCtx);
        return -1;
    }

    gsCtx = tCtx;
    return 0;
}

int modbusInterface::modbus_deinit(void)
{
    printf("Modbus deinit\n");
    modbus_close(gsCtx);
    modbus_free(gsCtx);
    return 0;
}

int modbusInterface::modbus_master_read_Data(unsigned int iQueryIndex, void **pDataBytes)
{
    int tRetVal = -1;
    ModbusQueryCnfg_t *tQueryCnfg = NULL;
    void *tempDest = NULL;

    if(NULL == pDataBytes)
    {
        printf("Error at pDataBytes is null at %s\n", __func__ );
        return -1;
    }

    if(iQueryIndex > MAX_MODBUS_QUERY)
    {
        printf("Error at iQueryIndex(%d) at %s\n", iQueryIndex, __func__ );
        return -1;
    }

    tQueryCnfg = &(pModbus->modbusCnfg.modbusQueryCnfg[iQueryIndex]);

    (*pDataBytes) = calloc(tQueryCnfg->mNoOfRegister, GET_MODBUS_QUERY_RESP_SIZE(tQueryCnfg->mQueryType));
    if(NULL == (*pDataBytes))
    {
        printf("Error at calloc(%s) in %s\n", strerror(errno), __func__ );
        return -1;
    }

    tempDest = (*pDataBytes);

    switch (tQueryCnfg->mQueryType)
    {
        case MODBUS_FC_READ_COILS:
        {
            tRetVal = modbus_read_bits(gsCtx, tQueryCnfg->modbusStartAddr, tQueryCnfg->mNoOfRegister, (uint8_t*)tempDest);
            if(-1 == tRetVal)
            {
                printf("Error at modbus_read_bits(%s)\n", modbus_strerror(errno));
            }
            break;
        }
        case MODBUS_FC_READ_DISCRETE_INPUTS:
        {
            tRetVal = modbus_read_input_bits(gsCtx, tQueryCnfg->modbusStartAddr, tQueryCnfg->mNoOfRegister, (uint8_t*)tempDest);
            if(-1 == tRetVal)
            {
                printf("Error at modbus_read_input_bits(%s)\n", modbus_strerror(errno));
            }
            break;
        }
        case MODBUS_FC_READ_HOLDING_REGISTERS:
        {
            printf("tQueryCnfg->modbusStartAddr: %d\n", tQueryCnfg->modbusStartAddr);
            printf("tQueryCnfg->mNoOfRegister  : %d\n", tQueryCnfg->mNoOfRegister);

            tRetVal = modbus_read_registers(gsCtx, tQueryCnfg->modbusStartAddr, tQueryCnfg->mNoOfRegister, (uint16_t*)tempDest);
            if(-1 == tRetVal)
            {
                printf("Error at modbus_read_registers(%s)\n", modbus_strerror(errno));
            }
            break;
        }
        case MODBUS_FC_READ_INPUT_REGISTERS:
        {
            tRetVal = modbus_read_input_registers(gsCtx, tQueryCnfg->modbusStartAddr, tQueryCnfg->mNoOfRegister, (uint16_t*)tempDest);
            if(-1 == tRetVal)
            {
                printf("Error at modbus_read_input_registers(%s)\n", modbus_strerror(errno));
            }
            break;
        }
#if 0
        case MODBUS_FC_WRITE_SINGLE_COIL:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_SINGLE_REGISTER:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_MULTIPLE_COILS:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
#endif
        default:
        {
            printf("Invalid Query type\n");
        }
    }
    return 0;
}

int modbusInterface::GetNumberOfQuery(void)
{
    return (pModbus->modbusCnfg.mNumOfQuery);
}

int modbusInterface::GetSlaveAddress(void)
{
    return (pModbus->modbusCnfg.mSlaveId);
}

int modbusInterface::SetConfigurations(void)
{
    pModbus->modbusCnfg.mNumOfQuery = 4;
    pModbus->modbusCnfg.mSlaveId = 1;
    pModbus->modbusCnfg.mTimeOutSec = 7;

    //pModbus->modbusCnfg.modbusQueryCnfg[0].modbusStartAddr = 4000;
    //pModbus->modbusCnfg.modbusQueryCnfg[0].mQueryType = MODBUS_FC_READ_HOLDING_REGISTERS;
    //pModbus->modbusCnfg.modbusQueryCnfg[0].mNoOfRegister = 20;

    pModbus->modbusCnfg.modbusQueryCnfg[0].modbusStartAddr = 42;
    pModbus->modbusCnfg.modbusQueryCnfg[0].mQueryType = MODBUS_FC_READ_HOLDING_REGISTERS;
    pModbus->modbusCnfg.modbusQueryCnfg[0].mNoOfRegister = 1;

    pModbus->modbusCnfg.modbusQueryCnfg[1].modbusStartAddr = 48;
    pModbus->modbusCnfg.modbusQueryCnfg[1].mQueryType = MODBUS_FC_READ_HOLDING_REGISTERS;
    pModbus->modbusCnfg.modbusQueryCnfg[1].mNoOfRegister = 1;

    pModbus->modbusCnfg.modbusQueryCnfg[2].modbusStartAddr = 2400;
    pModbus->modbusCnfg.modbusQueryCnfg[2].mQueryType = MODBUS_FC_READ_HOLDING_REGISTERS;
    pModbus->modbusCnfg.modbusQueryCnfg[2].mNoOfRegister = 10;

    pModbus->modbusCnfg.modbusQueryCnfg[3].modbusStartAddr = 2450;
    pModbus->modbusCnfg.modbusQueryCnfg[3].mQueryType = MODBUS_FC_READ_HOLDING_REGISTERS;
    pModbus->modbusCnfg.modbusQueryCnfg[3].mNoOfRegister = 10;

#if 0
    pModbus->modbusCnfg.mNumOfQuery = 1;
    pModbus->modbusCnfg.mSlaveId = 1;
    pModbus->modbusCnfg.mTimeOutSec = 7;

    pModbus->modbusCnfg.modbusQueryCnfg[0].modbusStartAddr = 4000;//4001;
    pModbus->modbusCnfg.modbusQueryCnfg[0].mQueryType = MODBUS_FC_READ_HOLDING_REGISTERS;
    pModbus->modbusCnfg.modbusQueryCnfg[0].mNoOfRegister = 20;
#endif
    return 0;
}
