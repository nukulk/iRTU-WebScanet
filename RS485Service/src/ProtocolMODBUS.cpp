#include "ProtocolMODBUS.hpp"
#include <string.h>
#include <stdarg.h>
#include <modbus/modbus.h>

#define GET_MODBUS_QUERY_RESP_SIZE(_arg_)   ((_arg_) == MODBUS_FC_READ_COILS) ? (sizeof(uint8_t)) :\
                                            ((_arg_) == MODBUS_FC_READ_DISCRETE_INPUTS) ? (sizeof(uint8_t)) : sizeof(uint16_t)

//static unsigned int num_of_query = 0;
//static ModbusQuery modbus_query_configuration[MAX_MODBUS_QUERY];
static modbus_t *modbus_context = nullptr;

ProtocolModbus::ProtocolModbus()
{
    memset(port_name, 0, 48);
    baud_rate = 9600;
    parity = 'N';
    data_bits = 8;
    stop_bits = 1;
    timeout_seconds = 10;
    slave_id = 0;
}

ProtocolModbus::~ProtocolModbus()
{
}

bool ProtocolModbus::SetDeviceName(const std::string &device_name, unsigned char slaveid)
{
    memset(port_name, 0, 48);
    strcpy(port_name, device_name.c_str());
    slave_id = slaveid;
    return false;
}

bool ProtocolModbus::SetPortConfiguration(int bd, char pr, int sb, int db, int xo, unsigned int timeout)
{
    baud_rate = bd;
    parity = pr;
    data_bits = db;
    stop_bits = sb;
    timeout_seconds = timeout;
    return true;
}

bool ProtocolModbus::SetProtocolConfigutaion(const ModbusParameters *ptr)
{
    parameters.num_of_query = ptr->num_of_query;

    if(parameters.num_of_query > MAX_MODBUS_QUERY)
    {
        parameters.num_of_query = MAX_MODBUS_QUERY;
    }

    for (int index = 0; index < parameters.num_of_query; index++)
    {
        parameters.modbus_query_configuration[0].start_address = ptr->modbus_query_configuration[index].start_address;
        parameters.modbus_query_configuration[0].query_type = ptr->modbus_query_configuration[index].query_type;
        parameters.modbus_query_configuration[0].num_of_register = ptr->modbus_query_configuration[index].num_of_register;
    }

    return true;
}

bool ProtocolModbus::Initialize()
{
    modbus_context = modbus_new_rtu(port_name, baud_rate, parity, data_bits, stop_bits);

    if (nullptr == modbus_context)
    {
        return false;
    }

    modbus_set_slave(modbus_context, slave_id);
    modbus_set_response_timeout(modbus_context, timeout_seconds, 0);

    if (-1 == modbus_connect(modbus_context))
    {
        modbus_free(modbus_context);
        return false;
    }

    /*
    parameters.num_of_query = 4;

    parameters.modbus_query_configuration[0].start_address = 42;
    parameters.modbus_query_configuration[0].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    parameters.modbus_query_configuration[0].num_of_register = 1;

    parameters.modbus_query_configuration[1].start_address = 48;
    parameters.modbus_query_configuration[1].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    parameters.modbus_query_configuration[1].num_of_register = 1;

    parameters.modbus_query_configuration[2].start_address = 2400;
    parameters.modbus_query_configuration[2].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    parameters.modbus_query_configuration[2].num_of_register = 10;

    parameters.modbus_query_configuration[3].start_address = 2450;
    parameters.modbus_query_configuration[3].query_type = MODBUS_FC_READ_HOLDING_REGISTERS;
    parameters.modbus_query_configuration[3].num_of_register = 10;
    */

    return true;
}

bool ProtocolModbus::Destroy()
{
    modbus_close(modbus_context);
    modbus_free(modbus_context);
    return true;
}

bool ProtocolModbus::Read(unsigned int index, void** buffer)
{
    int retval = -1;
    ModbusQuery *query = nullptr;
    void *temp_buffer = nullptr;

    if(nullptr == buffer)
    {
        return false;
    }

    if(index > MAX_MODBUS_QUERY)
    {
        return false;
    }

    query = &(parameters.modbus_query_configuration[index]);

    (*buffer) = calloc(query->num_of_register, GET_MODBUS_QUERY_RESP_SIZE(query->query_type));

    if(nullptr == (*buffer))
    {
        return false;
    }

    temp_buffer = (*buffer);

    switch (query->query_type)
    {
        case MODBUS_FC_READ_COILS:
        {
            retval = modbus_read_bits(modbus_context, query->start_address, query->num_of_register, (uint8_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
        case MODBUS_FC_READ_DISCRETE_INPUTS:
        {
            retval = modbus_read_input_bits(modbus_context, query->start_address, query->num_of_register, (uint8_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
        case MODBUS_FC_READ_HOLDING_REGISTERS:
        {
            retval = modbus_read_registers(modbus_context, query->start_address, query->num_of_register, (uint16_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
        case MODBUS_FC_READ_INPUT_REGISTERS:
        {
            retval = modbus_read_input_registers(modbus_context, query->start_address, query->num_of_register, (uint16_t*)temp_buffer);

            if(-1 == retval)
            {
            }

            break;
        }
#if 0
        case MODBUS_FC_WRITE_SINGLE_COIL:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_SINGLE_REGISTER:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_MULTIPLE_COILS:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
        case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
        {
            printf("Not implemented : todo : Implementation pending\n");
            break;
        }
#endif
        default:
        {
        }
    }

    return true;
}

bool ProtocolModbus::Write(unsigned int index, void* buffer)
{
    return false;
}

int ProtocolModbus::GetQueryCount()
{
    return parameters.num_of_query;
}

