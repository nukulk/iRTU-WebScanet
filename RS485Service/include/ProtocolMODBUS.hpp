#ifndef PROTOCOL_MODBUS
#define PROTOCOL_MODBUS

#include "IProtocolRS485.hpp"

#define MAX_MODBUS_QUERY (10)

typedef struct ModbusQuery
{
    unsigned char query_type;
    int start_address;
    int num_of_register;
}ModbusQuery;

typedef struct ModbusParameters
{
    unsigned int num_of_query;
    ModbusQuery modbus_query_configuration[MAX_MODBUS_QUERY];
}ModbusParameters;

class ProtocolModbus : public IProtocolRS485
{
public:
    ProtocolModbus();
    virtual ~ProtocolModbus() override;
    bool SetDeviceName(const std::string &device_name, unsigned char slaveid) override;
    bool SetPortConfiguration(int bd, char pr, int sb, int db, int xo, int unsigned timeout) override;
    bool Initialize() override;
    bool Destroy() override;
    bool Read(unsigned int index, void** buffer) override;
    bool Write(unsigned int index, void* buffer) override;
    bool SetProtocolConfigutaion(const ModbusParameters* ptr) override;
    int GetQueryCount();
private:
    ModbusParameters parameters;
};

#endif
