//
// Created by mukeshp on 07-Jul-20.
//

#ifndef _INTERFACE_H
#define _INTERFACE_H

#define MAX_MODBUS_QUERY (10)

typedef struct SerialPortCnfg
{
    char mPortName[48];
    int mBaudRate;
    char mParity;
    char mDataBits;
    char mStopBits;

    SerialPortCnfg();
    ~SerialPortCnfg();

}SerialPortCnfg_t;

typedef struct ModbusQueryCnfg
{
    unsigned char mQueryType;
    int modbusStartAddr;
    int mNoOfRegister;

    ModbusQueryCnfg();
    ~ModbusQueryCnfg();
}ModbusQueryCnfg_t;

typedef struct ModbusCnfg
{
    unsigned char mSlaveId;
    unsigned int mNumOfQuery;
    ModbusQueryCnfg_t modbusQueryCnfg[MAX_MODBUS_QUERY];
    unsigned int mTimeOutSec;

    ModbusCnfg()
    {
        this->mSlaveId = 1;
        this->mNumOfQuery = 1;
        this->mTimeOutSec = 7;
    }

    ~ModbusCnfg()
    {

    }
}ModbusCnfg_t;

class modbusInterface
{


    public:
    int modbus_init(void);
    int modbus_deinit(void);
    int modbus_master_read_Data(unsigned int iQueryIndex, void **pDataBytes);

    int GetNumberOfQuery(void);
    int GetSlaveAddress(void);

    int SetConfigurations(void);

    modbusInterface(void)
    {
    }

    ~modbusInterface(void)
    {
        this->modbus_deinit();
    }

    ModbusCnfg_t modbusCnfg;
    SerialPortCnfg_t mSerialPortCnfg;
};

extern modbusInterface *pModbus;

#endif //RS485INTERFACE_2_INTERFACE_H
