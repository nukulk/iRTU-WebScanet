#ifndef RS485_SERVICE
#define RS485_SERVICE

#include <stdbool.h>
#include <string>
#include <vector>
#include "IProtocolRS485.hpp"

class RS485Service
{
public:
    RS485Service();
    virtual ~RS485Service();
    bool Initialize();
    bool Destroy();
    bool Start();
    bool Restart();
    bool Stop();
    void* GetLogger();
private:
    void* config;
    void* message_bus;
    void* logger;
    IProtocolRS485 *io_protocol;

    std::vector<std::string> destination_list;
    bool enabled;
    std::string device_id;
    std::string protocol_name;
    std::string device;
    int baud_rate;
    int data_bits;
    int stop_bits;
    int xon_off;
    char parity;
    int slave_id;
    int port_timeout;

    ModbusParameters* params_modbus;
    BacnetParameters* params_bacnet;
    CanbusParameters* params_canbus;
    DNP3Parameters*   params_dnp3;
};

#endif
