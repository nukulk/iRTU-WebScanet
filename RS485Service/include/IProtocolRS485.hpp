#ifndef I_PROTOCOL_RS485
#define I_PROTOCOL_RS485

#include <string>

typedef struct ModbusParameters ModbusParameters;
typedef struct BacnetParameters BacnetParameters;
typedef struct CanbusParameters CanbusParameters;
typedef struct DNP3Parameters DNP3Parameters;

class IProtocolRS485
{
public:
    IProtocolRS485() {}
    virtual ~IProtocolRS485() {}
    virtual bool SetDeviceName(const std::string &device_name, unsigned char slaveid) = 0;
    virtual bool SetPortConfiguration(int bd, char pr, int sb, int db, int xo, unsigned int timeout) =0;
    virtual bool Initialize() = 0;
    virtual bool Destroy() = 0;
    virtual bool Read(unsigned int index, void** buffer) = 0;
    virtual bool Write(unsigned int index, void* buffer) = 0;
    virtual bool SetProtocolConfigutaion(const ModbusParameters*) { return false; }
    virtual bool SetProtocolConfigutaion(const BacnetParameters*) { return false; }
    virtual bool SetProtocolConfigutaion(const CanbusParameters*) { return false; }
    virtual bool SetProtocolConfigutaion(const DNP3Parameters*) { return false; }
protected:
    char port_name[48];
    int baud_rate;
    char parity;
    char data_bits;
    char stop_bits;
    unsigned int timeout_seconds;
    unsigned char slave_id;
};

#endif
